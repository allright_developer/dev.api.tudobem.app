<?php
// public authentication methods
Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@login');
Route::get('/logout', 'Api\AuthController@logout');
Route::post('/user', 'Api\AuthController@user');
Route::post('/user/confirm', 'Api\AuthController@confirm'); // user confirmation method
Route::get( '/get', 'Api\SurveyController@get' );

/* signed in USER routes */
Route::namespace('Api')->middleware(['auth'])->group( function() {

    Route::post( '/user/get', 'UserController@user' );
    Route::post( '/credit', 'CreditController@post' );
    Route::post( '/answer', 'AnswerController@post' );

    Route::post( '/promotion/favorite', 'PromotionController@favorite' ); // favorite a Promotion
    Route::get( '/promotion-get', 'PromotionController@get' ); // retrieve promotions

    Route::post( '/offerwall', 'OfferwallController@post' ); // post a new lead from OfferWall
    Route::get( '/offerwall/get-by-id/{id}', 'OfferwallController@getById' ); // get a complete offer data by id

    Route::post( '/offerwall/offer/impression', 'Offerwall\OfferwallStatController@impression' ); // general data grid feeder
    Route::post( '/offerwall/finish-impression', 'Offerwall\OfferwallStatController@offerwallImpressionFinish' ); // general data grid feeder

    Route::post( '/tracker/click', 'TrackerController@click' );

    Route::post( '/user/update', 'UserController@update' ); // general user data updating
    Route::get( '/user/profile/get', 'UserDataController@get' ); // get profile data
    Route::post( '/user/profile/update', 'UserDataController@update' ); // update user profile

    Route::post( '/product/favorite', 'ProductController@favorite' ); // toggle favorite
    Route::post( '/product/exchange', 'ProductController@exchange' ); // exchange credibens by products

    Route::post( '/ong/favorite/{id?}', 'OngController@favorite' ); // ong favorite
    Route::get( '/ong/{id?}', 'OngController@get' ); // get an ong by id

    Route::get( '/get-angels', 'AngelController@get' ); // angel and gift list

    Route::post( '/push/subscribe/{token}/{service?}', 'Service\PushController@subscribe' );

    Route::get( '/survey/get/{slug?}', 'SurveyController@get' );
});

/* actual guard to login as an admin */
Route::namespace( 'Api' )->middleware([ 'get.guard:admin' ])->group( function(){
    Route::post( '/admin/login', 'AuthController@adminLogin' );
});
// ADMIN signed in routes
Route::namespace( 'Api' )->middleware([ 'auth:admin' ])->group( function(){

    Route::post( '/user/data-grid', 'UserController@dataGrid' ); // user grid data

    Route::post( '/survey/data-grid', 'SurveyController@dataGrid' ); // general data grid feeder
    Route::post( '/survey', 'SurveyController@post'); // upsert of surveys
    Route::get( '/survey/get-by-id/{id}', 'SurveyController@getById' ); // getting a survey by id
    Route::post( '/survey/status', 'SurveyController@toggleStatus' ); // toggle offer status

    Route::post( '/promotion', 'PromotionController@post' ); // upsert promotion
    Route::post( '/promotion/toggle-status', 'PromotionController@toggleStatus' ); // promotion toggle status
    Route::get( '/promotion/get-by-id/{id}', 'PromotionController@getById' ); // retrieve by id url
    Route::post( '/promotion/data-grid', 'PromotionController@dataGrid' ); // general data grid feeder

    Route::post( '/offerwall/post', 'Offerwall\OfferController@post' ); // offer post data
    Route::post( '/offerwall/update', 'Offerwall\OfferController@update' ); // offer post update data
    Route::get( '/offerwall/offer/{id}', 'Offerwall\OfferController@get' ); // get complete offer data by id
    Route::get( '/offerwall/get-ordered', 'Offerwall\OfferController@getOrdered' ); // get offer ordered by order field
    Route::post( '/offerwall/post-ordered', 'Offerwall\OfferController@postOrdered' ); // post offer ordered by Offerwall order Module View
    Route::post( '/offerwall/offer/toggle-status', 'Offerwall\OfferController@toggleStatus' ); // toggle offer status
    Route::post( '/offerwall/data-grid', 'Offerwall\OfferController@dataGrid' ); // general offer data grid feeder
    Route::get( '/ad_publisher/{status?}', 'Offerwall\PublisherController@get' ); // publisher feed
    Route::post( '/category', 'CategoryController@post'); // categories create

    Route::post( '/company', 'CompanyController@post'); // company create
    Route::get( '/company', 'CompanyController@get'); // company create

    Route::get( '/stats/{entity}/{period?}', 'StatsController@get' ); // general simple stats
    Route::get( '/stats/card/{entity}/{type?}', 'StatsController@cardStats' ); // general simple card stats by comparison periods

    Route::post( '/product', 'ProductController@post' ); // upsert for products
    Route::post( '/product/toggle-status', 'ProductController@toggleStatus' ); // toggle status for products
    Route::post( '/product/data-grid', 'ProductController@dataGrid' ); // general data grid feeder
    Route::get( '/product/get-by-id/{id}', 'ProductController@getById' ); // getting a product by id
    Route::post( '/product/offer', 'Product\OfferController@post' ); // upsert an offer
    Route::post( '/product_offer/data-grid', 'Product\OfferController@dataGrid' ); // upsert an offer
    Route::get( '/product-offer/get-by-id/{id}', 'Product\OfferController@getById' ); // get to edit an offer

    Route::get( '/angel/gifts/{month?}/{year?}', 'AngelGiftController@get' ); // returns angel list
    Route::post( '/angel/gifts', [ \App\Http\Controllers\Api\AngelGiftController::class, 'post' ] ); // saves angel list
    Route::post( '/angel-gifts/data-grid', 'AngelGiftController@dataGrid' ); // angel list grid data

    Route::post( '/ong/data-grid', 'OngController@dataGrid' ); // ong data grid feeder
    Route::post( '/ong', 'OngController@post' ); // ong create method

    Route::get( '/settings', 'SettingsController@get' );
    Route::post( '/settings', 'SettingsController@post' );

});

Route::namespace( 'Api' )->middleware([ 'get.guard:admin', 'jwt.refresh' ])->group( function(){
    Route::post( '/admin/refresh', 'AuthController@refresh' );
});

/* public routes */
Route::get( '/offerwall', 'Api\OfferwallController@get' ); // get offers to OfferWall

Route::get( '/product/get/{slug?}', 'Api\ProductController@get' ); // get products

Route::get( '/category', 'Api\CategoryController@get'); // public categories feed
Route::get( '/ong-category', 'Api\OngCategoryController@get'); // public categories feed
Route::get( '/bank/{id?}', 'Api\BankController@get'); // public bank feed
Route::get( '/category/hierarchically', 'Api\CategoryController@getHierarchically' ); // categories hierarchically organized

Route::get( '/promotion/{slug?}', 'Api\PromotionController@get' ); // retrieve promotions

Route::get( '/ong/{id?}', 'Api\OngController@get' ); // ong get method
Route::get( '/settings/public', 'Api\SettingsController@getPublic' );

Route::get( '/tracker/conversion', 'Api\TrackerController@conversion' );
/* end public routes */
