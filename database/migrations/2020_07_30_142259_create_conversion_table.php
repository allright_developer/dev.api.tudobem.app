<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversion', function ( Blueprint $table ){
            $table->string('click_token', 32 )->comment('click token conversion');
            $table->unsignedInteger('id_user' )->nullable()->comment( 'user id that initiated the transaction' );
            $table->unsignedInteger('id_entity' )->nullable()->comment( 'entity id related with conversion' );
            $table->unsignedTinyInteger('entity' )->default( 1 )->comment( 'entity according with Click Mapper' );
            $table->json('params' )->nullable()->comment( 'conversion params' );
            $table->timestamps();
            $table->unsignedTinyInteger('status' )->default( 2 )->comment( '1=ok,2=wait,0=canceled' );

            $table->unique('click_token', 'uq_conversion' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversion');
    }
}
