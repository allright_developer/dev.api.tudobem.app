<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    protected $table = 'lead';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->string('token', 16 )->unique()->comment( 'unique token hash id' );
            $table->unsignedInteger( 'id_user' )->nullable()->comment( 'user id that created the lead' );
            $table->string( 'generated_by', 25 )->default( 'offer' )->comment( 'entity source lead generator');
            $table->unsignedInteger( 'id_entity' )->comment( 'lead generator entity id' );

            $table->json( 'data' )->comment( 'lead data in json format validated and ready to be sent' );
            $table->dateTime( 'delivered_at' )->nullable()->comment( 'sending date' );
            $table->dateTime('created_at' )->useCurrent()->comment('lead creation timestamp');
            $table->unsignedTinyInteger( 'status' )->default(1 )->comment('1=valid,0=invalid,2=wait_validation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
