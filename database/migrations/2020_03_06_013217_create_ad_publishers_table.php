<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdPublishersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_publisher', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_company' )->nullable()->nullable('related company entity with the ad publisher' );
            $table->string( 'label', 40 )->comment( 'ad publisher name' );
            $table->string( 'description', 120 );

            $table->timestamps();

            $table->unsignedTinyInteger( 'status' )->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_publishers');
    }
}
