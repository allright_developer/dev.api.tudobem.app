<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_image', function (Blueprint $table) {
            $table->unsignedInteger('id_promotion' )->comment('promotion id' );
            $table->string('name', 125 )->unique()->comment( 'image name in fs or cloud' );
            $table->string('description', 150 )->nullable()->comment( 'image brief description' );
            $table->unsignedTinyInteger('status' )->comment('image status' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_image');
    }
}
