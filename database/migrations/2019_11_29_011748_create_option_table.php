<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionTable extends Migration
{
    public $table = 'option';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('id_question')->comment('question id that question belongs to' );
            $table->unsignedTinyInteger('order')->default(0)->comment('answer sequential order in the question' );
            $table->unsignedTinyInteger('right' )->nullable()->comment('is this option the right option?' );
            $table->string('label', 70 )->comment('answer description' );
            $table->string('val',70)->nullable()->comment('answer value' );
            $table->integer('credits')->nullable()->default(0)->comment('amount to give to user when choosing it' );
            $table->integer('credits_beneficent')->nullable()->default(0)->comment('amount to give to Organization when it is marked');
            $table->timestamps();
            $table->unsignedTinyInteger('status' )->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
