<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceSubscriptionsTable extends Migration
{
    public $table = 'user_service_subscription';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table){
            $table->increments( 'id' );
            $table->unsignedInteger( 'id_user' )->nullable()->comment('it indicates if the subscription came from an signed in user');
            $table->string( 'name', 70 )->comment('service name')->default('fcm' );
            $table->string( 'type', 50 )->comment('push,email,whats')->default('push');
            $table->json( 'data' )->nullable()->comment('custom info' );
            $table->timestamps();
            $table->unsignedTinyInteger('status' )->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
