<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOfferAssetAddVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_asset', function (Blueprint $table) {
            $table->unsignedTinyInteger('ad_version' )
                ->after('url' )
                ->default(1)
                ->comment('the ad version that the asset belongs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_asset', function (Blueprint $table) {
            //
        });
    }
}
