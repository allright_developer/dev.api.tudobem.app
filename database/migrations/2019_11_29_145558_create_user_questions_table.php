<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserQuestionsTable extends Migration
{
    public $table = 'user_survey';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->unsignedInteger('id_user' )->comment('user id that finalized a survey');
            $table->unsignedInteger('id_survey' )->comment('survey id finalized by an user');
            $table->timestamp('created_at')->useCurrent();

            $table->unique(['id_user', 'id_survey' ], 'uq_user_survey' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
