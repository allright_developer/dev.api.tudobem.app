<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_asset', function (Blueprint $table) {

            $table->increments( 'id' );
            $table->unsignedInteger( 'id_offer' );
            $table->unsignedTinyInteger('type' )->default(1)->comment('1=img,2=video,3=lottie,...' );
            $table->string( 'name', 70 )->comment('asset name in the FS' );
            $table->string( 'description', 70 )->comment('asset desc to user in the front' );
            $table->unsignedTinyInteger('storage' )->default(1)->comment('how and where the image was stored' );
            $table->string( 'url' )->nullable()->comment( 'asset url' );

            $table->timestamps();

            $table->unsignedTinyInteger( 'status' )->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_assets');
    }
}
