<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger( 'id_category' )->nullable()->comment( 'category id, optional' );
            $table->unsignedInteger( 'id_product' )->nullable()->comment( 'product id, optional' );
            $table->unsignedInteger( 'id_company' )->nullable()->comment( 'id company, optional' );
            $table->string( 'name', 45 )->nullable()->comment( 'promotion name');
            $table->string( 'label', 90 )->nullable()->comment( 'promotion brief description');
            $table->text( 'description' )->nullable()->comment( 'large description to improve SEO' );
            $table->string( 'rules', 120 )->nullable()->comment( 'promotion participation rules with rules url' );
            $table->string('url', 120 )->comment('campaign site url' );

            $table->dateTime('begin_at' )->comment('promotion start date' );
            $table->dateTime('finish_at' )->comment('promotion end date' );

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
