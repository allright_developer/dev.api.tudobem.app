<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationsTable extends Migration
{
    public $table = 'user_invitation';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->unsignedInteger('id_user' )->comment('user invitated id' );
            $table->unsignedInteger('invited_by' )->comment('user inviter id' );
            $table->string('from', 40 )->nullable()->comment('it is like utm_source' );
            $table->timestamp('signedup_at' )->useCurrent()->comment('when the user was signed up' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitations');
    }
}
