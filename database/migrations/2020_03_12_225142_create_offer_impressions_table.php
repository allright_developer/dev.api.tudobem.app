<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferImpressionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_impression', function (Blueprint $table) {
            $table->string( 'hash', 12 )->unique()->comment( 'impression unique hash' );
            $table->string( 'offerwall', 12 )->comment( 'offerwall impression related hash' );
            $table->unsignedInteger( 'id_offer' )->comment( 'offer viewed' );
            $table->unsignedInteger( 'id_user' )->nullable()->comment( 'user that visualized the offer' );
            $table->unsignedInteger('impression_start_at' )->nullable()->comment('when the impression started in unix ts' );
            $table->unsignedInteger('impression_end_at' )->nullable()->comment('when the impression ended in unix ts' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_impression');
    }
}
