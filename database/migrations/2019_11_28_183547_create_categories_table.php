<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label', 50 )->unique()->comment( 'category unique name' );
            $table->string('slug', 50 )->unique()->comment( 'category friendly url name' );
            $table->string('description', 255 )->unique()->comment( 'category unique name' );
            $table->timestamps();
            $table->unsignedTinyInteger('status' )->default(1)->comment('1=act,2=invisible,0=soft-del' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
