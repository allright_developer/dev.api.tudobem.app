<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLeadDeliveryAddStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_delivery', function (Blueprint $table) {
            $table->unsignedTinyInteger( 'status' )->default(1 )->after( 'updated_at' )
                ->comment('1=active,0=unactv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lead_delivery', function (Blueprint $table) {
            //
        });
    }
}
