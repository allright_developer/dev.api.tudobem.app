<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateImpressionAddCopyField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_impression', function (Blueprint $table) {
            $table->unsignedTinyInteger('ad_version' )->nullable()->comment( 'advertise version' )->after( 'id_user' );
            $table->unsignedInteger('id_copy' )->nullable()->comment( 'copy id' )->after( 'id_user' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
