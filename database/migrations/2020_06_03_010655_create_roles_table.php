<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    public $table = 'collaborator';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->smallIncrements( 'id' );
            $table->unsignedInteger( 'id_department' )->nullable( true );
            $table->string('name', 50 )->comment( 'collaborator name' );
            $table->string( 'email', 120 )->comment( 'collaborator email' );
            $table->string( 'password', 120 );
            $table->string( 'role', 50 )->comment( 'admin,editor,analyst...' );
            $table->string( 'phone', 20 )->comment( 'collaborator phone' );
            $table->json( 'im' )->nullable()->comment( 'im like whatsapp, skype, zoom in key:value pairs' );
            $table->unsignedTinyInteger( 'status' )->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
