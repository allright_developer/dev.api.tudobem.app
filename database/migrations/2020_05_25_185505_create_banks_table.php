<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
    public $table = 'bank';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function ( Blueprint $table ) {
            $table->unsignedSmallInteger( 'id' )->unique('uq_bank_id')->comment( 'bank id according brazils central bank' );
            $table->string('label', 25 )->comment( 'bank Official name' );
            $table->unsignedTinyInteger( 'status' )->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
