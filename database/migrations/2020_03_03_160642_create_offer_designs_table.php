<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_design', function (Blueprint $table) {
            $table->unsignedInteger('id_offer' );

            $table->string('type', 15 )->default('card' )->comment('card,puzzle,crossword,...');
            $table->string('design', 15 )->default('default' )->comment('default,...');
            $table->timestamps();
            $table->unsignedTinyInteger( 'status' )->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_designs');
    }
}
