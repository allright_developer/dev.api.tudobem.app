<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100 )->comment( 'ONG name');
            $table->string('slug', 100 )->comment( 'ONG url slug');
            $table->string('description', 255 )->nullable()->comment( 'ONG brief description' );
            $table->json('social_data' )->nullable()->comment('ong civil and legal data');
            $table->timestamps();
            $table->unsignedInteger('status' )->default( 1 );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization');
    }
}
