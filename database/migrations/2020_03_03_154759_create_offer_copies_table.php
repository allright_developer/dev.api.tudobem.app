<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferCopiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_copy', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger( 'id_offer' );
            $table->json( 'text' )->comment( 'the copies in json format: h,b,f,btn[accept, decline]' );
            $table->timestamps();

            $table->unsignedTinyInteger( 'status' )->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_copies');
    }
}
