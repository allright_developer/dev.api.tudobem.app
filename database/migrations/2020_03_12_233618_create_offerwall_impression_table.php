<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferwallImpressionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offerwall_impression', function (Blueprint $table) {
            $table->string( 'hash','12' )->unique();
            $table->unsignedInteger('id_user' )->nullable()->comment( 'user id if it is a logged session');
            $table->json( 'offers' )->comment( 'offers of the offerwall' );
            $table->unsignedInteger( 'impression_start_at')->nullable()->comment( 'when the offerwall impression started' );
            $table->unsignedInteger( 'impression_end_at')->nullable()->comment( 'when the offerwall impression finished' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offerwall_impression', function (Blueprint $table) {
            //
        });
    }
}
