<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_offer', function (Blueprint $table) {
            $table->increments('id' );
            $table->string('hash', 9 )->unique()->comment('unique token offer id');
            $table->unsignedInteger('id_product' )->comment('product id');
            $table->string('label', 50 )->nullable()->comment( 'offer name');
            $table->string('description', 512 )->nullable()->comment('offer brief description');
            $table->unsignedSmallInteger('crypto' )->nullable()->comment('value in credibens' );
            $table->decimal('fiat',9, 2 )->nullable()->comment('value in fiat currency' );
            $table->dateTime( 'begin_at' )->comment('when the offer will be ACTIVATED' );
            $table->dateTime( 'finish_at' )->comment('when the offer will be DEACTIVATED' );
            $table->unsignedTinyInteger( 'status' )->default(2 )->comment('0=deactiv,1=active,2=awaiting' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_offer');
    }
}
