<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOfferCopyAlterTextToJsonCreateAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_copy', function (Blueprint $table) {
            $table->json( 'text' )->after('id_offer' )->comment('copy texts in json format' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_copy', function (Blueprint $table) {
            //
        });
    }
}
