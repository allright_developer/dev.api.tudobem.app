<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAngelGiftImageTable extends Migration
{
    public $table = 'angel_gift_image';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function ( Blueprint $table ) {
            $table->unsignedInteger('id_angel_gift' )->comment('gift id' );
            $table->string('name', 125 )->unique()->comment( 'image name in fs or cloud' );
            $table->string('description', 150 )->nullable()->comment( 'image brief description' );
            $table->unsignedInteger('storage')->default(1);
            $table->unsignedTinyInteger('status' )->comment('image status' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( $this->table, function (Blueprint $table) {
            //
        });
    }
}
