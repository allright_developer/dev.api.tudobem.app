<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function ( Blueprint $table ) {
            $table->increments('id' );// to make a settings history and rollback feature
            $table->unsignedInteger('created_by' )->nullable()->comment( 'user that created this config' );
            $table->json('conf' )->nullable()->comment( 'configuration settings' );
            $table->unsignedTinyInteger('status' )->default(1)->comment( 'it is allowed just 1 active settings' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
