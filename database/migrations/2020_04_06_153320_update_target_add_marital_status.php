<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTargetAddMaritalStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_target',function ( Blueprint $table ) {
            $table->json( 'children' )->nullable()->comment('number of children and their infos' )->after('economic' );
            $table->json( 'marital_status' )->nullable()->comment('1=single,2=married,3=serious_rel,null=all')->after('economic' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_target', function (Blueprint $table) {
            //
        });
    }
}
