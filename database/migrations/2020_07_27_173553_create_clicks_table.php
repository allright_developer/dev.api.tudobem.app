<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClicksTable extends Migration
{
    public $table = 'click';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table){

            $table->string('token', 32 )->comment('unique click id or transaction_id' );
            $table->unsignedTinyInteger('entity' )->default(1 )->comment( '1=offer,2=product, other see Mapper' );
            $table->unsignedInteger('id_entity' )->comment('entity id' );

            $table->json('params' )->nullable()->comment('additional params' );
            $table->dateTime( 'created_at' )->useCurrent();

            $table->unsignedTinyInteger( 'status' )->default( 2 );

            $table->index('entity', 'idx_click' );
            $table->unique('token', 'uq_click' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
