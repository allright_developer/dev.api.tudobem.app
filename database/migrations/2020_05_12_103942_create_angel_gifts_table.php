<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAngelGiftsTable extends Migration
{
    protected $table = 'angel_gift';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'id_category')->nullable()->comment( 'gift category' );
            $table->unsignedInteger( 'id_user_winner' )->nullable()->comment( 'user that got the prize' );
            $table->unsignedTinyInteger( 'place' )->default(1)->comment( 'the gift place in the ranking' );
            $table->string( 'label', 40 )->nullable( false )->comment( 'gift name' );
            $table->string( 'description', 155 )->nullable();
            $table->unsignedTinyInteger( 'month' )->nullable()->comment( 'vigency month of the gift' );
            $table->unsignedSmallInteger( 'year' )->nullable()->comment( 'vigency year of the gift' );
            $table->unsignedTinyInteger('status' )->default(1)->comment('1=active,0=closed,2=waiting' );

            $table->unique( ['month', 'year', 'status' ], 'uq_angel_gifts' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
