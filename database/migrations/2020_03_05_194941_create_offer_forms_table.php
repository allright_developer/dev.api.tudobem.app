<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_form', function (Blueprint $table) {
            $table->unsignedInteger('id_offer')->comment( 'offer that owns this form' );
            $table->json('fields' )->comment( 'array of fields: type,name,label,data,required,order...');
            $table->unsignedInteger('type' )->default(1)->comment('1=default,2=chat,3=dialog...' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_forms');
    }
}
