<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserExchangesTable extends Migration
{
    public $table = 'user_exchange';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->increments('id' );
            $table->unsignedInteger('id_product' )->comment('product to be exchanged with Credibens' );
            $table->unsignedInteger('id_user' )->comment( 'user purchasing the product' );
            $table->unsignedInteger('value_at_the_ask_time' )->nullable()->comment('value in credibens when the user asked for the product');
            $table->timestamps();
            $table->unsignedTinyInteger( 'status' )->comment( '0=cnld,1=delvd,2=wait,3=ongoing' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
