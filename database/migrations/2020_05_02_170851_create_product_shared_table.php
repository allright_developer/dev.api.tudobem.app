<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSharedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_shared', function (Blueprint $table) {
            $table->unsignedInteger( 'id_user' )->comment( 'user id' );
            $table->unsignedInteger( 'id_product' )->comment( 'product id' );
            $table->json( 'crypto_dist' )->nullable()->comment( 'how many crypto was dist to user and ong' );
            $table->unsignedTinyInteger( 'shared_on' )->default(1)->comment('1=fb,2=wtp,3=msgr,4=ptst' );
            $table->timestamp( 'shared_at' )->useCurrent()->comment( 'user share date' );

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_shared');
    }
}
