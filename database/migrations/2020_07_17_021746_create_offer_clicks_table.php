<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferClicksTable extends Migration
{
    public $table = 'offer_click';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create( $this->table, function (Blueprint $table) {
            $table->string('clkid', 32 )->nullable( false )->comment('unique click hash id' );
            $table->unsignedInteger('id_user' )->nullable()->comment('user that clickedm if there is one');
            $table->unsignedInteger('id_copy' )->nullable()->comment('text version');
            $table->unsignedInteger('id_adversion' )->nullable('advertise version');
            $table->string('id_offerwall', 16 )->nullable()->comment( 'offerwall hash id' );
            $table->dateTime('created_at' )->useCurrent();
            $table->dateTime('updated_at' )->nullable()->comment( 'time at status updating' );
            $table->unsignedTinyInteger('status' )->default(2)->comment( '0=canceled,1=accepted,2=waiting' );

            $table->unique('clkid', 'uq_offer_click' );
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
