<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAngelsTable extends Migration
{
    protected  $table = 'angel';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {

            $table->unsignedInteger( 'id_user' )->unique()->comment( 'user' );
            $table->unsignedTinyInteger( 'month' )->comment('month represented by its number: 1 to 12');
            $table->unsignedInteger( 'year' )->comment('year of counting' );
            $table->unsignedInteger( 'crypto_org' )->default( 0 )->comment( 'total crypto ong for the current month');
            $table->unsignedInteger( 'crypto_user' )->default(0 )->comment('total crypto user for the current month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angels');
    }
}
