<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserLogAddNotifiableReadAtFields extends Migration
{
    public $table = 'user_log';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( $this->table, function (Blueprint $table) {
            $table->timestamp('read_at' )->nullable()->after( 'data' )->comment('when the user was rid the notification' );
            $table->unsignedTinyInteger('notifiable' )->default(0)->after( 'data' )->comment('if this user log is notifiable=1' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( $this->table, function (Blueprint $table) {
            //
        });
    }
}
