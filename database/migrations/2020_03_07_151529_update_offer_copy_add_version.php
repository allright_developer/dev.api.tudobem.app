<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOfferCopyAddVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_copy', function (Blueprint $table) {
            $table->unsignedTinyInteger('ad_version' )
                ->after('text' )
                ->default(1)
                ->comment('the ad version that this copy belongs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_copy', function (Blueprint $table) {
            //
        });
    }
}
