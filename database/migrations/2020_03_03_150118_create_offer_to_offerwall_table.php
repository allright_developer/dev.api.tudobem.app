<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferToOfferwallTable extends Migration
{
    /**
     * Create the Offer store to Offerwall Module.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer', function (Blueprint $table) {
            $table->integerIncrements('id' );
            $table->unsignedTinyInteger( 'id_ad_publisher' )->nullable()->comment('if an offer comes from a platfrm, this field is used' );
            $table->string( 'id_offer_publisher', 64 )->nullable()->comment( 'offer id in the external provider');
            $table->string( 'name', 50 )->comment('Offer Name');
            $table->string( 'slug', 50 )->comment('Offer slug');
            $table->string( 'description', 512 )->comment('html Description');
            $table->string( 'url', 120 )->nullable()->comment('original url when the offer comes from a provider' );
            $table->dateTime( 'begin_at' )->useCurrent()->comment('when the Offer begins to run' );
            $table->dateTime( 'finish_at' )->nullable()->comment('when the Offer finishes' );

            $table->timestamps();

            $table->unsignedTinyInteger('status' )->default(1 );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer', function (Blueprint $table) {
            //
        });
    }
}
