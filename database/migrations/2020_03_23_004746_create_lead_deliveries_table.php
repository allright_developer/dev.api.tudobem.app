<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadDeliveriesTable extends Migration
{
    protected $table = 'lead_delivery';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function ( Blueprint $table ){
            $table->increments( 'id' );
            $table->string('entity' )->default('offer' )->comment('entity name to relate with lead');
            $table->string( 'id_entity' )->index( 'unq_lead_delivery' )->comment( 'entity id to relate with' );
            $table->json('periods' )->nullable()->comment('delivery periods in a offer_target array format');
            $table->string( 'transport', 10 )->default( 'crm' )->comment('aws,crm,ftp,ssh,mail...' );
            $table->json( 'frequency' )->nullable()->comment('by,how_many,limit');
            $table->unsignedInteger( 'how_many' )->nullable()->comment( 'how many leads to send to the endpoint' );
            $table->unsignedInteger('total_sent' )->nullable()->comment('how many leads already sent to the endpoint' );
            $table->json('conf' )->comment( 'delivery settings according to the transport type' );

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
