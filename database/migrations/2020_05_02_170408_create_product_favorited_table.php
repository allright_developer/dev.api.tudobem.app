<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductFavoritedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_favorited', function (Blueprint $table){

            $table->unsignedInteger( 'id_product' )->comment( 'product id' );
            $table->unsignedInteger( 'id_user' )->comment( 'user id' );
            $table->unsignedTinyInteger( 'favorited' )->comment( 'user id' );
            $table->timestamps();

            $table->unique( ['id_user', 'id_product' ], 'uq_product_favorited' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_favorited');
    }
}
