<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferPayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_payout', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedInteger('id_offer');
            $table->char('payout_type', 3 )->default('CPL' )->comment('CPL,CPA, CPS, CPC' );
            $table->unsignedTinyInteger('payout_in' )->default(1)->comment( 'payout in: 1=decimal,2=percentage' );
            $table->decimal('payout', 7, 2 )->comment('val/percent depending on payout_in' );
            $table->unsignedInteger('payout_user_crypto' )->default(3)->comment( 'user payout in credibens' );
            $table->unsignedInteger('payout_ong_crypto' )->default(3)->comment( 'ONG payout in credibens' );

            $table->timestamps();
            $table->unsignedTinyInteger( 'status' )->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_payouts');
    }
}
