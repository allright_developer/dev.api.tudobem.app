<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_log', function (Blueprint $table) {
            $table->unsignedInteger('id_user')->comment( 'user source log generator');
            $table->unsignedTinyInteger( 'action_type' )->comment('user action type, view ValueObject Mapper' );
            $table->string('description' )->nullable()->comment( 'log description' );
            $table->json('data' )->nullable()->comment('json additional data' );
            $table->timestamp('created_at' )->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_log');
    }
}
