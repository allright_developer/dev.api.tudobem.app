<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_survey')->comment('survey id that question belongs to' );
            $table->unsignedTinyInteger('order')->default(0)->comment('question sequential order in the Survey' );
            $table->string('type',10)->default('radio')->comment('radio,checkbox,text,puzzle' );
            $table->string('enunciation', 150 )->comment('question description' );
            $table->integer('credits')->nullable()->default(0)->comment('amount to give to user when responding the question');
            $table->integer('credits_beneficent')->nullable()->default(0)->comment('amount to give to Organization when user responds');
            $table->json('attributes' )->nullable(false)->comment('name,label,mask,validate,behavior,maxlength,placeholder');
            $table->timestamps();
            $table->unsignedTinyInteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question');
    }
}
