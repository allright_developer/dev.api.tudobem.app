<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCreditsTable extends Migration
{
    public $table = 'user_credit';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->string('token', 32 )->comment('unique entity token' );
            $table->unsignedInteger('id_user' )->nullable(false)->comment('user id');
            $table->unsignedInteger('crypto' )->default(0)->comment('Créditos do Bem amount');
            $table->unsignedDecimal('fiat',10, 2 )->default(0.00)->comment('Fiat money amount' );
            $table->timestamps();

            $table->unique('token', 'uq_user_credit' );
//            $table->foreign('id_user','fk_user_credit')->references('id')->on('users' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
