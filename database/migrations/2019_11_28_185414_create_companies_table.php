<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token', 32 )->unique()->nullable(false)->comment('public unique hash identifier');
            $table->unsignedInteger('id_category' )->nullable(true)->comment('company category' );
            $table->string('cnpj', 20 )->nullable(true )->comment('official brazilian company doc' );
            $table->string('label', 70 )->nullable(false)->unique();
            $table->string('slug', 70 )->nullable(false)->comment('friendly url name');
            $table->string('description', 255 )->nullable(true);
            $table->timestamps();
            $table->unsignedTinyInteger('status' )->default(1)->comment('0=soft-del,1=ok,2=pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
