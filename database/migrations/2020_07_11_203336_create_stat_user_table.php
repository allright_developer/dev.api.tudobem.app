<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatUserTable extends Migration
{
    public $table = 'stat_user';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function ( Blueprint $table ) {
            $table->string( 'period' )->nullable()->comment( 'PT15M, PT30M, PT1H, P1D, P7D, P30D' );
            $table->dateTime( 'start_at' )->comment( 'stat start date' );
            $table->dateTime( 'end_at' )->comment( 'stat end date' );
            $table->unsignedInteger( 'total' )->nullable();

            $table->unique(['start_at', 'end_at', 'period']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stat_user');
    }
}
