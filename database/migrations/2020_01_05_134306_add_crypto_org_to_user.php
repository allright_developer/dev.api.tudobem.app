<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCryptoOrgToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_credit', function (Blueprint $table) {
            $table->unsignedInteger('crypto_org' )
                  ->after('crypto' )->default(0)
                  ->comment( 'cryptos that user earned and is reserved to donate' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_credit', function (Blueprint $table) {
            //
        });
    }
}
