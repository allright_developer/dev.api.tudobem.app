<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOngCategoriesTable extends Migration
{
    public $table = 'ong_category';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->increments( 'id' );
            $table->string('label', 35 )->comment( 'ONG name');
            $table->string('slug', 35 )->comment( 'ONG friendly uri');
            $table->string('description', 170 )->nullable();
            $table->unsignedTinyInteger('status' )->default(1 );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ong_categories');
    }
}
