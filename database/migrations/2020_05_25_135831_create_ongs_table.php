<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOngsTable extends Migration
{
    public $table = 'ong';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table){
            $table->increments( 'id' );
            $table->unsignedInteger('id_ong_category' )->nullable( false )->comment( 'ONG category' );
            $table->string( 'label', 50 )->unique( 'uq_ong_name')->comment( 'ONG name' );
            $table->string( 'slug', 50 )->comment( 'ONG friendly name' );
            $table->text( 'description' )->nullable()->comment( 'ONG html description' );
            $table->string('social_id', 30 )->nullable()->comment( 'cnpj, cpf or other legal social id number' );
            $table->json( 'address' )->nullable()->comment('logradouro,cep,number etc' );
            $table->json( 'accounts' )->nullable()->comment('array of bank accounts data' );
            $table->json( 'images' )->nullable()->comment('array of images' );
            $table->unsignedTinyInteger('status' )->default(1)->comment('1=active,0=blocked' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
