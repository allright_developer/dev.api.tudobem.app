<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table)
        {
            $table->unsignedInteger('id', true );
            $table->string( 'token', 64 )->unique()->comment( 'unique product token' );
            $table->unsignedInteger( 'id_category' )->nullable( false )->comment( 'product category to organize and use in filter');
            $table->unsignedInteger( 'id_company' )->nullable()->comment( 'product manufacturer to organize and use in filter');

            $table->string( 'label', 120 )->unique()->comment( 'unique product name');
            $table->string( 'slug', 120 )->unique()->comment( 'unique product friendly system name');
            $table->string( 'description', 512 )->comment( 'Product Description' );

            $table->unsignedSmallInteger('crypto' )->default(20)->comment( 'valor em Credibens para troca');
            $table->unsignedSmallInteger('crypto_cashback_user' )->default(2)->comment( 'Credibens que o user ganha ao adquirir o produto');
            $table->unsignedSmallInteger('crypto_cashback_org' )->default(2)->comment( 'Credibens que a ONG ganha quando o prod é adquirido por um user');
            $table->unsignedDecimal('fiat', 8, 2 )->default(0)->comment( 'valor em Moeda Corrente para compra' );

            $table->unsignedTinyInteger( 'status' )->default(1)->comment('product status' );

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
