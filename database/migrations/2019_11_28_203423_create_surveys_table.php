<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('survey', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_category' )->nullable(false)->comment('survey category');
            $table->unsignedInteger('id_company' )->nullable(true)->default(null)->comment('company id' );
            $table->unsignedInteger('id_product' )->nullable(true)->default(null)->comment('product id' );
            $table->string('name', 50 )->nullable(false)->comment( 'Survey name' );
            $table->string('slug', 50 )->nullable(false)->comment( 'Survey friendly url name' );
            $table->string('description', 255 )->nullable(true)->comment( 'Survey description' );
            $table->unsignedTinyInteger('status' )->default(1)->comment( '' );
            $table->dateTime('begin_at' )->default(null)->comment( 'survey publish date' );
            $table->dateTime('finish_at' )->default(null)->comment( 'survey finish date' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
