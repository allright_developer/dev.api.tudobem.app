<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_target', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_offer')->unique();
            $table->unsignedInteger('gender')->nullable()->comment('1=F,2=M,null=all' );
            $table->unsignedInteger('age_min')->default(13 )->nullable()->comment('minimum age' );
            $table->unsignedInteger('age_max')->default(80 )->nullable()->comment('maximum age to target' );
            $table->json('economic' )->nullable()->comment('social economic class: a,b,c,d,e' );
            $table->json('geo')->nullable()->comment('geo coords to constrain an Offer' );
            $table->json('mobile_code')->nullable()->comment('mobile DDD range or list to show the Offer' );
            $table->json('running_period')->nullable()->comment('a json list of allowed running hours and days' );
            $table->timestamps();

            $table->unsignedTinyInteger( 'status' )->default(1 );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_targets');
    }
}
