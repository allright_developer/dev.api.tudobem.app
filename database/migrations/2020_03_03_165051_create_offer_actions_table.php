<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferActionsTable extends Migration
{
    /**
     * An Offer Action consists of what to do when an user accepts the Offer (open a url, send the Form data etc).
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_action', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_offer');
            $table->unsignedTinyInteger('type' )->default(1 )->comment( '1=url, 2=form');
            $table->json( 'conf' )->comment('json conf of the offer action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_actions');
    }
}
