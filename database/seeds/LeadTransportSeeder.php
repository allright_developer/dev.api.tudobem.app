<?php

use Illuminate\Database\Seeder;

class LeadTransportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Lead\LeadDelivery::firstOrCreate([
            'id_entity' => 3,
            'entity' => 'offer', // defaults to offer
            'periods' => null,
            'transport' => 'http-post', // http-form-post, aws, crm, ftp, ssh, mail
            'frequency' => [ 'by' => 'M', 'how_many' => 5, 'limit' => 10 ], // M=minute,H=hour
            'how_many' => 200, // how many leads to sent in total, null represents no limit
            'total_sent' => 0, // total sent since the last sending
            'conf' => [
                'data' => [
                    'mapping' => [
                        'name' => 'nome', 'cep' => 'cep', 'gender' => 'sexo', 'periodo_contato' => 'periodo_contato',
                        'apelido' => 'nickname', 'estacao_preferida' => 'estacao_preferida'
                    ]
                ],
                'transport_settings' => [
                    'params'   => ['key' => 'abcdefghijklmnopqrstuvxz'],
                    'endpoint' => 'http://localhost/'
                ]
            ]
        ]);
    }
}
