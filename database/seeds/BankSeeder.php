<?php

use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Bank::firstOrCreate([
            'id' => 1, 'label' => 'do Brasil'
        ]);

        \App\Model\Bank::firstOrCreate([
            'id' => 33, 'label' => 'Santander'
        ]);

         \App\Model\Bank::firstOrCreate([
            'id' => 74, 'label' => 'Safra'
        ]);
         \App\Model\Bank::firstOrCreate([
            'id' => 77, 'label' => 'Inter'
        ]);

         \App\Model\Bank::firstOrCreate([
            'id' => 104, 'label' => 'Caixa Econômica Federal'
        ]);
         \App\Model\Bank::firstOrCreate([
            'id' => 237, 'label' => 'Bradesco'
        ]);

        \App\Model\Bank::firstOrCreate([
            'id' => 318, 'label' => 'BMG'
        ]);
        \App\Model\Bank::firstOrCreate([
            'id' => 341, 'label' => 'Itau'
        ]);
        \App\Model\Bank::firstOrCreate([
            'id' => 260, 'label' => 'Nubank'
        ]);
    }
}
