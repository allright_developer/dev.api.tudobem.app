<?php

use Illuminate\Database\Seeder;

class LeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for( $i = 1; $i < 5; $i++ ){
            $faker = Faker\Factory::create('pt_BR' );
            $faker->addProvider( new \JansenFelipe\FakerBR\FakerBR( $faker ) );
            \App\Model\Lead\Lead::firstOrCreate([
                'token' => \App\Security\Token::generate( 8 ),
                'id_user' => \rand(1, 100 ),
                'generated_by' => 'offer',
                'id_entity' => \Illuminate\Support\Arr::random([1,3]),
                'data' => [
                    'data' => [
                        'cep' => $this->fakerCep(),
                        'cpf' => $faker->cpf,
                        'gender' => \Illuminate\Support\Arr::random( ['F','M'] ),
                        'apelido' => $faker->userName,
                        'periodo_contato' => \Illuminate\Support\Arr::random([ 'manha', 'tarde', 'noite' ] ),
                        'estacao_preferida' => \Illuminate\Support\Arr::random( [ 'consolacao', 'paulista', 'republica' ] ),
                    ]
                ]
            ]);
        }
    }

    public function fakerCep()
    {
        return str_pad( rand( 00000,10999), 5, '0', \STR_PAD_LEFT).'-'.
            str_pad(rand(000,999), 3, '0', \STR_PAD_LEFT);
    }
}
