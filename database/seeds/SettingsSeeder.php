<?php
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        \App\Model\Settings::query()->where('status', 1 )->update(['status', 0 ] );
        \App\Model\Settings::query()->firstOrCreate([
            'conf' => [
                'bonus' => [
                    'signup' => 5,
                    'memberGetMember' => 5
                ],
                'taxes' => 16
            ],
            'created_by' => 1,
            'status' => 1
        ]);
    }
}
