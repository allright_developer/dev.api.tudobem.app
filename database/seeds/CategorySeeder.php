<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bel = \App\Model\Category::firstOrCreate([
            'label' => 'Beleza',
            'slug' => \Illuminate\Support\Str::slug('beleza' ),
            'description' => 'Produtos e cuidados Estéticos'
        ]);

        $barba = \App\Model\Category::firstOrCreate([
            'id_category' => $bel->id,
            'label' => 'Barba e Cabelo',
            'slug' => \Illuminate\Support\Str::slug('Barba e Cabelo' ),
            'description' => 'Produtos para Barba, Cabelo e Bigode'
        ]);

        $cursos = \App\Model\Category::firstOrCreate([
            'label' => 'Cursos',
            'slug' => \Illuminate\Support\Str::slug('cursos' ),
            'description' => 'Cursos'
        ]);

        $graduação = \App\Model\Category::firstOrCreate([
            'label' => 'Graduação Superior',
            'id_category' => $cursos->id,
            'slug' => \Illuminate\Support\Str::slug('Graduação Superior' ),
            'description' => 'Graduação Superior, Bacharelado e afins'
        ]);

        $cartaoPresente = \App\Model\Category::firstOrCreate([
            'label' => 'Cartão Presente',
            'slug' => \Illuminate\Support\Str::slug('Cartão Presente' ),
            'description' => 'Cartão Presente, Vale Compras, Créditos em Serviços'
        ]);

        $cab = \App\Model\Category::firstOrCreate([
            'label' => 'Pele, Cabelos e Unhas',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('Pele, Cabelos e Unhas' ),
            'description' => 'Cremes e tratamentos Cosméticos para a Pele, Cabelos e Unhas'
        ]);

        $cab = \App\Model\Category::firstOrCreate([
            'label' => 'Hidratantes',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('Hidratantes' ),
            'description' => 'Cremes Hidratantes para a Pele'
        ]);

        $cab = \App\Model\Category::firstOrCreate([
            'label' => 'Rejuvenescedores',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('Rejuvenescedores' ),
            'description' => 'Cremes e Serums antirrugas, para tratar linhas de expressão e rejuvenescer a pele.'
        ]);

        $cab = \App\Model\Category::firstOrCreate([
            'label' => 'Manchas na Pele',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('Manchas e Melasma' ),
            'description' => 'Cremes Clareadores e para tratar manchas na pele'
        ]);

        $cab = \App\Model\Category::firstOrCreate([
            'label' => 'Manchas na Pele',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('Manchas e Melasma' ),
            'description' => 'Cremes Clareadores e para tratar manchas na pele'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Moda e Vestuário Feminino',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('Moda e Vestuário Feminino' ),
            'description' => 'Roupas e Acessórios de Moda Feminina'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Moda e Vestuário Masculino',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('Moda e Vestuário Masculino' ),
            'description' => 'Roupas e Acessórios de Moda Masculina'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Maquiagem para o Rosto',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('maquiagem para o rosto' ),
            'description' => 'Maquiagens para o Rosto, bases, pós, pincéis, lápis.'
        ]);
        \App\Model\Category::firstOrCreate([
            'label' => 'Maquiagem para Olhos e Sobrancelhas',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('maquiagem para o olhos e sobrancelhas' ),
            'description' => 'Maquiagens para o Contorno dos Olhos, Sobrancelhas e Supercílios.'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Batom e Lábios',
            'id_category' => $bel->id,
            'slug' => \Illuminate\Support\Str::slug('Batom e Lábios' ),
            'description' => 'Batons líquidos, em matte, lápis de contorno para os lábios e similares.'
        ]);

        $perf =\App\Model\Category::firstOrCreate([
            'label' => 'Perfumes',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Perfumes' ),
            'description' => 'Perfumes e loções em geral.'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Perfumes Masculinos',
            'id_category' => $perf->id,
            'slug' => \Illuminate\Support\Str::slug('Perfumes Masculinos' ),
            'description' => 'Perfumes exclusivos para os Homens'
        ]);



        \App\Model\Category::firstOrCreate([
            'label' => 'Perfumes Femininos',
            'id_category' => $perf->id,
            'slug' => \Illuminate\Support\Str::slug('Perfumes Femininos' ),
            'description' => 'Perfumes exclusivos para o público Feminino.'
        ]);

        $casa = \App\Model\Category::firstOrCreate([
            'label' => 'Utensílios Domésticos',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Utensílios Domésticos' ),
            'description' => 'Utensílios Domésticos em Geral'
        ]);

        $cmb = \App\Model\Category::firstOrCreate([
            'label' => 'Cama, Mesa e Banho',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Cama Mesa Banho' ),
            'description' => 'Tudo para Cama, Mesa e Banho'
        ]);
        \App\Model\Category::firstOrCreate([
            'label' => 'Ferramentas',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Ferramentas' ),
            'description' => 'Ferramentas em Geral.'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Saúde e Bem Estar',
            'slug' => \Illuminate\Support\Str::slug('saude e bem estar' ),
            'description' => 'Saúde, Remédios, Tratamentos, dicas e serviços de saúde.'
        ]);

        $cat = \App\Model\Category::firstOrCreate([
            'label' => 'Alimentos Básicos',
            'slug' => \Illuminate\Support\Str::slug('Alimentos Básicos' ),
            'description' => 'Alimentos Básicos para o dia-a-dia.'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Chocolates',
            'id_category' => $cat->id,
            'slug' => \Illuminate\Support\Str::slug('Chocolates' ),
            'description' => 'Chocolates doces, amargos, meio-amargos, bombons, barras de chocolates.'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Bebês',
            'slug' => \Illuminate\Support\Str::slug('bebes' ),
            'description' => 'Tudo para cuidar bem do seu Bebê'
        ]);

        $cat = \App\Model\Category::firstOrCreate([
            'label' => 'Pets/Animais de Estimação',
            'slug' => \Illuminate\Support\Str::slug('Pets Animais de Estimação' ),
            'description' => 'Brinquedos, ração, sachês, abrigos, roupinhas, shampoos e muito mais para o seu filhote.'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Ração, Sachês, Enlatados',
            'id_category' => $cat->id,
            'slug' => \Illuminate\Support\Str::slug('Ração, Sachês, Enlatados' ),
            'description' => 'Todos os tipos de alimentos para cães, gatos, pássaros e outros animais de estimação.'
        ]);

        $eletronicos = \App\Model\Category::firstOrCreate([
            'label' => 'Eletrônicos',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Eletrônicos' ),
            'description' => 'Celulares, Tablets, notebooks, Fones de Ouvidos, Watchers, acessórios.'
        ]);

        $celulares = \App\Model\Category::firstOrCreate([
            'label' => 'Celulares',
            'id_category' => $eletronicos->id,
            'slug' => \Illuminate\Support\Str::slug('Celulares' ),
            'description' => 'Celulares e acessórios para aparelhos celulares'
        ]);

        $android = \App\Model\Category::firstOrCreate([
            'label' => 'Android',
            'id_category' => $celulares->id,
            'slug' => \Illuminate\Support\Str::slug('Android' ),
            'description' => 'Celulares com o Sistema Operacional Android'
        ]);

        $apple = \App\Model\Category::firstOrCreate([
            'label' => 'Apple',
            'id_category' => $celulares->id,
            'slug' => \Illuminate\Support\Str::slug('Apple' ),
            'description' => 'Celulares, Tablets e acessórios da Apple'
        ]);

        $tel = \App\Model\Category::firstOrCreate([
            'label' => 'Telefonia',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Telefonia' ),
            'description' => 'Celulares, acessórios e peças para telefones'
        ]);

        $Operadoras = \App\Model\Category::firstOrCreate([
            'label' => 'Operadoras de Celular',
            'id_category' => $tel->id,
            'slug' => \Illuminate\Support\Str::slug('Operadoras de Celular' ),
            'description' => 'Operadoras de Telefonia Celular e Fixa'
        ]);

        $limpeza = \App\Model\Category::firstOrCreate([
            'label' => 'Produtos de Limpeza',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Produtos de Limpeza' ),
            'description' => 'Produtos de Limpeza em Geral'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Sabão em Pó',
            'id_category' => $limpeza->id,
            'slug' => \Illuminate\Support\Str::slug('Sabão em Pó' ),
            'description' => 'Sabão em Pó, lavar roupas'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Detergente',
            'id_category' => $limpeza->id,
            'slug' => \Illuminate\Support\Str::slug('Detergente' ),
            'description' => 'Detergentes e Lava-Louças'
        ]);

        \App\Model\Category::firstOrCreate([
            'label' => 'Amaciantes e Tira-Manchas',
            'id_category' => $limpeza->id,
            'slug' => \Illuminate\Support\Str::slug('Amaciantes e Tira-Manchas' ),
            'description' => 'Amaciantes e tira-manchas de roupas e tecidos'
        ]);

        $hig = \App\Model\Category::firstOrCreate([
            'label' => 'Higiene Pessoal',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Higiene Pessoal' ),
            'description' => 'Produtos de Higiene Pessoal, como sabonetes, esponjas, creme dental, escova de dente, enxaguante bucal, álcool em gel.'
        ]);

        $seg = \App\Model\Category::firstOrCreate([
            'label' => 'Seguros',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Seguros' ),
            'description' => 'Seguros para Carros, motos, casa, pessoal, celular.'
        ]);

        $fin = \App\Model\Category::firstOrCreate([
            'label' => 'Finanças',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Financas' ),
            'description' => 'Bancos, Investimentos.'
        ]);

        $sdiv = \App\Model\Category::firstOrCreate([
            'label' => 'Diversão',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Diversão' ),
            'description' => 'Cinema, Shows, atrações artísticas em geral.'
        ]);

        $educacao = \App\Model\Category::firstOrCreate([
            'label' => 'Educação',
            'id_category' => null,
            'slug' => \Illuminate\Support\Str::slug('Educação' ),
            'description' => 'Educação, Cursos, Formação Acadêmica'
        ]);

    }
}
