<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * run the google cloud sql proxy: ./cloud_sql_proxy -instances=long-octane-249221:southamerica-east1:tudobem=tcp:3306
     * @return void
     */
    public function run()
    {
         $this->call([
             OngCategorySeeder::class,
             CategorySeeder::class,
             OfferSeeder::class
//             CollaboratorSeeder::class,
             /*LeadSeeder::class*/
             /*UserDataSeeder::class*/
//             SettingsSeeder::class,
//             CompanySeeder::class,
         ]);
    }
}
