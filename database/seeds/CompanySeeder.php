<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = \App\Model\Company::query()->firstOrCreate([
            'label' => 'Allright Prestação de Serviços em Informática LTDA',
            'cnpj'  => '10.672.695/0001-01'
        ],
        [
            'slug'  => \Illuminate\Support\Str::slug( 'Allright Prestação de Serviços em Informática LTDA' ),
            'description' => 'Empresa de TI',
            'id_category' => '1',
            'token' => \str_replace('-','', substr( \Illuminate\Support\Str::uuid(),0,19 ))
        ]);
    }
}
