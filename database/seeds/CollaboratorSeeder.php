<?php

use Illuminate\Database\Seeder;

class CollaboratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Collaborator::query()->firstOrCreate([
            'email' => 'adriano@venddi.com.br'
        ],[
            'name' => 'Adriano Alves',
            'phone' => '11971783716',
            'password' => bcrypt('venddi20' ),
            'role' => 'admin',
            'id_department' => 1
        ]);
    }
}
