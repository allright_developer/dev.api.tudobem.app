<?php
use Illuminate\Database\Seeder;

class OngCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\OngCategory::query()->firstOrCreate([
            'label' => 'Crianças e Adolescentes',
            'slug' => \Illuminate\Support\Str::slug( 'Crianças e Adolescentes' ),
            'description' => 'Proteção e Amparo à Criança e Adolescente em Situação de Risco',
        ]);
        \App\Model\OngCategory::query()->firstOrCreate([
            'label' => 'Mulheres',
            'slug' => \Illuminate\Support\Str::slug( 'Mulheres' ),
            'description' => 'Proteção e Amparo às Mulheres',
        ]);
        \App\Model\OngCategory::query()->firstOrCreate([
            'label' => 'Idosos',
            'slug' => \Illuminate\Support\Str::slug( 'Idosos' ),
            'description' => 'Proteção, Abrigo e Amparo aos Idosos',
        ]);
        \App\Model\OngCategory::query()->firstOrCreate([
            'label' => 'Animais Abandonados',
            'slug' => \Illuminate\Support\Str::slug( 'Animais Abandonados' ),
            'description' => 'Proteção, Abrigo e Amparo aos Animais Abandonados e para Adoção',
        ]);

        \App\Model\OngCategory::query()->firstOrCreate([
            'label' => 'Saúde',
            'slug' => \Illuminate\Support\Str::slug( 'Saude' ),
            'description' => 'Ajuda Humanitária para o Combate à Epidemias e Pandemias, doenças relacionadas à Pobreza e Violência',
        ]);
        \App\Model\OngCategory::query()->firstOrCreate([
            'label' => 'Educação',
            'slug' => \Illuminate\Support\Str::slug( 'Educação' ),
            'description' => 'Educação e Alfabetização',
        ]);
        \App\Model\OngCategory::query()->firstOrCreate([
            'label' => 'LGBTQ+',
            'slug' => \Illuminate\Support\Str::slug( 'LGBTQ' ),
            'description' => 'Amparo e Proteção à população LGBTQ',
        ]);
        \App\Model\OngCategory::query()->firstOrCreate([
            'label' => 'Ajuda Humanitária',
            'slug' => \Illuminate\Support\Str::slug( 'Ajuda Humanitária' ),
            'description' => 'Projetos Diversificados de Ajuda Humanitária e Direitos Humanos',
        ]);

    }
}
