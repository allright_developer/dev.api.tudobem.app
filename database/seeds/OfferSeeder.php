<?php

use Illuminate\Database\Seeder;

class OfferSeeder extends Seeder
{
    /**
     * Testing queries:
     * truncate `offer_action`; truncate `offer`; truncate `offer_asset`; truncate `offer_copy`; truncate `offer_design`; truncate `offer_payout`; truncate `offer_target`;
     * SELECT * FROM `offer_target` where JSON_CONTAINS( economic,'"c"' ) AND JSON_CONTAINS( running_period->"$[2]",'11' ) AND JSON_CONTAINS( geography_mobile_code, '13')
     * SELECT * FROM
     *     `offer_target`
     * WHERE
     *     ( economic is null OR JSON_CONTAINS( economic,'"c"' ) )
     *     AND ( running_period is null OR JSON_CONTAINS( running_period->"$[2]",'11' ) ) -- insert here a variable replacing 2 to represent the day
     *     AND ( geography_mobile_code is null OR JSON_CONTAINS( geography_mobile_code, '13'))
     *     AND ( geography_cep is null OR JSON_CONTAINS(geography_cep, '"04905000"') )
     * @return void
     */
    public function run()
    {
        $afilio = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'Afilio',
            'description' => 'Plataforma de Campanhas Multipayout e Cupons'
        ]);

        $lomadee = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'Lomadee',
            'description' => 'Plataforma de Campanhas Multipayout, Cupons e Marketplace do grupo buscapé'
        ]);

        $actionPay = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'ActionPay',
            'description' => 'Plataforma de Campanhas Multipayout, Cupons e Marketplace sediada na Rússia'
        ]);

        $rakuten = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'Rakuten BR',
            'description' => 'Plataforma de Campanhas, Cupons e Marketplace'
        ]);

        $hotmart = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'Hotmart',
            'description' => 'A Maior Plataforma de Cursos e Produtos Digitais'
        ]);

        $amazon = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'Amazon',
            'description' => 'Plataforma de Afiliados da Amazon'
        ]);

        $amazon = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'Afiliace',
            'description' => 'Plataforma de Afiliados'
        ]);
        $adzappy = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'Adzappy',
            'description' => 'Plataforma de Afiliados'
        ]);
        $dgmax = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'DGMax',
            'description' => 'Plataforma de Afiliados'
        ]);

        $anunciar = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'Anunciar Midia',
            'description' => 'Plataforma de Afiliados'
        ]);
        $monetizze = \App\Model\AdPublisher::firstOrCreate([
            'label' => 'Monetizze',
            'description' => 'Plataforma de Produtos Físicos'
        ]);

        /*$offer = \App\Model\Offerwall\Offer::firstOrCreate([
            'id_publisher' => $lomadee->id,
            'id_offer_publisher' => 10,
            'name' => 'BeautyBox 40% Relâmpago',
            'slug' => \Illuminate\Support\Str::slug( 'BeautyBox 40% Relâmpago' ),
            'description' => 'Oferta relâmpago de 40% BeautyBox em toda linha de Perfumes',
//            'url' => 'https://www.sejaumarevendedora.boticario.com.br/Conteudo/revendedor/cadastro/default.aspx',
            'url' => null,
            'order' => 5,
            'begin_at' => '2020-03-05',
            'finish_at' => '2022-03-05'
        ]);

        $offerPayout = \App\Model\Offerwall\OfferPayout::firstOrCreate([
            'id_offer' => $offer->id,
            'payout_type' => 'CPA',
            'payout_in' => 2, // 1=decimal,2=Percentage
            'payout' => 5.5,
            'payout_user_crypto' => 125,
            'payout_ong_crypto' => 50,
        ]);

        $offerAsset = \App\Model\Offerwall\OfferAsset::firstOrCreate([
            'id_offer' => $offer->id,
            'name' => 'beauty-box-40.jpg',
            'url' => 'https://tbb.vtexassets.com/assets/vtex.file-manager-graphql/images/bbx_slider-semana-do-consumidor-necessaire-recheado_v2___4241dd87fedba32a217cdb294d21f382.gif',
            'description' => 'Relâmpago Beautybox 40%',
            'type' => \App\Mapper\OfferAsset::TYPE_IMG['id'],
            'storage' => \App\Mapper\OfferAsset::FS_AWS['id']
        ]);

        $texts = [
             'h' => 'Relâmpago Beautybox 40%' , // h
             'b' => 'Leve agora com até <b>40% de Desconto</b>! Você ainda Ganha Uma Necessaire cheia de Brindes Exclusivos da Beauty. Aproveite! <b>Oferta só vai até às 15h.</b>', // b
             'f' => 'Oferta válida somente das <b>11h às 15h </b>', // f
             'btn' => ['accept' => 'quero 40% OFF', 'decline' => 'prefiro comprar sem desconto' ] , // btn
        ];

        \App\Model\Offerwall\OfferCopy::firstOrCreate([
            'id_offer' => $offer->id,
            'text' => $texts
        ]);

        \App\Model\Offerwall\OfferDesign::firstOrCreate([
            'id_offer' => $offer->id
        ]);

        \App\Model\Offerwall\OfferAction::firstOrCreate([
            'id_offer' => $offer->id,
            'type' => 1,
            'conf' => [
                'url' => 'https://www.beautybox.com.br/'
            ]
        ]);

        \App\Model\Offerwall\OfferTarget::firstOrCreate([
            'id_offer' => $offer->id,
            'gender' => null,
            'age_min' => 18,
            'age_max' => 65,
            'economic' => ['d', 'b', 'c', 'e'],
            'geo' => null, // in meters
            'mobile_code' => null,
            'running_period' => [
                 [ 11,12,14,15 ], [ 11,12,14,15 ], [ 11,12,14,15 ], [ 11,12,14,15 ],
                 [ 11,12,14,15 ], [ 11,12,14,15 ], [ 11,12,14,15 ]
                ]
            ]
        );

        // SECOND offer
        $offer = \App\Model\Offerwall\Offer::firstOrCreate([
            'id_publisher' => $lomadee->id,
            'id_offer_publisher' => 11,
            'name' => 'Galaxy A10 com 30% OFF',
            'slug' => \Illuminate\Support\Str::slug( 'Galaxy A10 com 30% OFF' ),
            'description' => 'Smartphone Galaxy A10 com 30% OFF na Americanas <b class="text-orange">Só esta Semana</b>',
            'url' => null,
            'order' => 6,
            'begin_at' => '2020-03-05',
            'finish_at' => '2020-03-12'
        ]);

        $offerPayout = \App\Model\Offerwall\OfferPayout::firstOrCreate([
            'id_offer' => $offer->id,
            'payout_type' => 'CPA',
            'payout_in' => 2,
            'payout' => 7.73,
            'payout_user_crypto' => 229,
            'payout_ong_crypto' => 200,
        ]);

        $offerAsset = \App\Model\Offerwall\OfferAsset::firstOrCreate([
            'id_offer' => $offer->id,
            'name' => 'ec0fa23b.jpeg',
            'url' => 'https://images-americanas.b2w.io/produtos/01/00/oferta/134253/9/134253978_1SZ.jpg',
            'description' => 'Samsung Galaxy A10 é na Americanas',
            'type' => \App\Mapper\OfferAsset::TYPE_IMG['id'],
            'storage' => \App\Mapper\OfferAsset::FS_AWS['id']
        ]);

        $texts = [
             'h' => 'Pega 30% neste Galaxy A10 por Tempo Limitado',
             'b' => 'O <b class="text-primary">Samsung Galaxy A10</b> é um Smartphone completo, com muitos recursos para você ter uma experiência incrível. Visualmente moderno, com tela infinita de LCD TFT de 6.2”.',
             'f' => null, // footer texts
             'btn' => ['accept' => 'pegar Galaxy A10 com 30% OFF', 'decline' => 'depois' ], // button texts
        ];

        \App\Model\Offerwall\OfferCopy::firstOrCreate([
            'id_offer' => $offer->id,
            'text' => $texts
        ]);

        \App\Model\Offerwall\OfferDesign::firstOrCreate([
            'id_offer' => $offer->id
        ]);

        \App\Model\Offerwall\OfferAction::firstOrCreate([
            'id_offer' => $offer->id,
            'type' => 1,
            'conf' => [
                'url' => 'http://oferta.vc/v2/71b7afc744'
            ]
        ]);

        \App\Model\Offerwall\OfferTarget::firstOrCreate([
            'id_offer' => $offer->id,
            'gender' => null,
            'age_min' => 18,
            'age_max' => 70,
            'economic' => [ 'b', 'c', 'd' ],
            'geo' => [ 'lat' => -23.5305978, 'lng' => -46.6590878, 'radius' => 20000 ],
            'mobile_code' => null,
            'running_period' => null
        ]);

        // THIRD offer
        $offer = \App\Model\Offerwall\Offer::firstOrCreate([
            'id_publisher' => $afilio->id,
            'id_offer_publisher' => 12,
            'name' => 'Galaxy A50 com Bônus de R$ 200 Reais',
            'slug' => \Illuminate\Support\Str::slug( 'Galaxy A50 com Bônus de R$ 200 Reais' ),
            'description' => 'Smartphone <b class="text-orange">Samsung Galaxy A50</b> 128GB Dual Chip Android 9.0 Tela 6,4" Octa-Core 4G Câmera Tripla 25MP+5MP+8MP - Branco,
            <b class="text-orange">7 dias por semana</b> com o Aplicativo Carsystem pela <b class="text-orange">METADE do valor</b> de um seguro convencional?',
            'order' => 7,
            'url' => null,
            'begin_at' => '2020-03-11',
            'finish_at' => '2022-03-13'
        ]);

        $offerPayout = \App\Model\Offerwall\OfferPayout::firstOrCreate([
            'id_offer' => $offer->id,
            'payout_type' => 'CPA',
            'payout_in' => 2,
            'payout' => 7.8,
            'payout_user_crypto' => 500,
            'payout_ong_crypto' => 350,
        ]);

        $offerAsset = \App\Model\Offerwall\OfferAsset::firstOrCreate([
            'id_offer' => $offer->id,
            'name' => 'ec0fa23b.jpeg',
            'url' => 'https://images-americanas.b2w.io/produtos/01/00/images/134514/1/134514111_1SZ.jpg',
            'description' => 'Galaxy A50',
            'type' => \App\Mapper\OfferAsset::TYPE_IMG['id'],
            'storage' => \App\Mapper\OfferAsset::FS_AWS['id']
        ]);

        $texts = [
            'h' => 'Pega R$ 200 de Bônus no Galaxy A50!',
            'b' => 'O <b class="text-primary">Smartphone Samsung Galaxy A50</b> 128GB Dual Chip Android 9.0 Tela 6,4" Octa-Core 4G Câmera Tripla 25MP+5MP+8MP é um dos Melhores custo-benefício do mercado',
            'f' => null, // footer texts
            'btn' => ['accept' => 'quero A50 com 200 Reais de Bônus', 'decline' => 'já tenho celular' ], // button texts
        ];

        \App\Model\Offerwall\OfferCopy::firstOrCreate([
            'id_offer' => $offer->id,
            'text' => $texts
        ]);

        \App\Model\Offerwall\OfferDesign::firstOrCreate([
            'id_offer' => $offer->id
        ]);

        \App\Model\Offerwall\OfferAction::firstOrCreate([
            'id_offer' => $offer->id,
            'type' => 1,
            'conf' => [
                'url' => 'http://oferta.vc/v2/71c0736481'
            ]
        ]);

        \App\Model\Offerwall\OfferTarget::firstOrCreate([
            'id_offer' => $offer->id,
            'gender' => null,
            'age_min' => 18,
            'age_max' => 70,
            'economic' => [ 'a','b', 'c', 'd' ],
            'geo' => [ 'lat' => -23.5305978, 'lng' => -46.6590878, 'radius' => 20000 ],
            'mobile_code' => null,
            'running_period' => null
        ]);*/

        /*// FOURTH offer
        $offer = \App\Model\Offerwall\Offer::firstOrCreate([
            'id_publisher' => $lomadee->id,
            'id_offer_publisher' => 7,
            'name' => '40% para Elas do O Boticário',
            'slug' => \Illuminate\Support\Str::slug( '40% para Elas do O Boticário' ),
            'description' => 'Para Elas, O Boticário dá <b class="text-green-11">40% de Desconto</b>,
            <b class="text-orange">Para o resto de 2020 todos os dias</b>! Com o Aplicativo O Boticários, <b class="text-orange">Elas têm METADE do valor</b> de qualquer produto.',
            'order' => 4,
            'url' => null,
            'begin_at' => '2020-03-05',
            'finish_at' => '2020-03-25'
        ]);

        $offerPayout = \App\Model\Offerwall\OfferPayout::firstOrCreate([
            'id_offer' => $offer->id,
            'payout_type' => 'CPL',
            'payout_in' => 1,
            'payout' => 20.00,
            'payout_user_crypto' => 150,
            'payout_ong_crypto' => 100,
        ]);

        $offerAsset = \App\Model\Offerwall\OfferAsset::firstOrCreate([
            'id_offer' => $offer->id,
            'name' => 'ec0fa23b.jpeg',
            'url' => 'https://tudobem.s3.sa-east-1.amazonaws.com/img/product/ec0fa23b.jpeg',
            'description' => 'Seja uma Revendedora Boticário',
            'type' => \App\Mapper\OfferAsset::TYPE_IMG['id'],
            'storage' => \App\Mapper\OfferAsset::FS_AWS['id']
        ]);

        $texts = [
            'h' => '40% Para Elas do O Boticário',
            'b' => 'Inglês Profissional. Viaje com estilo para qualquer lugar e viva o seu melhor com as aulas de Inglês do App <b class="text-orange">Babbel</b>',
            'f' => 'Oferta válida somente para os primeiros <b>20 cadastrados</b>', // footer texts
            'btn' => ['accept' => 'quero 40% o Ano Todo', 'decline' => 'não gosto de Descontos' ], // button texts
        ];

        \App\Model\Offerwall\OfferCopy::firstOrCreate([
            'id_offer' => $offer->id,
            'text' => $texts
        ]);

        \App\Model\Offerwall\OfferDesign::firstOrCreate([
            'id_offer' => $offer->id
        ]);

        \App\Model\Offerwall\OfferAction::firstOrCreate([
            'id_offer' => $offer->id,
            'type' => 1,
            'conf' => [
                'url' => 'https://www.sejaumarevendedora.boticario.com.br/Conteudo/revendedor/cadastro/default.aspx'
            ]
        ]);

        \App\Model\Offerwall\OfferTarget::firstOrCreate([
            'id_offer' => $offer->id,
            'gender' => null,
            'age_min' => 25,
            'age_max' => 70,
            'economic' => ['a','b', 'c', 'd'],
            'geo' => null,
            'mobile_code' => [ 11, 21, 31, 41, 61, 51 ],
            'running_period' => [
                null, [ 10,11,13,14,15,16,17,18,19,20 ], [ 10,11,13,14,15,16,17,18,19,20 ], [ 9,10,11,13,14,15,16,17,18,19,20 ],
                [ 9,10,11,13,14,15,16,17,18,19,20 ], [ 9,10,11,13,14,15,16,17,18,19,20 ], [ 10,11,12,13,14 ]
            ]
        ]);*/
    }
}
