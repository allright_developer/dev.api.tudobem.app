<?php

use Illuminate\Database\Seeder;

class UserDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\User\Data::firstOrCreate([
            'id_user' => 1, 'data' => [ 'cep' => '04905-000', 'economic' => 'a' ]
        ]);
    }
}
