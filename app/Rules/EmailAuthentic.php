<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EmailAuthentic implements Rule
{


    /**
     * Determine if the validation rule passes. true is ok, false is fail
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $r = ( new \App\Service\TheChecker\EmailValidator)->validate( $value );
        if( $r['ok'] ){
            if( $r['score'] > 5 )
                $r = true;
            else
                $r = false;
        }
        else
            $r = false;

        return $r;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'inv';
    }
}
