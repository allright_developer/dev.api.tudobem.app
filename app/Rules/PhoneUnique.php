<?php
namespace App\Rules;

use App\Formatter\UserInput;
use Illuminate\Contracts\Validation\Rule;

class PhoneUnique implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = UserInput::phone( $value );
        $ddd = substr( $value,0, 2 );
        $phone = substr( $value,2, 9 );

        return \App\Model\User::where('ddd', $ddd)->where('phone', $phone )->first() === null ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'use';
    }
}
