<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdPublisher extends Model
{
    protected $table = 'ad_publisher';
    protected $guarded = [ 'id' ];
}
