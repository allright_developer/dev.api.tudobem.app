<?php
namespace App\Model;

use App\Model\Traits\QuestionDataHandler;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use QuestionDataHandler;

    protected $table = 'question';
    protected $guarded = ['id'];
    protected $casts = [ 'attributes' => 'json' ];

}
