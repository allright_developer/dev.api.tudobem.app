<?php
namespace App\Model\Offerwall;

use App\Model\Traits\DatetimeHandlable;
use App\Model\Traits\Offerwall\Offerable;
use App\Model\Traits\RuntimeDateRange;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Offer extends Model
{
    use Offerable, DatetimeHandlable, RuntimeDateRange;

    protected $guarded = ['id'];
    protected $table = 'offer';

    /**
     * @param array $data
     * @return array
     */
    public function prepareData( array $data ): array
    {
        $preparedData = null;
        $preparedData[ 'slug' ]      = Str::slug( $data[ 'name' ] );
        $preparedData[ 'begin_at' ]  = $this->setDbFormat( $data[ 'begin_at' ] );
        $preparedData[ 'finish_at' ] = $this->setDbFormat( $data[ 'finish_at' ] );

        if( isset( $data['status' ] ) )
            $preparedData[ 'status' ] = $data['status']['value'];

        if( is_array( $data['id_ad_publisher'] ) )
            $preparedData[ 'id_ad_publisher' ] = $data['id_ad_publisher'][ 'value' ];

        if( isset( $data['id_offer_publisher'] ) )
            $preparedData[ 'id_offer_publisher' ] = $data['id_offer_publisher'];

        $preparedData[ 'order' ] = $this->query()->where( 'status', 1 )->count()+1;
        $preparedData[ 'name' ]  = $data[ 'name' ];
        $preparedData[ 'slug' ]  = Str::slug( $data[ 'name' ] );
        $preparedData[ 'description' ] = $data[ 'description' ];

        $preparedData[ 'begin_at' ]  = $this->setDbFormat( $data[ 'begin_at' ] );
        $preparedData[ 'finish_at' ] = $this->setDbFormat( $data[ 'finish_at' ] );

        return $preparedData;
    }
}
