<?php

namespace App\Model\Offerwall;

use Illuminate\Database\Eloquent\Model;

class OfferTarget extends Model
{
    protected $guarded = ['id'];

    protected $table = 'offer_target';

    protected $casts = [
        'economic' => 'json',
        'geo' => 'json',
        'mobile_code' => 'json',
        'running_period' => 'json',
        'interests' => 'json',
        'marital_status' => 'json'
    ];

    public function prepareData( array $data, int $idOffer )
    {
        $preparedData = [];
        $preparedData[ 'id_offer' ] = $idOffer;
        $preparedData[ 'gender' ]      = $data['gender'];
        $preparedData[ 'children' ]    = $data['children'];
//        $preparedData[ 'mobile_code' ] = $data[ 'mobile_code' ]; // @todo implement it
        $preparedData[ 'age_min' ]     = $data[ 'age' ]['min'];
        $preparedData[ 'age_max' ]     = $data[ 'age' ]['max'];

        if( count( $data[ 'economic' ] ) > 0 )
            $preparedData[ 'economic' ] = $data[ 'economic' ];

        if( count( $data[ 'interests' ] ) > 0 )
            $preparedData[ 'interests' ] = $data[ 'interests' ];

        if( count( $data[ 'marital_status' ] ) > 0 )
            $preparedData[ 'marital_status' ] = $data[ 'marital_status' ];

        if( $data[ 'running_period' ]['all'] === true )
            $preparedData[ 'running_period' ] = null;
        else
            $preparedData[ 'running_period' ] = $data[ 'running_period' ][ 'periods' ];
        // @todo implement GEO
        if( isset( $data['geo'] ) )
            $preparedData['geo'] = $data['geo'];

        return $preparedData;
    }

    public static function getByIdOffer( $idOffer )
    {
        $target = self::query()->where( 'id_offer', $idOffer )->first()->toArray();

        if( $target ){

            $preparedTarget = [
                'age' => [
                    'min' => $target['age_min'],
                    'max' => $target['age_max']
                ],
                'children' => $target['children'],
                'economic' => $target['economic'] ?? [],
                'interests' => $target['interests'] ?? [],
                'marital_status' => $target['marital_status'] ?? [],
                'running_period' => [
                    'periods' => $target['running_period'] ?? [ [],[],[],[],[],[],[] ],
                    'all' => $target['running_period'] ? false : true
                ],
                'gender' => (string) $target['gender'],
            ];


        }

        return $preparedTarget;
    }
}
