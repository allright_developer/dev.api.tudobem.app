<?php

namespace App\Model\Offerwall;

use App\Model\Traits\Offerwall\Payoutable;
use Illuminate\Database\Eloquent\Model;

class OfferPayout extends Model
{
    use Payoutable;

    protected $guarded = [ 'id' ];
    protected $table = 'offer_payout';

    /**
     * @param array $data
     * @param int $idOffer
     * @return array
     */
    public function prepareData( array $data, int $idOffer ): array
    {
        $preparedData = [];

        /*if( isset( $data['status' ] ) )
            $preparedData[ 'status' ] = $data['status'];
        else
            $preparedData[ 'status' ] = 1;*/
        $preparedData[ 'id_offer' ] = $idOffer;
        $preparedData[ 'payout' ] = $data['payout'] ?? 0.25;
        $preparedData[ 'payout_in' ] = $data['payout_in'][ 'value' ] ?? 1;
        $preparedData[ 'payout_user_crypto' ] = $data['payout_user_crypto'] ?? 1;
        $preparedData[ 'payout_ong_crypto' ] = $data['payout_ong_crypto']  ?? 1;
        $preparedData[ 'payout_type' ] = $data['payout_type'] ?? 1;
        $preparedData[ 'status' ] = 1;

        return $preparedData;
    }

    /**
     * Persists payout properly and keeps only one active
     * @param array $data
     * @param int $idOffer
     * @return bool
     */
    public function persistPayout( array $data, int $idOffer ): bool
    {
        if( $this->thereIsAnEqualAndActivePayout( $data, $idOffer ) )
            return true;

        self::query()
            ->where('status', 1 )
            ->where('id_offer', $idOffer )
            ->update([ 'status' => 0 ] );

        return $this->fill( $this->prepareData( $data, $idOffer ) )->save();
    }

    public function thereIsAnEqualAndActivePayout( array $data, $idOffer )
    {
        return OfferPayout::query()
            ->where('id_offer', $idOffer )
            ->where('payout_type', $data['payout_type'] )
            ->where('payout_in', $data['payout_in'] )
            ->where('payout_user_crypto', $data['payout_user_crypto'] )
            ->where('payout_ong_crypto', $data['payout_ong_crypto'] )
            ->where('payout', $data['payout'] )
            ->where('status', 1 )
            ->first();

    }
}
