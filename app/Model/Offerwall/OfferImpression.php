<?php

namespace App\Model\Offerwall;

use Illuminate\Database\Eloquent\Model;

class OfferImpression extends Model
{
    protected $table = 'offer_impression';
    protected $primaryKey = 'hash';
    public $timestamps = false;

    protected $guarded = [ 'id' ];
}
