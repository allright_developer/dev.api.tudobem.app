<?php

namespace App\Model\Offerwall;

use Illuminate\Database\Eloquent\Model;

class OfferAction extends Model
{
    protected $guarded = [ 'id' ];
    protected $table = 'offer_action';

    protected $casts = [ 'conf' => 'json' ];

    public function prepareData( array $data, int $idOffer )
    {
        $preparedData = [ 'id_offer' => $idOffer, 'type' => $data[ 'type' ] ];
        if( $data['type'] === '1' ){
            $preparedData[ 'conf' ] = [ 'url' => $data[ 'conf' ][ 'url' ] ];
        }
        else{
            $preparedData[ 'conf' ] = [
                'text' => $data[ 'conf' ][ 'text' ],
                'fields' => $this->getTranslatedFields( $data['conf']['fields'] )
            ];
        }
        return $preparedData;
    }

    public function getTranslatedFields( array $fields )
    {
        $translatedFields = [];
        $subTypes = \App\Mapper\OfferAction::subTypes;

        foreach( $fields as $key => $field ){
            if( !isset( $field['name'] ) ){
                $field['name'] = \App\Mapper\OfferAction::formatName( $field[ 'type' ] );
            }

            $translatedFields[ $key ] = [
                'hint' => $field['hint'] ?? null,
                'label' => $field['label'] ?: 'Preencha este Campo',
                'name' => $field['name'],
                'required' => $field['required'] ?? false,
            ];

            if( $field['type'] === 'Custom' ){
                $translatedFields[ $key ][ 'type' ] = $subTypes[ $field[ 'subType' ] ];

                if( $field[ 'subType' ] !== 'text' && isset( $field['options'] ) ){
                    $translatedFields[ $key ][ 'options' ] = [];
                    foreach( $field[ 'options' ] as $option ){
                        $translatedFields[ $key ][ 'options' ][ $option['value'] ] = $option['label'];
                    }
                }
            }
            else{
                $translatedFields[ $key ][ 'type' ] = \App\Mapper\OfferAction::predefinedTypes[ $field[ 'name' ] ];
            }
        }
        return $translatedFields;
    }

    public static function getByIdOffer( $idOffer, $mapping )
    {
        $action = self::query()->select(['type', 'conf'])->where( 'id_offer', $idOffer )->first()->toArray();

        if( $action['type'] === 2 ){
            // converting fields to be edited
            $fields =  $action['conf']['fields'];
            $mapping = $mapping[ 'conf' ][ 'data' ][ 'mapping' ];
            foreach( $fields as $k => $field ){
                if( isset( $mapping[ $field['name'] ] ) ){
                    $fields[ $k ]['mapping'] = [ 'name' => $mapping[ $field['name'] ] ];
                }

                if( isset( $field['options'] ) ){
                    $options = [];
                    foreach( $field['options'] as $value => $label )
                        $options[] = [ 'label' => $label, 'value' => $value ];

                    $fields[ $k ]['options'] = $options;
                }
            }
            $action[ 'conf' ][ 'fields' ] = $fields;
        }

        return $action;
    }
}
