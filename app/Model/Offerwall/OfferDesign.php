<?php

namespace App\Model\Offerwall;

use Illuminate\Database\Eloquent\Model;

class OfferDesign extends Model
{
    protected $guarded = ['id'];
    protected $table = 'offer_design';
}
