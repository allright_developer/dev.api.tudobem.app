<?php

namespace App\Model\Offerwall;

use App\Security\Token;
use Illuminate\Database\Eloquent\Model;

class OfferwallImpression extends Model
{
    protected $table = 'offerwall_impression';
    public $timestamps = false;
    protected $primaryKey = 'hash';
    protected $guarded = ['id'];
    protected $casts = [
        'offers' => 'json'
    ];

    public static function add( array $data )
    {
        $h = null;
        if( count( $data[ 'offers' ] ) ){

            $h = self::getToken();
            $data[ 'impression_start_at' ] = time();
            $data[ 'hash' ] = $h;
            self::firstOrCreate( $data );
        }

        return $h;
    }

    public static function getToken()
    {
        return Token::generate( 8 );
    }
}
