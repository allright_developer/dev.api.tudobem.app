<?php

namespace App\Model\Offerwall;

use App\Model\Traits\Offerwall\Copyable;
use Illuminate\Database\Eloquent\Model;

class OfferCopy extends Model
{
    use Copyable;

    protected $guarded = ['id'];
    protected $table = 'offer_copy';

    protected $casts = [ 'text' => 'json' ];
}
