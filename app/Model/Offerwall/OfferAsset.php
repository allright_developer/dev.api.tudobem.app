<?php

namespace App\Model\Offerwall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OfferAsset extends Model
{
    protected $guarded = ['id'];

    protected $table = 'offer_asset';

    public static function getByOfferAndVersion( $idOffer, $version = 1 )
    {
        $result = DB::select(
            "
            SELECT oas.id_offer, oas.name, oas.description, oas.storage, oas.url, ad_version, oas.ad_version
            FROM offer_asset oas
            WHERE oas.id_offer = :id_offer AND oas.ad_version = :version AND oas.status = 1
            LIMIT 1
         ", [ 'id_offer' => $idOffer, 'version' => $version ] );

        if( count($result) )
            $result = $result[0];

        return $result;
    }
}
