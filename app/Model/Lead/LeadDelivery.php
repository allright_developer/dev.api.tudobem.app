<?php

namespace App\Model\Lead;

use App\Model\Traits\Lead\DeliverySetters;
use Illuminate\Database\Eloquent\Model;
use App\Mapper\OfferAction;

class LeadDelivery extends Model
{
    use DeliverySetters;

    protected $table = 'lead_delivery';

    protected $fillable = [ 'entity', 'id_entity', 'periods', 'transport', 'frequency', 'total_sent', 'how_many', 'conf', 'status' ];

    protected $casts = [
        'periods'   => 'json',
        'conf'      => 'json',
        'frequency' => 'json'
    ];

    /**
     * Get delivery settings to persist it
     * @param array $data
     * @return array
     */
    public function getDeliverySettings( array $data ): array
    {
        $settings = [];

        if( $data['allPeriods'] === true )
            $settings[ 'periods' ] = null;
        else
            $settings[ 'periods' ] = $data[ 'periods' ];

        $settings[ 'transport' ] = $data[ 'transport' ][ 'value' ];
        $settings[ 'frequency' ] = [
            'by' => $data['frequency']['by']['value'],
            'limit' => $data[ 'frequency' ][ 'limit' ],
            'how_many' => $data[ 'frequency' ][ 'how_many' ]
        ];

        $settings['how_many'] = $data[ 'how_many' ];
        $settings['conf'][ 'data' ] = [
            'transport_settings' => $data[ 'transport_settings' ]
        ];

        return $settings;
    }

    public function getFieldMapping( array $fields ): array
    {
        $mapping = [];
        foreach( $fields as $field ){
            if( isset( $field[ 'mapping' ] ) ){
                $name = OfferAction::formatName( $field[ 'type' ] );
                $mapping[ $name ] = $field[ 'mapping' ][ 'name' ];
            }
            elseif( isset( $field['subType'] ) ){
                $mapping[ $field[ 'name' ] ] = $field['name'];
            }
            else{
                $name = trim( strtolower( $field[ 'type' ] ) );
                $mapping[ $name ] = $name;
            }
        }

        return $mapping;
    }

    /**
     * @param array $data
     * @param int $idOffer
     * @param string $entity
     * @return array
     */
    public function prepareData( array $data, int $idOffer, string $entity = 'offer' ): array
    {
        $preparedData = $this->getDeliverySettings( $data[ 'delivery' ] );
        $preparedData['conf']['data']['mapping'] = $this->getFieldMapping( $data['fields'] );
        $preparedData['id_entity'] = $idOffer;
        $preparedData['entity'] = $entity;

        return $preparedData;
    }

    public static function getByIdOffer( $idOffer, $entity = 'offer' )
    {
        $leadDelivery = self::query()
            ->select('periods', 'transport', 'frequency', 'how_many', 'conf', 'status' )
            ->where( 'id_entity', $idOffer )->where('entity', $entity )->first();

        if( $leadDelivery ){

            $leadDelivery = $leadDelivery->toArray();
            if( $leadDelivery['periods' ] === null ){
                $leadDelivery[ 'allPeriods' ] = true;
            }
            else
                $leadDelivery[ 'allPeriods' ] = false;

            if( isset( $leadDelivery['conf']['data']['transport_settings'] ) )
                $leadDelivery[ 'transport_settings' ] = $leadDelivery['conf'][ 'data' ]['transport_settings'];

            $leadDelivery[ 'transport' ] = [ 'value' => $leadDelivery['transport' ] ];
        }

        return $leadDelivery;
    }
}
