<?php
namespace App\Model\Lead;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lead extends Model
{
    protected $table = 'lead';

    protected $primaryKey = 'token';

    protected $casts = [
        'data' => 'json', 'response' => 'json'
    ];

    public $timestamps = false;
    protected $guarded = [ 'id' ];

    /**
     * Gets the leads to send
     * @param array $data
     * @return array Array of Objects with lead data
     */
    public static function getToSend( array $data ): array
    {
        $resultData = [];
        $sql = "SELECT
                    l.token, l.data
                FROM `lead` l
                WHERE
                    l.generated_by = :entity AND l.id_entity = :id_entity AND l.delivered_at IS NULL AND l.status = 1
                ORDER BY l.created_at ASC
                LIMIT :limit";

        $result = DB::select( $sql, $data );

        if( $result )
            $resultData = collect( $result )->map( fn( $item, $key ) => array_merge( json_decode( $item->data, true ), [ 'token' => $item->token ] ) )->all();

        return $resultData;
    }
}
