<?php
namespace App\Model;

use App\Events\UserCreated;
use App\Events\UserEmailConfirmed;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','ddd', 'phone', 'gender', 'token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    protected $dispatchesEvents = [
        'created' => UserCreated::class
    ];

    /**
     * Confirm the user account and email
     * @param string $token
     * @throws \Exception
     * @return Model|null
     */
    public function confirmEmail( string $token ): Model
    {
        if( !$token )
            throw new \Exception( 'User Token not found', 422 );

        $user = User::query()
            ->where('token', $token )
            ->first( [ 'id', 'token', 'name', 'status', 'email_verified_at' ] );
        if( $user ){
            if( $user->email_verified_at === null ){ // if the user email is not confirmed, persists user confirmation data
                $user->email_verified_at = \date( 'Y-m-d H:i:s' );
                $user->status = 1;
                $user->save();
                event( new UserEmailConfirmed( $token ) );
            }
            else
                throw new \Exception( 'Account already confirmed', 421 );
        }
        else
            throw new \Exception( 'user not found', 422 );

        return $user;
    }
}
