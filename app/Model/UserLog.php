<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $table = 'user_log';
    protected $guarded = [ 'created_at' ];

    protected $casts = [ 'data' => 'json' ];

    public $timestamps = false;
}
