<?php
namespace App\Model\Promotion;

use App\Security\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $table = 'promotion_image';
    protected $guarded = [ 'id' ];

    public function addImage( array $data, $idPromotion )
    {
        $newImageEntity = null;
        $imgFullPath = public_path( \App\Mapper\PromotionImage::FS_AWS[ 'path' ] );
        $toFile = $imgFullPath;
        $cropData = $data['canvas'];

        $tmpImage = \Intervention\Image\ImageManagerStatic::make( $data['data'] );
        $name = Token::generate(9).'.'.explode('/', $tmpImage->mime() )[1];
        $fullName = \App\Mapper\PromotionImage::FS_AWS[ 'path' ].$name;
        $toFile .= $name;

        $resultImg = $tmpImage->crop( $cropData['width'], $cropData['height'], $cropData['left'] < 0 ? 0 : $cropData['left'], $cropData['top'] < 0 ? 0 : $cropData['top'])
            ->resize( 450, null, function( $constraint ){
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save( $toFile );

        $s3 = Storage::disk( 's3' );
        $fileResult = $s3->put( $fullName, $resultImg->getEncoded(), [ 'ACL' => 'public-read'] );

        if( $resultImg && $fileResult ){
//            $resultImg->destroy();
            unlink( $toFile );

            $imageData = [
                'id_promotion' => $idPromotion,
                'name' => $fullName,
                'description' => $data['description'],
                'storage' => \App\Mapper\PromotionImage::FS_AWS[ 'id' ],
                'status' => 1
            ];

            $newImageEntity = Image::create( $imageData );
        }

        return $newImageEntity;
    }

    public function addImages( array $images, $idPromotion )
    {
        foreach( $images as $image ){
            $this->addImage( $image, $idPromotion );
        }
    }

    public function removeImage( array $data )
    {
        $image = Image::where('name', $data['name'] )->first();
        if( $image ){
            Image::where( 'name', $data['name'] )->where( 'id_promotion', $data['id_promotion' ] )
                ->update( ['status' => 0] );
            Storage::disk( 's3' )->delete( $data[ 'name' ] );
        }
    }

    public static function getImagesByPromotionId( $idPromotion )
    {
        $images = Image::where( 'id_promotion', $idPromotion )->where('status', 1)
            ->get()->all();

        $data = [];
        if( $images ){
            $storage = Storage::disk( 's3' );
            foreach ( $images as $image ){
                $data[] = [
                    'canvas' => [],
                    'id_promotion' => $image->id_promotion,
                    'name' => $image->name,
//                    'data' => '/statics/icons/icon-256x256.png',
                    'data' => $storage->url( $image->name ) , // original
                    'description' => $image->description,
                    'storage' => $image->storage,
                    'status' => $image->status,
                ];
            }
        }
        return $data;
    }
}
