<?php

namespace App\Model\Promotion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class User extends Model
{
    protected $table = 'promotion_user';
    protected $fillable = [ 'id_promotion', 'id_user', 'favorited' ];

    protected function setKeysForSaveQuery( Builder $query )
    {
        $query->where( 'id_promotion', '=', $this->id_promotion );
        $query->where( 'id_user', '=', $this->id_user );

        return $query;
    }
    public function prepareData( array $data )
    {
        $preparedData = [
            'id_promotion' => $data[ 'promotion' ],
            'id_user' => $data[ 'user' ],
            'favorited' => $data[ 'favorite' ],
        ];

        return $preparedData;
    }
}
