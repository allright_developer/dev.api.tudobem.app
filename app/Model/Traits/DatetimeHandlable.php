<?php
namespace App\Model\Traits;

/**
 * Class DatetimeHandlable has methods to translate dates in both ways
 * @package App\Model\Traits
 */
trait DatetimeHandlable
{
    public function setBeginAt( $v )
    {
        $this->attributes['begin_at'] = $this->setDbFormat( $v );
    }

    public function setFinishAt( $v )
    {
        $this->attributes['finish_at'] = $this->setDbFormat( $v );
    }

    public function getBeginAtAttribute()
    {
        return $this->getViewFormat( $this->attributes['begin_at'] );
    }

    public function getFinishAtAttribute()
    {
        return $this->getViewFormat( $this->attributes['finish_at'] );
    }

    public function setDbFormat( $v )
    {
        $date = \DateTime::createFromFormat('d/m/Y H:i', $v );
        return $date->format( 'Y-m-d H:i:s' );
    }

    public function getViewFormat( $v )
    {
        $date = new \DateTime( $v );
        return $date->format( 'd/m/Y H:i' );
    }
}
