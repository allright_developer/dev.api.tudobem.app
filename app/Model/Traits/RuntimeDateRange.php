<?php
namespace App\Model\Traits;

use App\Model\Product\Offer;

trait RuntimeDateRange
{
    /**
     * It verifies if both finish at or begin at dates collides with other valid offer date vigency
     * Used only by Product Offer entity
     * @return bool
     */
    public function collidesWithOtherOffer( array $data )
    {
        // the new offer should begins after the vigency of all other product offers

        $beginAtCollidesWith  = $this
            ->where( 'id_product', '=', $data[ 'id_product' ] )
            ->where( 'status', '>', 0 )
            ->where( 'finish_at', '>=', $data['begin_at'] );

        if( isset( $data['id'] ) )
            $beginAtCollidesWith->where( 'id', '<>', $data[ 'id' ] );

        return $beginAtCollidesWith
            ->select( [ 'id', 'begin_at', 'finish_at' ] )
            ->first() ;
    }

    public function isFinishLessThanBegin( array $data )
    {
        $dateBegin  = new \DateTime( $data[ 'begin_at' ] );
        $dateFinish = new \DateTime( $data[ 'finish_at' ] );

        return ($dateFinish <= $dateBegin);
    }
}
