<?php

namespace App\Model\Traits\Gridable;

trait GridArticle
{
    use Gridable;

    /**
     * @var array columns config params
     */
    public $columns =
    [
        [ 'field' => 'id', 'name' => 'id','label' => 'ID','searchable' => false, 'searchType' => 'text','visible' => false, 'sortable' => true ],
        [
            'field' => 'id_category', 'name' => 'id_category','title' => 'Categoria', 'searchable' => true, 'searchType' => 'select','visible' => true, 'sortable' => true
        ],
        [
            'data' => 'id_author', 'name' => 'id_author','title' => 'Autor', 'searchable' => true, 'searchType' => 'select','visible' => true, 'sortable' => true
            ,'dataMap' => '%get_categories%'
        ],

        [ 'data' => 'title', 'name' => 'title','title' => 'Título', 'searchable' => true, 'searchType' => 'text','visible' => true, 'sortable' => true ],
        [ 'data' => 'published_at', 'name' => 'published_at','title' => 'Publicado em', 'searchable' => true, 'searchType' => 'datetime','visible' => true, 'sortable' => true ],
        [ 'data' => 'created_at', 'name' => 'created_at','title' => 'Criado em', 'searchable' => true, 'searchType' => 'datetime','visible' => true, 'sortable' => true ],
        [ 'data' => 'updated_at', 'name' => 'updated_at','title' => 'Atualizado em', 'searchable' => true, 'searchType' => 'datetime','visible' => true, 'sortable' => true ],
        [ 'data' => 'status', 'name' => 'status','title' => 'Status', 'searchable' => true, 'searchType' => 'select', 'visible' => true, 'sortable' => true ]

    ];

    public $functions = [];

}
