<?php
namespace App\Model\Traits\Gridable;

trait Gridable
{
    /**
     * Get an array of searchable columns prefixed with the table name
     * @return array|string
     */
    public function getSearchableColumns( $toSQL = false )
    {
        $collection = collect( $this->columns );
        $cols = $collection->where('data', true )->map(function( $item, $index ) {
            return $this->table.'.'.$item['data'];
        })->toArray();

        if( $toSQL )
            $cols = implode(',', $cols );

        return $cols;
    }

    public function getColumnsMetaData( $json = true )
    {
        $cols = $this->columns;
        if( $json )
            $cols = \json_encode( $cols );

        if( $this->functions ){
            foreach ( $this->functions as $key => $function ){
                $cols = str_replace('"%'.$key.'%"', $function, $cols );
            }
        }

        return $cols;
    }
}
