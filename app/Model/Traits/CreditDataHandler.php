<?php
namespace App\Model\Traits;

trait CreditDataHandler
{
    public static function getUserCredits( $idUser )
    {
        return self::where( 'id_user', $idUser )->first();
    }
}
