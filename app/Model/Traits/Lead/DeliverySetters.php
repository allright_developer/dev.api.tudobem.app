<?php
namespace App\Model\Traits\Lead;

use Illuminate\Support\Facades\DB;

trait DeliverySetters
{
    /**
     * Get all valid and active Delivery settings to process the lead Queue
     * @param int $status
     * @return array
     */
    public static function getAll( int $status = 1 ): array
    {
        $dayOfWeek = \date( 'w' );
        $currentHour = (int) \date( 'H'); // 24h format

        $deliverySetters = DB::select(
            "
            SELECT
                entity, id_entity, transport, frequency, how_many, total_sent, conf
            FROM lead_delivery
            WHERE
                status = :status AND how_many > total_sent AND
                ( periods IS NULL OR JSON_CONTAINS( periods->\"$[$dayOfWeek]\",'$currentHour' ) )
            ",
            [ 'status' => $status ]
        );

        return $deliverySetters;
    }

    /**
     * It calculates how many leads to send and if the Leqd queue should be stopped
     * @param \stdClass $data
     * @return int the number of leads to send
     * @throws \Exception
     */
    public static function howManyToSend( \stdClass $data ): int
    {
        $howManyToSend = 0;
        $remaining = $data->how_many - $data->total_sent; // how many leads left to send

        $data->frequency = json_decode( $data->frequency );

        if( $remaining > 0 && $data->frequency ){
            $expr = $data->frequency->how_many.$data->frequency->by;
            $presentTs = new \DateTime();
            $pastTs = (new \DateTime())->sub( new \DateInterval("PT$expr") );

            $sql = "SELECT count(token) total FROM `lead` WHERE id_entity = :id_entity AND status = 1 AND delivered_at between :begin AND :finish";

            $totalSentInPeriod = DB::select( $sql, [ 'begin' => $pastTs, 'finish' => $presentTs, 'id_entity' => $data->id_entity ] );
            $totalSentInPeriod = $totalSentInPeriod[0]->total;

            $howManyToSend = $data->frequency->limit - $totalSentInPeriod;
            if( $howManyToSend > $remaining ){
                $howManyToSend = $remaining;
                self::turnOffDelivery( [ 'entity' => $data->entity, 'id_entity' => $data->id_entity ] );
            }
        }
        elseif( $remaining <= 0 ){
            self::turnOffDelivery( [ 'entity' => $data->entity, 'id_entity' => $data->id_entity ] );
        }
        return $howManyToSend;
    }

    /**
     * Deactivates the Lead Delivery due to reaching the lead limit to send
     * @param array $data [ entity: entity name, id_entity: entity id ]
     * @return int
     * @todo create a notification that the Lead Queue has finished
     */
    public static function turnOffDelivery( array $data ): int
    {
        return self::where(
            [ [ 'entity' => $data['entity'] ], [ 'id_entity' => $data[ 'id_entity' ] ] ]
        )
        ->update( ['status' => 0 ] );
    }
}
