<?php
namespace App\Model\Traits\Conventionable;


trait Field
{
    /**
     * Discover if a field is a foreign key and return meta data about the field
     * @param string $fieldName
     * @return array Array containing key and table keys or null
     */
    public static function resolveForeignKey( $fieldName ): array
    {
        $splited = explode('_', $fieldName );
        $resolved = null;
        if( $splited[0] === 'id' && count( $splited ) > 1 ){
            $resolved = [
                'key' => $splited[0],
                'table' => $splited[1]
            ];
        }

        return $resolved;
    }
}
