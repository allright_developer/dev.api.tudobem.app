<?php
namespace App\Model\Traits;

use App\Model\Product\Image;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Mixed_;

trait ProductDataHandler
{
    public function buildJson()
    {
        $images = (new Image())->getImagesByProductId( $this->id ) ;
        $json = [
            'id' => $this->id,
            'id_category' => $this->id_category,
            'id_company' => $this->id_company,
            'label' => $this->label,
            'description' => $this->description,
            'crypto' => $this->crypto,
            'crypto_cashback_user' => $this->crypto_cashback_user,
            'crypto_cashback_org' => $this->crypto_cashback_org,
            'crypto_actions' => $this->crypto_actions,
            'fiat' => $this->fiat,
            'status' => $this->status,
            'images' => $images
        ];

        return $json;
    }


    public function getCreatedAtAttribute()
    {
        return $this->getViewFormat( $this->attributes['created_at'] );
    }

    public function setDbFormat( $v )
    {
        $date = \DateTime::createFromFormat('d/m/Y H:i', $v );
        return $date->format( 'Y-m-d H:i:s' );
    }

    public function getViewFormat( $v )
    {
        $date = new \DateTime( $v );
        return $date->format( 'd/m/Y H:i' );
    }

    public static function get( array $filters )
    {
        $limit = $filters[ 'limit' ] ?? 20;
        $stmt = self::defineFilters( $filters );
        $additionalSql = self::getAdditionalJoins( $filters );
        $sql = "
            SELECT
                p.id, p.order, p.label, p.slug, p.description, p.crypto, p.crypto_cashback_user,
                p.crypto_cashback_org, p.cta, p.status p_status,
                po.label offer_label, po.description offer_desc, po.crypto offer_crypto,
                pi.name image_name, pi.description image_desc,
                c.label category_label, c.slug category_slug
                {$additionalSql['col']}
            FROM product p
            INNER JOIN product_image pi ON pi.id_product = p.id AND pi.status = 1
            LEFT JOIN  product_offer po ON po.id_product = p.id AND po.status = 1
            LEFT JOIN  category c ON c.id = p.id_category
            {$additionalSql['sql']}
            WHERE p.status = 1 $stmt
            ORDER BY p.order ASC, p.id DESC
            LIMIT :limit
        ";

        return DB::select( $sql, [ 'limit' => $limit ] );
    }

    public static function defineFilters( array $filters ): string
    {
        $stmt = isset( $filters[ 'id_category' ] ) ? " AND p.id_category = {$filters['id_category']}" : '';
        $stmt .= isset( $filters[ 'slug' ] ) ? " AND p.slug = '{$filters['slug']}'" : '';

        return $stmt;
    }

    public static function getAdditionalJoins( array $stmt ): array
    {
        $sql = [ 'sql' => '', 'col' => ''];
        if( isset($stmt['id_user']) ){
            $sql['sql'] = "LEFT JOIN product_favorited pf ON pf.id_product = p.id AND pf.id_user = {$stmt['id_user']}";
            $sql['col'] = ',pf.favorited';
        }

        return $sql;
    }

    /**
     * Get a single Product with Offer by id
     * @param int|null $idProduct product id
     * @return array|null
     */
    public static function getWithOffer( int $idProduct = null, int $status = 1 )
    {
        $res = null;
        if( $idProduct ){
            $sql = "SELECT
                    p.id, p.crypto, po.crypto offer_crypto
                FROM product p
                LEFT JOIN
                    product_offer po ON po.id_product = p.id AND po.status = 1
                WHERE
                    p.id = :id_product AND p.status = :status";

            $res = DB::select( $sql, [ 'id_product' => $idProduct, 'status' => $status ] );
            if( $res )
                $res = $res[0];
        }
        return $res;
    }
}
