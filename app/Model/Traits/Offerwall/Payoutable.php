<?php

namespace App\Model\Traits\Offerwall;
use Illuminate\Support\Facades\DB;

trait Payoutable
{
    public static function getByOffer( $idOffer )
    {
        $result = DB::select(
    "
            SELECT
                op.id, op.id_offer, op.payout_type type, op.payout_in,
                op.payout_user_crypto user, op.payout_ong_crypto ong
            FROM offer_payout op
            WHERE op.id_offer = :id_offer AND op.status = 1
            LIMIT 1
         ", [ 'id_offer' => $idOffer ] );

        if( count($result) )
            $result = $result[0];

        return $result;
    }
}
