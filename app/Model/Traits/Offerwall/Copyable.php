<?php

namespace App\Model\Traits\Offerwall;
use Illuminate\Support\Facades\DB;

trait Copyable
{
    public static function getByOfferAndVersion( $idOffer, $version = 1 )
    {
        $result = DB::select(
    "
            SELECT oc.id, oc.text, oc.ad_version
            FROM offer_copy oc
            WHERE oc.id_offer = :id_offer AND oc.ad_version = :version AND oc.status = 1
            LIMIT 1
         ", [ 'id_offer' => $idOffer, 'version' => $version ] );

        if( count($result) )
            $result = $result[0];

        return $result;
    }
}
