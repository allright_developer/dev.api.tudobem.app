<?php
namespace App\Model\Traits\Offerwall;

use App\Model\User;
use App\Service\Google\GeoCoder\GeoCoder;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Integer;

trait Offerable
{
    public static $userData = null;

    /**
     * Not so simple offer retrieving method
     * @param null $user
     * @param int $count
     * @param null $params
     * @return array Array of qualified offers to show to the user
     */
    public static function simpleGet( $user = null, $params = null ): array
    {
        $notViewingTwice = '';
        $targetSQL = self::getRunningPeriods();
        $params = self::resolveParams( $params );

        if( $user ){
            $targetSQL .= self::getTargetQueryByUser( $user );
//            $notViewingTwice = self::notViewingTwice( $user->id );
        }
        $now = date('Y-m-d H:i:s' );
        $sql = "SELECT
                    o.id, o.id_ad_publisher, o.id_offer_publisher, o.order, o.name, o.slug, o.description, o.begin_at, o.finish_at, o.status,
                    oa.type action_type, oa.conf,
                    od.type design_type, od.design,
                    oc.text, oc.ad_version, oc.id id_copy,
                    op.payout_ong_crypto payout_ong, op.payout_user_crypto payout_user,
                    oi.description asset_desc, oi.url asset_url
                    -- oi.id_offer, oi.geo, oi.economic, oi.gender, oi.mobile_code, oi.gender,
                FROM
                    offer o
                LEFT JOIN offer_action oa ON oa.id_offer = o.id
                LEFT JOIN offer_design od ON od.id_offer = o.id AND od.status = 1
                LEFT JOIN offer_copy oc ON oc.id_offer = o.id AND oc.ad_version = {$params['ad_version']} AND oc.status = 1
                LEFT JOIN offer_payout op ON op.id_offer = o.id AND op.status = 1
                LEFT JOIN offer_asset oi ON oi.id_offer = o.id AND oi.ad_version = {$params['ad_version']} AND oi.status = 1
                $targetSQL
                WHERE
                    o.status = 1 AND o.finish_at > '$now' AND o.begin_at < '$now' $notViewingTwice {$params['slug']} {$params['id']}
                ORDER BY o.`order` ASC, o.id desc
                LIMIT :count";

        $offers = DB::select( $sql, [ 'count' => $params['limit'] ] );

        return $offers;
    }

    public static function getTargetQueryByUser ( User $user ) : string
    {
        $sql = [];
        $userData = User\Data::getByIdUser( $user->id );
        if( $user->ddd ){
            $sql[] = '( ot.mobile_code IS NULL OR JSON_CONTAINS( ot.mobile_code, \''.$user->ddd.'\') )';
        }
//
//        if( $user->gender ){
//            $sql[] = '( ot.gender IS NULL OR ot.gender = \'"'.$user->gender.'"\' )';
//        }

        if( $userData ){
            $userData = json_decode( $userData->data );
            self::$userData = $userData;
            if( isset( $userData->geo ) )
                $sql[] = '(ot.geo IS NULL OR JSON_CONTAINS_PATH( ot.geo, \'all\', \'$.lat\', \'$.lon\', \'$.radius\') )';

            if( isset( $userData->economic ) )
                $sql[] = '( ot.economic IS NULL OR JSON_CONTAINS( ot.economic, \'"'.$userData->economic.'"\') )';

            if( isset( $userData->dob ) ){ // day of birth
                $age = ( \DateTime::createFromFormat('d/m/Y', $userData->dob ) )->diff( new \DateTime )->y;
                $sql[] = '( ot.age_min IS NULL OR ot.age_min <= '.$age.' ) AND ( ot.age_max IS NULL OR ot.age_max >= '.$age.' )';
            }
        }

        if( $sql )
            $sql = ' AND '.implode(" AND ", $sql );
        else
            $sql = '';

        return $sql;
    }

    /**
     * @return string the SQL to constrain an Offer by days and hours
     */
    public static function getRunningPeriods() : string
    {
        $dayOfWeek = \date( 'w' );
        $currentHour = (int) \date( 'H'); // 24h format
        $currentHour = $currentHour === 23 ? 0 : $currentHour + 1;
        $sql = "RIGHT JOIN offer_target ot ON ot.id_offer = o.id AND ot.status = 1 AND
                ( ot.running_period IS NULL OR JSON_CONTAINS( ot.running_period->\"$[$dayOfWeek]\",'$currentHour' ) )";

        return $sql;
    }

    public static function validateGeo( array $coords ): bool
    {
        $distanceInKM = ( new GeoCoder )->getDistance( $coords[ 'origin_lat' ], $coords[ 'origin_lon' ], $coords[ 'destination_lat' ], $coords[ 'destination_lon' ] );
        return $distanceInKM <= $coords['radius' ];
    }

    /**
     * It avoids to show a same offer twice to a same user.
     * @param int $idUser
     * @return string SQL string to be added to the main query
     */
    public static function notViewingTwice( int $idUser ): string
    {
        $sql = "AND o.id NOT IN ( select id_offer from offer_impression om where om.id_offer = o.id AND om.id_user = $idUser AND om.impression_end_at IS NOT NULL )";
        return $sql;
    }

    /**
     * just a helper method to resolve some params
     * @param $params
     * @return array Array of resolved params
     */
    public static function resolveParams( $params ): array
    {
        if( !$params )
            $params = [];

        if( !isset( $params[ 'ad_version' ] ) )
            $params[ 'ad_version' ] = 0;

        if( isset( $params['slug'] ) ){
            $params['slug'] = preg_replace('/[\$\*\%\(\s]+/','', $params[ 'slug' ]);
            $params['slug'] = " AND o.slug = '{$params['slug']}'";
        }
        else
            $params['slug'] = '';

        if( isset( $params['id'] ) && is_numeric( $params['id'] ) ){
//            $params['id'] = preg_replace('/\D/','', $params[ 'id' ]);
            $params['id'] = " AND o.id = '{$params['id']}'";
        }
        else
            $params['id'] = '';

        if( !isset( $params['limit'] ) )
            $params['limit'] = 8;

        return $params;
    }
}
