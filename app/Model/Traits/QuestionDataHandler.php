<?php
namespace App\Model\Traits;

use Illuminate\Support\Str;

trait QuestionDataHandler
{
    public function generateUniqueVal( $salt = '' )
    {
        $val = strtolower( substr( Str::uuid(),0,4 ) );
        if( $salt )
            $val .= '_'.$salt;

        return $val;
    }

    public static function prepareData( array $data, $idSurvey, $order )
    {
        $questionData = [
            'id_survey'  => $idSurvey,
            'order'      => $order,
            'type'       => $data[ 'type' ],
            'title'      => $data[ 'title' ],
            'status'      => $data[ 'status' ] === 2 ? 1 : $data['status']
        ];

        if( $data['bonify'] ){
            $questionData['credits'] = $data['credits']['user'] < 0 ? 0 : $data['credits']['user'];
            $questionData['credits_beneficent'] = $data['credits']['org'] < 0 ? 0 : $data['credits']['org'];
        }
        else{
            $questionData['credits'] = 0;
            $questionData['credits_beneficent'] = 0;
        }

        $questionData['attributes']['required'] = $data['required'];
        if( isset( $data[ 'attr' ]) && $data['type'] === 'text' )
            $questionData['attributes'] = array_merge( $questionData['attributes'], $data[ 'attr' ] );

        return $questionData;
    }
}
