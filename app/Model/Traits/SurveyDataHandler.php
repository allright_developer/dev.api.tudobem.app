<?php
namespace App\Model\Traits;

use App\Model\Option;
use App\Model\Question;

trait SurveyDataHandler
{
    public function buildJson()
    {
        $img = json_encode([ 'data' => null, 'canvas' => [] ], \JSON_FORCE_OBJECT );
        $json = [
            'id' => $this->id,
            'id_category' => $this->id_category,
            'id_company' => $this->id_company,
            'id_product' => $this->id_product,
            'name' => $this->name, 'credits' => $this->credits, 'description' => $this->description,
            'questions' => [], 'finish_at' => $this->finish_at, 'begin_at' => $this->begin_at,
            'image' => $this->image === null ? $img : $this->image,
            'bonify' => $this->credits ? true : false
        ];
        $questions = Question::where( 'id_survey', $this->id )->where('status', 1)
                        ->orderBy('order')
                        ->get()->all();
        foreach ( $questions as $question ){
            $questionData = [
                'id' => $question->id,
                'val' => $question->type === 'checkbox' ? [] : '',
                'title' => $question->title,
                'type' => $question->type,
                'status' => $question->status,
                'credits' => $question->credits ? [ 'user' => $question->credits, 'org' => $question->credits_beneficent ] : null,
                'bonify' => $question->credits ? true : false
            ];

            $attr = $question->getAttribute( 'attributes' );

            $questionData[ 'required' ] = $attr[ 'required' ];

            if( $question->type === 'text' ){
                $questionData ['attr' ] = [
                    'behavior' => $attr['behavior'], 'name' => 'q'.$question->id,
                    'maxlength' => $attr['maxlength']
                ];

                if( $attr['behavior'] === 'date' ){
                    $questionData['attr'][ 'mask' ] = '##/##/####';
                    $questionData['attr'][ 'placeholder' ] = 'Formato: DD/MM/AAAA';
                }

                if( $attr['behavior'] === 'datetime' ){
                    $questionData['attr'][ 'mask' ] = '##/##/####';
                    $questionData['attr'][ 'placeholder' ] = 'Formato: DD/MM/AAAA HH:MM';
                }

                if( $attr['behavior'] === 'number' )
                    $questionData[ 'type' ] = 'number';
            }

            if( in_array( $question->type, [ 'radio', 'checkbox' ] ) ){
                $options = Option::where( 'id_question', $question->id )->orderBy('order' )->get()->all();
                $questionData['answers'] = [];
                foreach( $options as $option ){
                    $questionData[ 'answers' ][] = [
                        'val' => $option->id, 'label' => $option->label, 'status' => $option->status
                    ];
                }
            }

            $json['questions'][] = $questionData;
        }

        return $json;
    }
}
