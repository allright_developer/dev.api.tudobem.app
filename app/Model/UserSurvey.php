<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserSurvey extends Model
{
    protected $table = 'user_survey';
    protected $fillable = ['id_user', 'id_survey'];

    public $timestamps = false;
}
