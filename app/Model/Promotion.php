<?php
namespace App\Model;

use App\Model\Traits\DatetimeHandlable;
use App\Model\Traits\RuntimeDateRange;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Promotion extends Model
{
    use DatetimeHandlable, RuntimeDateRange;

    protected $table = 'promotion';
    protected $guarded = [ 'id' ];

    public function prepareData( array $data )
    {
        $preparedData = [];
        $preparedData[ 'name' ] = $data[ 'name' ];
        $preparedData[ 'slug' ] = Str::slug( $data[ 'name' ] );
        $preparedData[ 'label' ] = $data[ 'label' ];
        $preparedData[ 'description' ] = $data[ 'description' ];
        $preparedData[ 'rules' ] = $data[ 'rules' ];
        $preparedData[ 'url' ] = $data[ 'url' ];
        $preparedData[ 'begin_at' ] = $this->setDbFormat( $data[ 'begin_at' ] );
        $preparedData[ 'finish_at' ] = $this->setDbFormat( $data[ 'finish_at' ] );
        $preparedData[ 'status' ] = $data['status']['value'];

        if( is_array( $data['id_category'] ) )
            $preparedData['id_category' ] = $data['id_category']['value'];

        if( is_array( $data['id_company'] ) )
            $preparedData['id_company' ] = $data['id_company']['value'];

        if( is_array( $data['id_product'] ) )
            $preparedData['id_product' ] = $data['id_product']['value'];

        return $preparedData;
    }

    public function getBySlug( $slug = null )
    {
        $result = null;
        $promotions = [];
        $t      = $this->table;
        $imgTbl = 'promotion_image';
        $user   = Auth::user();
        $columns = [
            $t.'.id', $t.'.slug', $t.'.description', $t.'.label', $t.'.name', $t.'.url', $t.'.rules',
            $t.'.finish_at', $t.'.begin_at', $t.'.status'
        ];

        $query = $this->newQuery()
            ->where( $t.'.status', 1 );

        if( $user ){
            $columns[] = 'promotion_user.favorited';
            $query->leftJoin('promotion_user', function( $join ) use( $t, $user ) {
                $join->on( 'promotion_user.id_promotion', '=', $t.'.id' )->where( 'promotion_user.id_user', '=', $user->id );
            });
        }

        if( $slug )
            $result = $query->where( $t.'.slug', $slug );

        $result = $query
            ->select( $columns )
            ->limit(20 )
            ->orderBy('id', 'desc' )
            ->get()->all();

        foreach( $result as $promotion ){
            $images = \App\Model\Promotion\Image::query()
                ->where('id_promotion', $promotion->id )
                ->where('status', 1 )
                ->select(['name', 'description'] )->get();
            $fullPathImages = $images->map( function( $image, $key ){
                return [ 'name' => $image->name, 'description' => $image->description, 'path' => $image->name ];
            });
            $promotions[ $promotion->slug ] = [
                'id' => $promotion->id,
                'name' => $promotion->name,
                'label' => $promotion->label,
                'desc' => $promotion->description,
                'slug' => $promotion->slug,
                'url' => $promotion->url,
                'rules' => $promotion->rules,
                'favorited' => $promotion->favorited,
                'finish_at' => $promotion->finish_at,
                'begin_at' => $promotion->begin_at,
                'status' => $promotion->status,
                'images' => $fullPathImages
            ];
        }
        return $promotions;
    }

    public function images()
    {
        return $this->hasMany('App\Model\Promotion\Image', 'id_promotion' );
    }
}
