<?php
namespace App\Model;
use App\Security\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Ong extends Model
{
    protected $table = 'ong';
    protected $guarded = [];

    protected $casts = [
        'accounts' => 'json', 'address' => 'json', 'images' => 'json'
    ];

    public function prepareData( array $data )
    {
        $preparedData = [
            'id_ong_category' => $data['id_category']['value'],
            'social_id' => $data['social_id'],
            'label' => $data['label'],
            'slug' => Str::slug( $data['label'] ),
            'description' => $data['description'],
            'site' => $data['site'],
            'address' => $data['address'],
            'accounts' => $data['accounts']
        ];

        $images = [];
        if( $data['id'] ){
            foreach ( $data['images'] as $key => $image ) {
                switch ( $image['status'] ) {
                    case 1:
                        $images[] = [ 'name' => $image['name'], 'description' => $image['description'], 'status' => 1 ];
                        break;
                    case 2:
                        $images[] = $this->addImage( $image );
                        break;
                    case 0:
                        $this->removeImage( $data['images'][$key][ 'name' ] );
                        break;
                }
            }
        }
        else
            $images = $this->addImages( $data[ 'images' ] );

        $preparedData[ 'images' ] = $images;
        return $preparedData;
    }

    public function addImage( array $data )
    {
        $publicPath = 'img/product/';
        $cloudPath = 'img/ong/';
        $newImageEntity = null;
        $toFile = public_path( $publicPath );
        $cropData = $data['canvas'];

        $tmpImage = \Intervention\Image\ImageManagerStatic::make( $data['data'] );
        $name = Token::generate(9).'.'.explode('/', $tmpImage->mime() )[1];
        $fullName = $publicPath.$name;
        $toFile .= $name;

        $finalImg = $tmpImage->crop( $cropData['width'], $cropData['height'], $cropData['left'] < 0 ? 0 : $cropData['left'], $cropData['top'] < 0 ? 0 : $cropData['top'])
            ->save( $toFile );

        $s3 = Storage::disk( 's3' );
        $fileResult = $s3->put( $cloudPath.$name, $finalImg->getEncoded(), [ 'ACL' => 'public-read'] );

        if( $finalImg && $fileResult ){
//            $resultImg->destroy();
            unlink( $toFile );

            $newImageEntity = [
                'name' => $cloudPath.$name,
                'description' => $data['description'],
                'status' => 1
            ];

        }

        return $newImageEntity;
    }

    public function addImages( array $images ): array
    {
        $imageStorage = [];
        foreach( $images as $image ){
            $image = $this->addImage( $image );
            if( $image )
                $imageStorage[] = $image;
        }
        return $imageStorage;
    }

    /**
     * removes the image from cloud storage
     * @param string $name
     * @return bool
     */
    public function removeImage( string $name )
    {
        return Storage::disk( 's3' )->delete( $name );
    }
}
