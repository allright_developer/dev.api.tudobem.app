<?php
namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Collaborator extends Authenticatable implements JWTSubject
{
    protected $table = 'collaborator';

    protected $casts = [ 'im' => 'json' ];

    protected $guarded = [ 'id' ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
