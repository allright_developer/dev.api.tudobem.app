<?php
namespace App\Model\Tracker;

use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    protected $table = 'click';

    protected $guarded = [];

    public $timestamps = false;

    protected $primaryKey = 'token';

    public $incrementing = false;

    /**
     * Change click status to avoid double conversion and fraud
     * @param string $token click unique token
     * @param int $status the new status
     * @return int 1 in success, 0 otherwise
     */
    public static function changeStatus( string $token, int $status )
    {
        return self::query()->where('token', $token )->update([ 'status' => $status ] );
    }
}
