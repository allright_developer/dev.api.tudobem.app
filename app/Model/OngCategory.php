<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OngCategory extends Model
{
    protected $guarded = [];
    protected $table = 'ong_category';
}
