<?php

namespace App\Model\User;

use App\Security\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class AngelGiftImage extends Model
{
    protected $table = 'angel_gift_image';
    protected $guarded = [ 'id' ];

    public function addImage(array $data, $idAngelGift )
    {
        $newImageEntity = null;
        $imgFullPath = public_path( \App\Mapper\AngelGiftImage::FS_AWS[ 'path' ] );
        $toFile = $imgFullPath;
        $cropData = $data['canvas'];

        $tmpImage = \Intervention\Image\ImageManagerStatic::make( $data['data'] );
        $name = Token::generate(9).'.'.explode('/', $tmpImage->mime() )[1];
        $fullName = \App\Mapper\AngelGiftImage::FS_AWS[ 'path' ].$name;
        $toFile .= $name;

        $resultImg = $tmpImage->crop( $cropData['width'], $cropData['height'], $cropData['left'] < 0 ? 0 : $cropData['left'], $cropData['top'] < 0 ? 0 : $cropData['top'])
            ->resize( 450, null, function( $constraint ){
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save( $toFile );

        $s3 = Storage::disk( 's3' );
        $fileResult = $s3->put( $fullName, $resultImg->getEncoded(), [ 'ACL' => 'public-read' ] );

        if( $resultImg && $fileResult ){
//            $resultImg->destroy();
            unlink( $toFile );

            $imageData = [
                'id_angel_gift' => $idAngelGift,
                'name' => $fullName,
                'description' => $data['description'],
                'storage' => \App\Mapper\AngelGiftImage::FS_AWS[ 'id' ],
                'status' => 1
            ];

            $newImageEntity = self::create( $imageData );
        }

        return $newImageEntity;
    }

    public function addImages( array $images, $idAngelGift )
    {
        foreach( $images as $image ){
            $this->addImage( $image, $idAngelGift );
        }
    }

    public function removeImage( array $data )
    {
        $image = AngelGiftImage::query()->where('name', $data['name'] )->first();
        if( $image ){
            AngelGiftImage::where( 'name', $data['name'] )->update( ['status' => 0] );
            Storage::disk( 's3' )->delete( $data[ 'name' ] );
        }
    }
}
