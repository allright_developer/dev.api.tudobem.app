<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Data extends Model
{
    protected $table = 'user_data';
    protected $guarded = [ 'id' ];

    protected $casts = [
        'data' => 'json'
    ];

    /**
     * @param int $idUser
     * @return array
     */
    public static function getByIdUser( int $idUser ): \stdClass
    {
        $data = DB::select("SELECT data FROM user_data WHERE id_user = :id_user", [ 'id_user' => $idUser ] );
        if( $data )
            $data = $data[0];

        return $data;
    }
}
