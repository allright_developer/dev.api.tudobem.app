<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $table = 'user_invitation';

    public $timestamps = false;
    protected $guarded = [];
}
