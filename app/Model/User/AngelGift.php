<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AngelGift extends Model
{
    protected $table = 'angel_gift';
    public $timestamps = false;
    protected $guarded = ['id'];

    public static function getGifts( int $month = null, int $year = null, int $status = 1 )
    {
        $month = $month ?? date('m' );
        $year = $year ?? date('Y' );
        $sql = "SELECT
                    ag.id, ag.id_category, ag.month, ag.year, ag.label, ag.description,
                    place, i.name img, i.description img_desc, i.status img_status
                FROM angel_gift ag
                INNER JOIN angel_gift_image i ON i.id_angel_gift = ag.id
                WHERE ag.month = :month AND ag.year = :year AND ag.status = :status ORDER BY place";
        return DB::select( $sql, [ 'month' => $month, 'year' => $year, 'status' => $status ] );
    }

    public function prepareData( array $data ): array
    {
        $preparedData = [];

        $preparedData['id_category'] = $data['id_category']['value'];
        $preparedData['place']  = $data['place'];
        $preparedData['label']  = $data['label'];
        $preparedData['description']  = $data['description'];
        $preparedData['month']  = $data['month'];
        $preparedData['year']   = $data['year'];
        $preparedData['status'] = isset( $data['status'] ) ? $data['status'] : 1;

        return $preparedData;
    }

    public function alreadyExists( array $data )
    {
        return $this->query()->where(
            [
                ['month', '=', $data['month'] ],
                ['year', '=', $data['year'] ],
                ['place', '=', $data['place'] ]
            ]
        )->first();
    }
}
