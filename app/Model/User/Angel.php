<?php

namespace App\Model\User;


use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Angel extends Model
{
    protected $table = 'angel';
    protected $primaryKey = 'id_user';
    public $incrementing = false;

    protected $fillable = [ 'crypto_org', 'crypto_user', 'month', 'year', 'id_user' ];

    /**
     * Get ranking by the current month/year
     * @return array
     */
    public static function getList(): array
    {
        $sql = "SELECT a.crypto_org, a.crypto_user, u.name, u.token FROM angel a
                INNER JOIN users u ON u.id = a.id_user
                WHERE a.month = MONTH(NOW()) AND a.year = YEAR(NOW())
                ORDER BY a.crypto_org DESC LIMIT 10";
        $res = DB::select( $sql );

        return $res;
    }

    public function getGifts()
    {
        return [

        ];
    }
}
