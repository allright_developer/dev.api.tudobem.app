<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class ServiceSubscription extends Model
{
    protected $casts = [ 'data' => 'json'];

    protected $guarded = [];

    protected $table = 'user_service_subscription';
}
