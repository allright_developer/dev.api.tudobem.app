<?php

namespace App\Model;

use App\Model\Traits\CreditDataHandler;
use Illuminate\Database\Eloquent\Model;

class UserCredit extends Model
{
    use CreditDataHandler;
    protected $table = 'user_credit';
    protected $guarded = [];
}
