<?php
namespace App\Model;

use App\Model\Traits\DatetimeHandlable;
use App\Model\Traits\SurveyDataHandler;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Survey extends Model
{
    use SurveyDataHandler;
    use DatetimeHandlable;

    protected $table = 'survey';
    protected $guarded = ['id'];

    protected $casts = [ 'credits' => 'json', 'compiled' => 'json', 'image' => 'json' ];

    public function prepareData( array $data )
    {
        return [
            'name'        => $data['name'],
            'slug'        => Str::slug( $data['name'] ),
            'description' => $data['description'],
            'id_category' => $data['id_category']['value'],
            'id_company'  => $data['id_company'] !== null ? $data['company']['value'] : null,
            'id_product'  => $data['id_product'] !== null ? $data['product']['value'] : null,
            'begin_at'    => $this->setDbFormat( $data['begin_at'] ),
            'finish_at'   => $this->setDbFormat( $data['finish_at'] ),
            'credits'     => $data['bonify'] ? $data['credits'] : null,
        ];
    }

    /**
     * @param Model $user
     * @param string $slug
     * @return Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getAllowedSurveyByUserAndSlugCriteria( Model $user, string $slug ): Model
    {
        $survey = Survey::query()
            ->select( [ 'id', 'slug', 'compiled' ] )
            ->whereNotIn('id', UserSurvey::query()->select('id_survey')->where('id_user', $user->id )->get()->toArray() )
            ->where( 'status', 1 )
            ->where('slug', $slug );

        return $survey->firstOrFail();
    }
}
