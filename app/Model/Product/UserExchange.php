<?php

namespace App\Model\Product;

use App\Mapper\ProductUserExchangeMapper;
use App\Model\Product;
use App\Model\UserCredit;
use Illuminate\Database\Eloquent\Model;

class UserExchange extends Model
{
    protected $table = 'user_exchange';

    protected $guarded = ['id'];

    /**
     * @param int $idProduct
     * @param int $idUser
     * @return bool
     */
    public static function persist( int $idProduct, int $idUser )
    {
        $userCredit = UserCredit::query()->where('id_user', $idUser )->first(['crypto', 'id']);
        $product = Product::getWithOffer( $idProduct );
        $response = false;
        if( $product ){
            $price = $product->offer_crypto ?: $product->crypto;
            if( $userCredit->crypto >= $price ){
                $exchange = Product\UserExchange::create([
                    'id_product' => $idProduct,
                    'id_user' => $idUser,
                    'value_at_the_ask_time' => $price,
                    'status' => ProductUserExchangeMapper::STATUS_WAITING
                ]);

                if( $exchange ){
                    $success = $userCredit->decrement( 'crypto', $price );
                    if( $success )
                        $response = $userCredit->crypto;
                }
            }
        }

        return $response;
    }
}
