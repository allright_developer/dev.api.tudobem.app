<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class Favorited extends Model
{
    protected $table = 'product_favorited';

    protected $guarded = [ 'favorited_at' ];
}
