<?php
namespace App\Model\Product;
use App\Model\Product;
use App\Model\Traits\DatetimeHandlable;
use App\Model\Traits\RuntimeDateRange;
use App\Security\Token;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Mixed_;

class Offer extends Model
{
    use DatetimeHandlable, RuntimeDateRange;

    protected $table = 'product_offer';
    protected $guarded = [ 'created_at' ];

    /**
     * Prepare the product data to be persisted
     * @param array $data
     * @return array
     */
    public function prepareData( array $data ): array
    {
        $preparedData = null;

        if( !isset( $data[ 'hash' ] ) )
            $data[ 'hash' ] = Token::generate(9 );

        $product_token = preg_replace('/\s+/', '', $data[ 'product_token' ] );
        $product = Product::select(['id'])->where('token', $product_token )->first();
        if( $product ){
            $data[ 'begin_at' ] = $this->setDbFormat( $data[ 'begin_at' ] );
            $data[ 'finish_at' ] = $this->setDbFormat( $data[ 'finish_at' ] );
            unset( $data[ 'product_token' ] );
            $data[ 'id_product' ] = $product->id;
            $data[ 'status' ] = isset( $data['status'] ) ? $data['status']['value'] : 2;
            $preparedData = $data;
        }
        return $preparedData;
    }
}
