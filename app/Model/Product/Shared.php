<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class Shared extends Model
{
    public $timestamps = false;

    protected $guarded = [ 'shared_at'];
}
