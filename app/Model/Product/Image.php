<?php

namespace App\Model\Product;

use App\Security\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $table = 'product_image';
    protected $guarded = [ 'id' ];

    public function addImage( array $data, $idProduct )
    {
        $newImageEntity = null;
        $localImgFullPath = public_path( \App\Mapper\ProductImage::FS_AWS[ 'path' ] );
        $toFile = $localImgFullPath;
        $cropData = $data['canvas'];

        $tmpImage = \Intervention\Image\ImageManagerStatic::make( $data['data'] );
        $name = Token::generate(9).'.'.explode('/', $tmpImage->mime() )[1];
        $fullName = \App\Mapper\ProductImage::FS_AWS[ 'path' ].$name;
        $toFile .= $name;

        $resultImg = $tmpImage->crop( $cropData['width'], $cropData['height'], $cropData['left'] < 0 ? 0 : $cropData['left'], $cropData['top'] < 0 ? 0 : $cropData['top'])
            ->resize( 450, null, function( $constraint ){
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save( $toFile );

        $s3 = Storage::disk( 's3' );
        $fileResult = $s3->put( $fullName, $resultImg->getEncoded(), [ 'ACL' => 'public-read'] );

        if( $resultImg && $fileResult ){
//            $resultImg->destroy();
            unlink( $toFile );

            $imageData = [
                'id_product' => $idProduct,
                'name' => $fullName,
                'description' => $data['description'],
                'storage' => \App\Mapper\ProductImage::FS_AWS[ 'id' ],
                'status' => 1
            ];

            $newImageEntity = Image::create( $imageData );
        }

        return $newImageEntity;
    }

    public function addImages( array $images, $idProduct )
    {
        foreach( $images as $image ){
            $this->addImage( $image, $idProduct );
        }
    }

    public function removeImage( array $data )
    {
        $image = Image::where('name', $data['name'] )->first();
        if( $image ){
            Image::where( 'name', $data['name'] )->update( ['status' => 0] );
            Storage::disk( 's3' )->delete( $data[ 'name' ] );
        }
    }

    public function getImagesByProductId( $idProduct )
    {
        $images = Image::where( 'id_product', $idProduct )->where('status', 1)
            ->get()->all();

        $data = [];
        if( $images )
            foreach ( $images as $image ){
                $data[] = [
                    'id' => $image->id,
                    'canvas' => [],
                    'name' => $image->name,
//                    'data' => '/statics/icons/icon-256x256.png',
                    'data' => '/'.$image->name , // original
                    'description' => $image->description,
                    'storage' => $image->storage,
                    'status' => $image->status,
                ];
            }

        return $data;
    }
}
