<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Settings extends Model
{
    protected $guarded = [];

    protected $casts = [ 'conf' => 'json' ];

    /**
     * @param array $data
     * @return Settings|bool
     */
    public static function add( array $data )
    {
        self::where('status', 1)->update( [ 'status' => 0 ] );
        $settings = new Settings();
        $settings->fill([
            'created_by' => Auth::user()->id,
            'conf' => [
                'bonus' => $data['bonus'],
                'taxes' => $data['taxes'],
                'crypto' => [
                    'brl' => $data['crypto'] ?? 0.05
                ]
            ]
        ]);

        $res = $settings->save();
        if( $res )
            $res = $settings;

        return $res;
    }
}
