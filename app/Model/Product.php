<?php
namespace App\Model;
use App\Model\Traits\ProductDataHandler;
use App\Security\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    use ProductDataHandler;

    protected $table   = 'product';
    protected $guarded = [ 'id' ];

    protected $casts = [ 'crypto_actions' => 'json', 'cta' => 'json' ];

    public function prepareData( array $data )
    {
        $preparedData = [
            'id_category' => $data['id_category']['value'],
            'id_company' => $data['id_company']['value'],
            'label' => $data['label'],
            'slug' => Str::slug( $data['label'] ),
            'description' => $data['description'],
            'crypto' => $data['crypto'],
            'crypto_cashback_user' => $data['crypto_cashback_user'],
            'crypto_cashback_org' => $data['crypto_cashback_org'],
            'crypto_actions' => $data['crypto_actions'],
            'cta'  => $this->prepareCta( $data['cta'] ),
            'fiat' => $data['fiat']
        ];

        if( !$data['id'] )
            $preparedData['token'] = Token::generate();

        return $preparedData;
    }

    public function prepareCta( $cta )
    {
        $res = null;
        if( is_array( $cta ) ){
            if( $cta['type']['value'] === 1 ){
                $res = [
                    'type'  => $cta['type']['value'],
                    'flat'  => $cta['flat'],
                    'label' => $cta['label'],
                    'color' => $cta['color'],
                    'link'  => $cta['link']
                ];
            }
        }
        return $res;
    }
}
