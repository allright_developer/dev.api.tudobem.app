<?php
namespace App\Model\Stat;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    protected $table   = 'stat_user';

    public $timestamps = false;

    protected $guarded = [];

    protected const DATE_FORMAT = 'Y-m-d H:i:s';

    public static function getDefaultStat(): array
    {
        $stats = [];

        $time = now()->setMinute(0)->setSecond(0);
        $lastHour = $time->format( self::DATE_FORMAT );
        $lastHourTotal = DB::select( "SELECT total FROM stat_user WHERE end_at = :end_at AND period = 'PT1H'", ['end_at' => $lastHour ] );
        $currentHourTotal = DB::select( "SELECT SUM(total) total FROM stat_user WHERE start_at >= :start_at AND period = 'PT15M'", [ 'start_at' => $lastHour ] );

        $stats[ 'hourly' ] = [ 'previous' => $lastHourTotal ? $lastHourTotal[0]->total : 0, 'current' => $currentHourTotal ? $currentHourTotal[0]->total : 0 ];

        $today = $time->format('Y-m-d' );
        $lastDay = DB::select( "SELECT total FROM stat_user where period = 'P1D' ORDER BY start_at DESC LIMIT 1" );
        if( $lastDay )
            $lastDay = $lastDay[0]->total;
        else
            $lastDay = 0;
//        dd( DB::select( "SELECT SUM(total) total FROM stat_user where period = 'PT30M' AND DATE_FORMAT( start_at, '%Y-%m-%d' ) = :today", [ 'today' => $today ] ), $today );
        $currentDay = DB::select( "SELECT SUM(total) total FROM stat_user where period = 'PT30M' AND DATE_FORMAT( start_at, '%Y-%m-%d' ) = :today", [ 'today' => $today ] )
            [0]->total;

        $stats[ 'daily' ] = [ 'previous' => $lastDay, 'current' => $currentDay ?? 0 ];

        return $stats;
    }
}
