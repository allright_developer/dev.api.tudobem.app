<?php

namespace App\Model;

use App\Security\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Company extends Model
{
    protected $table = 'company';
    protected $guarded = ['id'];

    public function prepareData( array $data )
    {
        $preparedData = [
            'label' => $data['label'],
            'slug' => Str::slug( $data['label'] ),
            'description' => $data['description'],
            'id_category' => $data['id_category']['value'],
            'cnpj' => $data['cnpj'] ?? null,
            'status' => $data['status']['value']
        ];

        if( !$data['id'] )
            $preparedData['token'] = Token::generate();

        return $preparedData;
    }
}
