<?php
/**
 * Data grid Manager for DataTables
 * User: tohdi
 * Date: 08/04/18
 * Time: 21:35
 */
namespace App\Service\TableGrid;

use Illuminate\Database\Eloquent\Model;

class DataTable
{
    public $addJoins = true;
    /**
     * @var Model
     */
    public $model;

    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    public $query;

    public function __construct( Model $model )
    {
        $this->model = $model;
        $this->query = $model->newQuery();
    }

    /**
     * @param array $paginationParams
     * @return $this
     */
    public function setPagination( $paginationParams )
    {
        $table = $this->model->getTable();
        $skip = $paginationParams['page'] <= 1 ? 0 : ($paginationParams['rowsPerPage'] * $paginationParams['page']) - $paginationParams['rowsPerPage'];
        $this->query->skip( $skip )->take( $paginationParams['rowsPerPage'] )
             ->orderBy( $table.'.'.$paginationParams[ 'sortBy' ], $paginationParams[ 'descending' ] ? 'desc' : 'asc' );

        return $this;
    }

    /**
     * @param array $filterParams
     * @return $this
     */
    public function setFilter( $filterParams )
    {
        if( !empty( $filterParams['value'] ) ){

            $value = is_string( $filterParams['value'] ) ? $filterParams['value'] : $filterParams['value']['value'];

            if( $filterParams['operator']['value'] === 'like' )
                $filterParams['value'] = '%'.$value.'%';

            if( $filterParams['field']['type'] === 'datetime' )
                $value = \DateTime::createFromFormat('d/m/Y H:i', $value )->format('Y-m-d H:i');

            $this->query->where( $filterParams['field']['value'], $filterParams['operator']['value'], $value );
        }

        return $this;
    }

    /**
     * @param string|array $columns
     * @return $this
     */
    public function setColumns( $columns = '*' )
    {
        $table = $this->model->getTable();
        $columns = collect( $columns );
        $columns = $columns->map(function( $item, $index ) use( $table ) {
            return $table.'.'.$item;
        })->toArray();
//        dd( $columns );

        $this->query->select( $columns );
        return $this;
    }
    /**
     * @return int
     */
    public function getTotalRows()
    {
        return $this->query->count( $this->model->getTable().'.'.$this->model->getKeyName() );
    }

    /**
     * Auto discover method to add joins to the query and get label related to the foreign key and use it in the User View
     * @param array $selectColumns
     * @return $this
     */
    public function hasJoins( array $selectColumns )
    {
        if( $this->addJoins ){
            $foreignColumns = [];
            foreach ( $selectColumns as $column ){
                if( preg_match('/^id_/', $column ) ){
                    $foreignColumns[] = $column;
                    $columns = explode('_', $column,2);
                    $this->query->join( $columns[1], $columns[1].'.id', '=', $this->model->getTable().'.'.$column, 'left' );
                    $this->query->addSelect( $columns[1].".label as {$columns[1]}_label" );
                }
            }
        }

        return $this;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getData()
    {
        return $this->query->get();
    }
}
