<?php
namespace App\Service\TheChecker;

use ExceptionLog\ExceptionLog;
use ExceptionLog\LevelMapper;
use GuzzleHttp\Client;

/**
 * TheChecker Api for email validator
 * @author adriano.alves <adriano@venddi.com.br>
 * @see https://thechecker.co/api
 * @version 0.2
 */
class EmailValidator
{
    protected $endpoint = 'https://api.thechecker.co/v2/verify?';

    protected $apiKey = '43d701447f5d9e87713a5fcf4a64cb4135baa7f08930a9038e054383ef7acd8b';

    /**
     * curl client to request an email validation
     * @var Client
     */
    protected $client;
    /**
     * @var array
     */
    public static $responseMessages = [
        'reason' => [
            'invalid_email' => [
                'description' => 'O formato do email é inválido', 'score' => 0
            ],
            'invalid_domain' => [
                'description' => 'O domínio não é válido ou não possui um servidor de Email SMTP', 'score' => 0
            ],
            'rejected_email' => [
                'description' => 'O endereço de email foi rejeitado pelo servidor de Email, email não existe', 'score' => 0
            ],
            'accepted_email' => [
                'description' => 'Endereço de Email foi aceito pelo servidor SMTP', 'score' => 7
            ],
            'low_quality' => [
                'description' => 'Endereço de email possui baixa qualidade e apresenta riscos de entrega', 'score' => 2
            ],
            'low_deliverability' => [
                'description' => 'Email apresenta baixa entregabilidade, não é possível garantí-la', 'score' => 3
            ],
            'no_connect' => [
                'description' => 'Não foi possível se conectar ao servidor SMTP', 'score' => 0
            ],
            'timeout' => [
                'description' => 'A tentativa de conexão excedeu o tempo limite', 'score' => 1
            ],
            'unavailable_smtp' => [
                'description' => 'Servidor de email Indisponível para processar solicitação', 'score' => 1
            ],
            'role' => [
                'description' => 'Email de Grupo ou Cargo corporativo', 'score' => 2
            ],

        ],
        /**
         * descrição e qualidade dos tipos de respostas sobre a consulta de email, a escala dos statuses vai de 0 a 3,
         * sendo 0 ruim e 3 muito bom
         */
        'result' => [
            'deliverable' => [
                'description' => 'Entregabilidade OK', 'quality' => 3
            ],
            'undeliverable' => [
                'description' => 'Impossível Entregar email/Spamtrap', 'quality' => 0
            ],
            'risky' => [
                'description' => 'Email apresenta Riscos de Envio', 'quality' => 1
            ],
            'unknown' => [
                'description' => 'Indeterminado', 'quality' => 1
            ]
        ]
    ];

    public function __construct($apiKey = null)
    {
        if($apiKey)
            $this->apiKey = $apiKey;

        $this->client = new Client();
    }

    public function validate($email)
    {
        try {
            $response = [ 'ok' => false ];
            $query = \http_build_query( [ 'api_key' => $this->apiKey, 'email' => $email ] );
            $response = $this->client->get( $this->endpoint.$query )->getBody()->getContents();
            $response = $this->parseResponse( $response );

        } catch ( \Exception $e ) {
            $response['message'] = 'Formato de email Inválido';

        } catch ( \Exception $e ) {

            $response['message'] = 'Erro no servidor de validação';
            ExceptionLog::persist( $e, LevelMapper::LEVEL_ERROR_APPLICATION, [ 'system' => 'thechecker v2' ] );
        }
        return $response;
    }

    protected function parseResponse( $response )
    {
        $r = null;
        $data = json_decode($response, true);

        if( is_array( $data ) ){

            $r = ['ok' => true];

            $r['quality'] = self::$responseMessages['result'][$data['result']]['quality'];
            $r['score'] = self::$responseMessages['reason'][$data['reason']]['score'];
            $r['disposable'] = $data['disposable'];
            $r['role'] = $data['role'];
            $r['accept_all'] = $data['accept_all'];
            $r['email'] = $data['did_you_mean'] !== null ?: $data['email'];

            if($r['score'] > 0) {
                if(!$data['disposable'])
                    $r['score']++;
                if(!$data['role'])
                    $r['score']++;
                if(!$data['accept_all'])
                    $r['score']++;
            }

            $r['original_data'] = $data;
        } else {
            $r = ['ok' => false, 'message' => 'Erro na consulta'];
            ExceptionLog::persist( new \Exception('parsing error'), LevelMapper::LEVEL_ERROR_APPLICATION, [ 'system' => 'thechecker v2'] );
        }

        return $r;
    }
}
