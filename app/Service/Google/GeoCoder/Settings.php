<?php
namespace App\Service\Google\GeoCoder;

class Settings
{
    const URL = 'https://maps.googleapis.com/maps/api/geocode/json?';

    const AVOID = [
        'tolls' => 'tolls',
        'highways' => 'highways',
        'ferries' => 'ferries',
        'indoor' => 'indoor',
    ];

    const MODE = [
        'bicycling' => 'bicycling',
        'driving' => 'driving',
        'transit' => 'transit',
        'walking' => 'walking',
    ];

    const UNIT = [
        'imperial' => 'imperial',
        'metric' => 'metric'
    ];

    const LANGUAGE = 'pt-BR';

}
