<?php
namespace App\Service\Google\Fcm;

use GuzzleHttp\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Topic;

class WebPush
{
    const DEFAULT_API_URL = 'https://fcm.googleapis.com/fcm/send';
    /**
     * @var Client
     */
    public Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Send a message to a group of interest: a Topic
     * @param array $data icon, title, body, subtitle, badge, sound, ttl, click_action
     * @param string|null $topic
     * @return bool
     */
    public function send( array $data, string $topic = null ): bool
    {
        $data = $this->setData( $data );
        $client = new \sngrl\PhpFirebaseCloudMessaging\Client;
        $client->setApiKey( env('FIREBASE_SERVER_KEY') );
        $notification = new Notification( $data['title'], $data['body'] );
        $notification->setClickAction( $data['click_action'] )
        ->setBadge($data['icon'] )
        ->setSubtitle( $data['subtitle'] )
        ->setIcon( $data['icon'] )
        ->setBadge( $data['icon'] )
        ->setData([ 'actions' => $data[ 'click_action' ] ] )
        ->setSound( [100,50,200,100,300] )
        ->setTimeToLive($data['ttl']);

        $message = new Message();
        $message->addRecipient( new Topic( $topic ) )
        ->setNotification( $notification );

        $response = $client->send( $message );
        return $response->getStatusCode() === 200;
    }

    /**
     * Subscribe an user in a group of interest called topic
     * @param string $token user generated id
     * @param string $topic topic name
     * @return bool it returns true on subscription success
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function subscribe( string $token, string $topic ): bool
    {
        $endpoint = "https://iid.googleapis.com/iid/v1/{$token}/rel/topics/${topic}";
        $response = $this->client->post( $endpoint, [ 'headers' => $this->getDefaultHeaders() ] );
        return $response->getStatusCode() === 200;
    }

    public function setData( array $data )
    {
        $data = [
            'title' => isset( $data['title'] ) ?: 'tudobem.app',
            'subtitle' => isset( $data['subtitle'] ) ?: 'faça o bem e ganhe também',
            'body' => isset( $data['body'] ) ?: 'Faça parte do tudobem!',
            'icon' => isset( $data['icon'] ) ?: 'https://staging.tudobem.app/statics/icons/apple-icon-180x180.png',
            'badge' => isset( $data['icon'] ) ?: 'https://staging.tudobem.app/statics/icons/apple-icon-180x180.png',
            'ttl' => isset( $data['ttl'] ) ?: 15,
            'click_action' => isset( $data['click_action'] ) ?: 'https://tudobem.app',
        ];

        return $data;
    }

    /**
     * @return string[]
     */
    protected function getDefaultHeaders()
    {
        return [
            'Content-Type' => 'application/json',
            'Authorization' => 'key='.config('broadcasting.connections.fcm.server_key' )
        ];
    }
}
