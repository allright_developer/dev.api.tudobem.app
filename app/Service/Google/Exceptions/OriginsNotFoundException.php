<?php
namespace App\Service\Google\Exceptions;
use Throwable;

class OriginsNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct( 'Please add at least one origin', 22, null );
    }
}
