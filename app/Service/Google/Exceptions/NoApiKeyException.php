<?php
namespace App\Service\Google\Exceptions;
use Throwable;

class NoApiKeyException extends \Exception
{
    public function __construct()
    {
        parent::__construct( 'An API key is needed: please access your GCP account and create it.', 1, null );
    }
}
