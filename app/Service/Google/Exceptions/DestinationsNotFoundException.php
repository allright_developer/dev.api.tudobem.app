<?php
namespace App\Service\Google\Exceptions;
use Throwable;

class DestinationsNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct( 'Please add at least one destination', 21, null );
    }
}
