<?php
namespace App\Service\Google\Exceptions;

class AddressTypeException extends \Exception
{
    public function __construct($message = "")
    {
        parent::__construct( $message, 3, null );
    }
}
