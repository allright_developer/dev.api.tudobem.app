<?php
namespace App\Service\Google\Exceptions;

class InvalidResponseException extends \Exception
{
    public function __construct()
    {
        parent::__construct( 'An invalid response returned from GCP Service', 4, null );
    }
}
