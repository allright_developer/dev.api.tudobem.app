<?php
namespace App\Service\Lead\Transporter;

use App\Model\Lead\Lead;

class Http implements Transporter
{
    public \stdClass $settings;

    public string $type;

    public $client;

    public function __construct( $settings, $type = 'post' )
    {
        $this->settings = $settings;
        $this->type = $type;

        $this->client = \curl_init();
        \curl_setopt( $this->client, \CURLOPT_RETURNTRANSFER, 1 );
        \curl_setopt( $this->client, \CURLOPT_TIMEOUT, 7 );
    }

    /**
     * Main method to execute the lead sending
     * @param array $leads Leads to send
     * @param \stdClass|null $mapping Data mapping to convert the form field names to endpoint field names
     * @return bool
     */
    public function doTransport( array $leads, \stdClass $mapping = null ): bool
    {
        $res = false;
        foreach ( $leads as $lead ){
            $dataToSend = [];
            foreach ( $mapping as $field => $value )
                $dataToSend[ $field ] = $lead['data'][ $field ] ?? '';

            // @todo implement other types, like aws services, ftp, jobs ...
            switch( $this->type ){
                case 'post': case 'jwt':
                    \curl_setopt( $this->client, CURLOPT_POSTFIELDS, http_build_query( $dataToSend ) );
                break;

                case 'get':
                    if( isset( $this->settings->params ) )
                        $dataToSend = \array_merge( $dataToSend, $this->settings->params );

                    \curl_setopt( $this->client, CURLOPT_URL, $this->settings->endpoint.'?'.http_build_query( $dataToSend ) );
                break;
            }

            $result   = \curl_exec( $this->client );
            $response = \curl_getinfo( $this->client );
            // each lead should be changed with result data
            if( $response[ 'http_code' ] < 400 )
                $toUpdate = [ 'delivered_at' => \date( 'Y-m-d H:i:s'), 'status' => 2, 'response' => [ 'data' => $result ] ];
            else
                $toUpdate = [ 'response' => [ 'http_code' => $response[ 'http_code' ], 'when' => \date('Y-m-d H:i:s' ), 'server_response' => $result ] ];

            Lead::where( 'token', $lead['token' ] )
                ->update( $toUpdate );
        }

        return $res;
    }

    /**
     * @todo add http-jwt switch with bearer validity check by ttl
     * @param string|null $type
     * @return Http
     */
    public function resolveType( string $type = null ): Http
    {
        if( $type )
            $this->type = $type;

        $params = '';
        if( isset( $this->settings->params ) )
            $params = '?'.\http_build_query( $this->settings->params );

        switch( $this->type ){
            case 'post':
                \curl_setopt( $this->client, CURLOPT_POST, true );
            break;
            case 'jwt':
                // @todo add a verification for the token validity and token renew process
                \curl_setopt( $this->client, CURLOPT_HEADER, [ 'Content-Type: application/json', 'Authorization: Bearer '.$this->settings->bearer ] );
            break;
        }

        if( $this->type !== 'get' )
            \curl_setopt( $this->client, CURLOPT_URL, $this->settings->endpoint.$params );

        return $this;
    }
}
