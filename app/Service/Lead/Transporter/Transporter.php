<?php
namespace App\Service\Lead\Transporter;

interface Transporter
{
    public function doTransport( array $leads, \stdClass $mapping = null ): bool;
}
