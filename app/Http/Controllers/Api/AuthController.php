<?php
namespace App\Http\Controllers\Api;

use App\Events\EmailIntegrationEvent;
use App\Events\PushSubscription;
use App\Formatter\UserInput;
use App\Http\Requests\RegisterAuthRequest;
use App\Model\Settings;
use App\Model\User;
use App\Model\UserCredit;
use App\Model\UserLog;
use App\Security\Token;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthController extends Controller
{
    public $loginAfterSignup = true;

    /**
     * @param RegisterAuthRequest $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @calls EmailIntegrationEvent, PushSubscription, UserCreated
     */
    public function register( RegisterAuthRequest $request )
    {
        $response = [ 'success' => false, 'data' => [] ];
        return $response;
        try{
            // registering user
            $passwd = UserInput::phone( $request->post('phone' ) );
            $request->request->set( 'password', $passwd );

            $user = User::firstOrCreate([
                'email' => $request->post('email' ),
            ],[
                'token' => Token::generate( 8 ),
                'name'  => $request->post('name' ),
                'ddd' => substr( $passwd,0, 2 ),
                'phone' => substr( $passwd,2, 9 ),
                'password'  => bcrypt( $passwd ),
            ]);
            event( new EmailIntegrationEvent( $user ) );

            $pushToken = $request->post('ptkn' );
            if( $pushToken )
                event( new PushSubscription( $user, 'institutional', $pushToken) );
            // login after register
            if( $this->loginAfterSignup ) {
                $response = $this->login( $request, $user );
            }
            else{
                $credits = UserCredit::where( 'id_user', $user->id )->first();
                $response = response()->json([
                    'success' => true,
                    'data' => [
                        'token' => $token,
                        'user' => $user->only( ['name', 'email', 'email_verified_at' ] ),
                        'credits' => $credits->only( ['crypto', 'fiat'] ),
                        'level' => 0
                    ]
                ], 200);
            }
        }
        catch ( HttpResponseException $e ){
            $response = $e->getResponse();
        }
        catch (\Exception $e ){
            $response = response()->json([
                'success' => false,
                'data' => ['message' => $e->getMessage().' : '.$e->getFile().' : '.$e->getLine(), 'type' => 'error_register' ]
            ], 401);
        }

        return $response;
    }

    public function login( Request $request, $user = null )
    {
        $credentials = $request->request->all();
        $password = UserInput::phone( $credentials['password'] );
        $credentials = [ 'password' => $password, 'email' => $credentials['email'] ];
        $response = null;

        if ( $token = $this->guard()->attempt( $credentials ) ){
            $c = [ 'token','name', 'email', 'gender', 'ddd', 'phone', 'email_verified_at', 'phone_verified_at' ];
            $user = Auth::user();
            $credits = UserCredit::where( 'id_user', $user->id )->first();
            $response = response()
                ->json([
                        'success' => true,
                        'data' => [
                            'token' => $token,
                            'user' => $user->only( $c ),
                            'user_data' => User\Data::where( 'id_user', $user->id )->first( [ 'data' ] )->data,
                            'credits' => $credits->only( [ 'crypto', 'crypto_org', 'fiat' ] ),
                            'level' => 0
                        ]
                    ], 200);
            // logging user login
            UserLog::create([
                'id_user' => $user->id, 'action_type' => \App\Mapper\UserLog::USER_LOGIN['id'],
                'description' => 'Login Efetuado', 'data' => null
            ]);
        }
        else{
            $user = User::where( 'email', $request->post( 'email' ) )->first();
            $responseData = [ 'data' => [ 'error' => 'login_error'], 'success' => false, 'messages' => null ];
            if( $user )
                $responseData = [ 'data' => [ 'error' => 'phone_not_related' ], 'success' => false, 'messages' => null ];

            $response = response()->json( $responseData, 401);
        }

        return $response;
    }

    public function adminLogin( Request $request )
    {
        $response = null;
        $credentials = $request->request->all();
        $token = $this->guard()->attempt( $credentials );
        if ( $token  ){
            $c = [ 'name', 'email', 'phone', 'role', 'im' ];
            $user = Auth::user();
            $response = response()
                ->json( [ 'token' => $token, 'user' => $user->only( $c ) ], 200 );
        }
        else{
            $user = User::where( 'email', $request->post( 'email' ) )->first();
            $responseData = [ 'error' => 'login_error', 'success' => false, 'messages' => null ];
            if( $user )
                $responseData = [ 'data' => [ 'error' => 'error_credential'], 'success' => false, 'messages' => null ];

            $response = response()->json( $responseData, 401);
        }

        return $response;
    }

    public function logout( Request $request )
    {
        try {
            $this->guard()->logout();
            return response()->json([
                'status' => 'success',
                'msg' => 'Logout efetuado com sucesso.'
            ], 200);

        } catch ( JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Pedimos perdão, não conseguimos desconectar o usuário.'
            ], 421);
        }
    }

    /**
     * Get authenticated user
     */
    public function user( Request $request )
    {
        try{
            $user = Auth::user();

            $response = response()->json([
                'success' => true,
                'data' => [ 'token']
            ]);
        }catch ( TokenExpiredException $e ){
            $response = response()->json(['success' => false, 'data' => [ 'message' => $e->getMessage() ] ], 401 );
        }catch ( TokenInvalidException $e ){
            $response = response()->json(['success' => false, 'data' => [ 'message' => $e->getMessage() ] ], 403 );
        }catch ( JWTException $e ){
            $response = response()->json(['success' => false, 'data' => [ 'message' => $e->getMessage() ] ], 500 );
        }catch ( \Exception $e ){
            $response = response()->json(['success' => false, 'data' => [ 'message' => $e->getMessage() ] ], 404 );
        }

        return $response;
    }

    public function confirm( Request $request )
    {
        $token = $request->post('token' );
        $response = null;
        try{
            $user = ( new User )->confirmEmail( $token );

            $bonus = \json_decode( Settings::query()->where('status', 1 )->first(['conf']) )->conf->bonus;
            $cryptos = $bonus->emailValidation;
            UserCredit::where( 'id_user', $user->id )->increment( 'crypto', $cryptos ?? 5 );
            $result = [ 'crypto' => $cryptos, 'email_verified_at' => $user->email_verified_at, 'name' => $user->name ];
            $response = response()->json( $result );
        }
        catch ( \Exception $e ){
            $response = response()->json( [ 'message' => $e->getMessage(), 'code' => $e->getCode() ] );
        }

        return $response;
    }
    /**
     * Refresh JWT token
     */
    public function refresh()
    {
        $response = null;
        if ($token = $this->guard()->refresh()) {
            $response = response()
                ->json(['success' =>  true, 'token' => $token ], 200)
                ->header('Authorization', $token);
        }
        else
            $response = response()->json( [ 'error' => 'refresh_token_error'], 422 );

        return $response;
    }

    /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard();
    }
}
