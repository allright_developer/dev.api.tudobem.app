<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\User\AngelGift;
use App\Model\User\AngelGiftImage;
use App\Service\TableGrid\DataTable;
use Illuminate\Http\Request;

class AngelGiftController extends Controller
{
    public function post( Request $request )
    {
        try {
            $response = ['message' => 'ok' ];
            $gifts = $request->post( 'gifts' );
            if( is_array( $gifts ) ){
                foreach( $gifts as $gift ){

                    $gift['month'] = $request->post('month');
                    $gift['year'] = $request->post('year');

                    if( isset( $gift['id'] ) ){
                        $this->update( $gift );
                    }
                    else{
                        $angelGift = new AngelGift;
                        if( $angelGift->alreadyExists( $gift ) === null ){
                            $id = $angelGift->fill( $angelGift->prepareData( $gift ) )->save();
                            if( $id )
                                (new AngelGiftImage)->addImages( $gift['images'], $angelGift->id );
                        }
                    }
                }
            }

            $response = response()->json( $response );
        }
        catch ( \Exception $e ){
            $response = response()->json([ 'msg' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ], 421 );
        }

        return $response;
    }

    public function get( $month = null, $year = null )
    {
        try {
            $response = AngelGift::getGifts( $month, $year );
            $response = response()->json( $response );
        }
        catch ( \Exception $e ){
            $response = response()->json([ 'msg' => 'Os Prêmios para o mês não foram encontrados' ], 421 );
        }

        return $response;
    }

    protected function update( array $data )
    {
        $angelGift = AngelGift::query()->find( $data['id'] );
        $angelGift->update( $angelGift->prepareData( $data ) );

        if( $data[ 'images' ] )
            foreach ( $data['images'] as $image ){

                switch ( $image['status'] ){
                    case 1:
                        AngelGiftImage::query()->where('name', $image['name'] )->update( [ 'description' => $image['description' ] ] );
                        break;
                    case 2:
                        (new AngelGiftImage)->addImage( $image, $data['id'] );
                        break;
                    case 0:
                        (new AngelGiftImage)->removeImage( $image );
                        break;
                }
            }
    }

    public function dataGrid( Request $request )
    {
        $columns = $request->post( 'columns' );
        $pd = $request->post('pagination' );
        $fd = $request->post( 'filter');

        $dt = new DataTable( new AngelGift );
        $totalRows = $dt->setColumns( $columns )->hasJoins( $columns )->setFilter( $fd )->getTotalRows();

        $dt->setPagination( $pd );

        return response()->json( [ 'total' => $totalRows, 'data' => $dt->getData() ] );
    }
}
