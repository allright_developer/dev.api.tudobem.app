<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Model\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function post( CompanyRequest $request )
    {
        $data = $request->post();
        $response = null;
        try {
            if( $data['id'] )
                return $this->update( $data );
            // saving the new entity
            $entity = new Company;
            $entity->fill( $entity->prepareData( $data ) );
            $entity->save();
            $response = response()->json( [ 'success' => true, 'data' => [ 'id' => $entity->id ] ] );

        }catch ( \Exception $e ){
            $response = response()->json([ 'success' => false, 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 500 );
        }

        return $response;
    }

    public function get( Request $request, $status = 1 )
    {
        $options = Company::query()
            ->where( 'status', $status )
            ->select('id as value', 'id_category', 'label', 'description' )
            ->get()
            ->mapWithKeys(function( $option, $key ){
                return [ $option->value => $option ];
            });
        return response()->json( [ 'success' => true, 'options' => $options ] );
    }

    protected function update( array $data )
    {
        $response = null;
        $response = response()->json( [ 'success' => true, 'data' => null, 'message' => 'Pesquisa atualizada com sucesso' ] );
        return $response;
    }
}
