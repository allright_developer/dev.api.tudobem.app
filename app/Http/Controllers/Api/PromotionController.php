<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PromotionRequest;
use App\Model\Promotion;
use App\Service\TableGrid\DataTable;
use ExceptionLog\ExceptionLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PromotionController extends Controller
{
    public function post( PromotionRequest $request )
    {
        $response = null;
        $data = $request->post();
        try{
            if( $data['id'] )
                return $this->update( $data );

            $entity = new Promotion;
            $preparedData = $entity->prepareData( $data );

            $isLessThan = $entity->isFinishLessThanBegin( $preparedData );

            if( $isLessThan  )
                return response()->json( [ 'success' => false, 'errors' => ['dates' => [ 'A data de Finalização precisa ser maior que a Data de Início' ] ]  ], 422 );

            $entity->fill( $preparedData )->save();

            (new Promotion\Image)->addImages( $data['images'], $entity->id );
            $response = response()->json( [ 'ok' => true ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ], 400 );
        }

        return $response;
    }

    public function update( array $data )
    {
        $entity = Promotion::find( $data['id'] );
        $preparedData = $entity->prepareData( $data );
        $isLessThan = $entity->isFinishLessThanBegin( $preparedData );

        if( $isLessThan  )
            return response()->json( [ 'success' => false, 'errors' => ['dates' => [ 'A data de Finalização precisa ser maior que a Data de Início' ] ] ], 422 );

        $entity->update( $preparedData );

        if( $data[ 'images' ] )
            foreach ( $data['images'] as $image ){

                switch ( $image['status'] ){
                    case 1:
                        Promotion\Image::where('name', $image['name'] )->update( [ 'description' => $image['description' ] ] );
                    break;
                    case 2:
                        (new Promotion\Image )->addImage( $image, $data['id'] );
                    break;
                    case 0:
                        (new Promotion\Image )->removeImage( $image );
                    break;
                }
            }

        return response()->json( [ 'data' => [ 'ok' => true ] ] );
    }

    public function favorite( Request $request )
    {
        $response = null;
        $data = $request->post();

        try{
            $idUser = Auth::user()->id;
            $promotion = Promotion\User::where(
                [
                    [ 'id_promotion', '=', $data['id_promotion'] ],
                    [ 'id_user', '=', $idUser ]
                ]
            )->first();

            if( $promotion )
                $promotion->update( [ 'favorited' => $data[ 'favorited' ] ] );
            else
                $promotion = Promotion\User::create([
                    'id_user' => $idUser,
                    'id_promotion' => $data['id_promotion'],
                    'favorited' => $data['favorited']
                ]);
            $response = response()->json( [ 'favorited' => $promotion->favorited ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }
    /**
     * Get Promotions
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function get( Request $request, $slug = null )
    {
        $response = null;
        try{
            $promotion = new Promotion;

            $response = response()->json( [ 'data' => $promotion->getBySlug( $slug ) ] );

        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function getById( Request $request, $id )
    {
        $response = null;
        try{
            $entity = Promotion::query()->find( $id );
            $data = $entity->getAttributes();
            if( $data ){
                $images = Promotion\Image::getImagesByPromotionId( $entity->id );
                $data[ 'images' ] = $images;
            }

            $data[ 'finish_at' ] = $entity->getFinishAtAttribute();
            $data[ 'begin_at' ] = $entity->getBeginAtAttribute();
            $response = response()->json( [ 'data' => $data ] );

        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function toggleStatus( Request $request )
    {
        $response = null;
        $data = $request->post();

        try{
            if( $data['id'] ){
                Promotion::query()->where('id', $data['id'] )->update([ 'status' => $data['status' ] ] );
            }
            $response = [ 'ok' => 'true' ];
        }catch ( \Exception $e ){
            ExceptionLog::persist( $e, ExceptionLog\LevelMapper::LEVEL_ERROR_DATABASE );
            $response = ['ok' => false, 'message' => $e->getMessage() ];
        }

        return response()->json( $response );
    }
    public function dataGrid( Request $request )
    {
        $columns = $request->post('columns' );
        $pd = $request->post('pagination' );
        $fd = $request->post( 'filter');

        $dt = new DataTable( new Promotion );
        $totalRows = $dt->setColumns( $columns )->hasJoins( $columns )->setFilter( $fd )->getTotalRows();

        $dt->setPagination( $pd );

        return response()->json( [ 'total' => $totalRows, 'data' => $dt->getData() ] );
    }
}
