<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Stat\Users;
use Illuminate\Http\Request;

class StatsController extends Controller
{
    /**
     * @todo make the request endpoint to get chart data
     * @param Request $request
     * @param string $entity
     * @param string $period
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function get( Request $request, $entity, $period = 'day' )
    {
        $response = null;
        $data = $response->get();
        try{
            $response = response()->json( $data );

        }catch (\Exception $e ){
            $response = response()->json( ['message' => 'Erro', 'data' => [ 'message' => $e->getMessage(), 'line' => $e->getLine(),'file' => $e->getFile() ] ] );
        }

        return $response;
    }

    public function cardStats( Request $request, $entity, $type = 'daily' )
    {
        $response = null;

        try{
            $data = null;

            if( $request->get('test' ) ){
                $data = [
                    'current' => \random_int( 20, 1000 ), 'previous' => \random_int( 20, 900 )
                ];
            }else{
                $data = Users::getDefaultStat();
            }
            $response = response()->json( $data );
        }catch (\Exception $e ){
            $response = response()->json( ['message' => 'Erro', 'data' => [ 'message' => $e->getMessage(), 'line' => $e->getLine(),'file' => $e->getFile() ] ] );
        }

        return $response;
    }
}
