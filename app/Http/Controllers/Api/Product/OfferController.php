<?php
namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\OfferRequest;
use App\Model\Product;
use App\Service\TableGrid\DataTable;
use Illuminate\Http\Request;

class OfferController extends Controller
{

    public function post( OfferRequest $request )
    {
        $data = $request->post();
        $response = null;

        try {
            if( $data['hash'] )
                return $this->update( $data );
            // saving the new survey
            $entity = new Product\Offer;

            $preparedProductOfferData = $entity->prepareData( $data );

            if( $preparedProductOfferData ){

                $entity->fill( $preparedProductOfferData );
                // validate the unique lifetime range
                $collides = $entity->collidesWithOtherOffer( $preparedProductOfferData );
                $isLessThan = $entity->isFinishLessThanBegin( $preparedProductOfferData );

                if( $isLessThan  )
                    return response()->json( [ 'success' => false, 'message' => [ 'A data de Finalização precisa ser maior que a Data de Início' ]  ], 421 );
                if( $collides  )
                    return response()->json( [ 'success' => false,  'message' => [ 'A data de Início colide com a vigência de outra Oferta' ] ], 421 );

                $entity->save();
                $response = response()->json( [ 'success' => true, 'id' => $entity->id ] );
            }
            else
                $response = response()->json( [ 'success' => false,  'message' => ['O produto com o token informado não existe'] ] );

        }catch ( \Exception $e ){
            $response = response()->json([ 'success' => false, 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 500 );
        }

        return $response;
    }

    protected function update( array $data )
    {
        $response = null;
        $entity = Product\Offer::query()->where( 'hash', $data['hash'] )->first();

        if( $entity ){

            $preparedProductOfferData = $entity->prepareData( $data );
            // validate the unique lifetime range
            $collides = $entity->collidesWithOtherOffer( $preparedProductOfferData );
            $isLessThan = $entity->isFinishLessThanBegin( $preparedProductOfferData );

            if( $isLessThan  )
                return response()->json( [ 'success' => false, 'message' => [ 'A data de Finalização precisa ser maior que a Data de Início' ]  ] );
            if( $collides  )
                return response()->json( [ 'success' => false,  'message' => [ 'A data de Início colide com a vigência de outra Oferta' ] ] );

            $entity->update([
                'description' => $data['description'],
                'label'     => $data['label'],
                'crypto'    => $data['crypto'],
                'fiat'      => $data['fiat'],
                'begin_at'  => $preparedProductOfferData['begin_at'],
                'finish_at' => $preparedProductOfferData['finish_at'],
                'status' => $preparedProductOfferData['status']
            ]);
        }
        $response = response()->json( [ 'success' => true, 'data' => $data, 'message' => 'Oferta atualizada com sucesso' ] );
        return $response;
    }

    public function getById( Request $request, $id )
    {
        $response = null;

        try{
            $productOffer = Product\Offer::query()->where( 'hash', $id )->firstOrFail();
            $response = response()->json( $productOffer->toArray() );
        }catch ( \Exception $e ){

        }

        return $response;
    }
    public function dataGrid( Request $request )
    {
        $columns = $request->post('columns' );
        $pd = $request->post('pagination' );
        $fd = $request->post( 'filter' );
        $product = Product::query()->select(['id'])->where('token', $fd['value' ] )->first();
        $fd['value'] = (string) $product->id;
        $fd['field']['value'] = 'id_product';

        $dt = new DataTable( new Product\Offer );
        $totalRows = $dt->setColumns( $columns )->hasJoins( $columns )->setFilter( $fd )->getTotalRows();
        $dt->setPagination( $pd );

        return response()->json( [ 'total' => $totalRows, 'data' => $dt->getData() ] );
    }
}
