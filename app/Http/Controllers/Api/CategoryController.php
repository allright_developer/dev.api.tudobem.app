<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function get( Request $request, $status = 1 )
    {
        $categories = Category::query()
            ->where( 'status', $status )
            ->select('id as value', 'id_category', 'label', 'description' )
            ->get()
            ->mapWithKeys(function( $category, $key ){
                return [ $category->value => $category ];
            });
        return response()->json( [ 'success' => true, 'options' => $categories ] );
    }

    public function getHierarchically( Request $req )
    {
        $response = null;
        $response = Category::query()
            ->select('id', 'label', 'description' )
            ->where('status', 1 )
            ->whereNull('id_category' )
            ->get()
            ->map( function( $category ) {
                $children = Category::query()
                    ->select( 'id', 'label', 'description' )
                    ->where('id_category', $category->id )
                    ->get()
                    ->all();

                $category->children = $children;
                return $category;
            });

        return response()->json( $response );
    }
}
