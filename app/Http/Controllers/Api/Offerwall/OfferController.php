<?php

namespace App\Http\Controllers\Api\Offerwall;

use App\Http\Controllers\Controller;
use App\Http\Requests\OfferRequest;
use App\Model\AdPublisher;
use App\Model\Lead\LeadDelivery;
use App\Repository\AdVersion;
use ExceptionLog\ExceptionLog;
use App\Model\Offerwall\{Offer, OfferAction, OfferDesign, OfferPayout, OfferTarget};
use App\Service\TableGrid\DataTable;
use Illuminate\Http\Request;
use function Illuminate\Support\Facades\Log;

class OfferController extends Controller
{
    public function get( Request $request, $id )
    {
        $response = null;

        try{
            $offer = Offer::query()->findOrFail( $id )->toArray();
            $leadDelivery = LeadDelivery::getByIdOffer( $id );
            $offerAction  = OfferAction::getByIdOffer( $id, $leadDelivery );
            $adVersions   = AdVersion::getAllByIdOffer( $id );
            $payout       = OfferPayout
                ::where( 'id_offer', $id )
                ->where('status', 1 )
                ->first()->toArray();

            $response = [
                'name' => $offer['name'],
                'description' => $offer['description'],
                'begin_at' => $offer['begin_at'],
                'finish_at' => $offer['finish_at'],
                'action' => [
                    'conf' => [
                        'delivery' => $leadDelivery,
                        'fields'   => isset( $offerAction['conf']['fields'] ) ? $offerAction['conf']['fields'] : [],
                        'text'     => isset( $offerAction['conf']['text'] ) ? $offerAction['conf']['text'] : [ 'header' => null, 'thankyou' => null ],
                        'url'      => isset( $offerAction['conf']['url'] ) ? $offerAction['conf']['url'] : null
                    ],
                    'type' => (string) $offerAction[ 'type' ]
                ],
                'ad_version' => $adVersions,
                'id_ad_publisher' => [
                    'value' => $offer[ 'id_ad_publisher' ],
                    'label' => $offer['id_ad_publisher'] === null ? 'Sem Publisher' : AdPublisher::query()->select('label' )->where('id', $offer['id_ad_publisher'] )->first()->label ],
                'payout' => $payout,
                'target' => OfferTarget::getByIdOffer( $id )
            ];

            $response = response()->json( $response );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function post( OfferRequest $request )
    {
        $response = null;
        $data = $request->post();
//        dd( $data );

        try{
            $entity = new Offer();
            $preparedData = $entity->prepareData( $data );

            $isLessThan = $entity->isFinishLessThanBegin( $preparedData );

            if( $isLessThan  )
                return response()->json( [ 'success' => false, 'errors' => ['dates' => [ 'A data de Finalização precisa ser maior que a Data de Início' ] ]  ], 422 );

            $result = $entity->fill( $preparedData )->save();
            if( $result ){
                $idOffer = $entity->id;
//                Payout
                $entity = new OfferPayout();
                $preparedData = $entity->prepareData( $data[ 'payout' ], $idOffer );
                $entity->fill( $preparedData )->save();
//                Design
                ( new OfferDesign )->fill( [ 'id_offer' => $idOffer ] )->save();
//                Ad Versions (Copies and Assets)
                AdVersion::save( $data['ad_version'], $idOffer );
//                Action
                $entity = new OfferAction;
                $entity->fill( $entity->prepareData( $data[ 'action' ], $idOffer ) )->save();
//                Delivery
                if( $data[ 'action' ]['type' ] === '2' ){
                    $entity = new LeadDelivery;
                    $preparedData = $entity->prepareData( $data['action']['conf'], $idOffer );
                    $entity->fill( $preparedData )->save();
                }
//                Target and Running Periods
                $entity = new OfferTarget;
                $preparedData = $entity->prepareData( $data['target'], $idOffer );
                $entity->fill( $preparedData )->save();
            }
            $response = response()->json( [ 'message' => '' ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ], 421 );
        }

        return $response;
    }

    public function update( Request $request )
    {
        $response = null;
        $data = $request->post();

        try{
            $entity = Offer::query()->findOrFail( $data['id'] );
            $preparedData = $entity->prepareData( $data );

            $isLessThan = $entity->isFinishLessThanBegin( $preparedData );

            if( $isLessThan )
                return response()->json( [ 'success' => false, 'errors' => ['dates' => [ 'A data de Finalização precisa ser maior que a Data de Início' ] ]  ], 422 );

            $result = $entity->update( $preparedData );
            if( $result ){
                $idOffer = $entity->id;
//                Payout
                ( new OfferPayout )->persistPayout( $data[ 'payout' ], $idOffer );
//                @todo implement offer design options
//                ( new OfferDesign )->fill( [ 'id_offer' => $idOffer ] )->save();

//                Ad Versions (Copies and Assets)
                AdVersion::update( $data['ad_version'], $idOffer );
//                Action
                $entity = OfferAction::query()->where('id_offer', $idOffer )->firstOrFail();
                $entity->update( $entity->prepareData( $data[ 'action' ], $idOffer ) );
//                Delivery
                if( $data[ 'action' ]['type' ] === '2' ){
                    $entity = LeadDelivery::query()
                        ->where('id_entity', $idOffer )
                        ->where('entity', 'offer' )
                        ->firstOrFail( ['id'] );
                    $preparedData = $entity->prepareData( $data['action']['conf'], $idOffer );
                    $entity->update( $preparedData );
                }
//                Target and Running Periods
                $entity = OfferTarget::query()->where('id_offer', $idOffer )->firstOrFail();
                $preparedData = $entity->prepareData( $data[ 'target' ], $idOffer );
                $entity->update( $preparedData );
            }
            $response = response()->json( [ 'message' => 'Configurações da Oferta atualizadas!' ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ], 421 );
        }
        return $response;
    }

    public function getOrdered( Request $request )
    {
        $response = null;

        try{
            $offers = Offer::query()
                ->where('status', 1 )
                ->limit(25  )
                ->orderBy('order', 'asc' )->get(['id', 'name', 'description'])->toArray();
            $response = [ 'offers' => $offers ];
        }catch ( \Exception $e ){
            ExceptionLog::persist( $e, ExceptionLog\LevelMapper::LEVEL_ERROR_DATABASE );
        }

        return response()->json( $response );
    }

    public function postOrdered( Request $request )
    {
        $response = null;
        $orderedList = $request->post('list' );
        try{
            if( is_array( $orderedList ) ){
                foreach( $orderedList as $offer ){
                    Offer::query()->where('id', $offer['id'] )->update([ 'order' => $offer['order'] ] );
                }
            }
            $response = [ 'ok' => 'true' ];
        }catch ( \Exception $e ){
            ExceptionLog::persist( $e, ExceptionLog\LevelMapper::LEVEL_ERROR_DATABASE );
            $response = ['ok' => false, 'message' => $e->getMessage() ];
        }

        return response()->json( $response );
    }

    public function toggleStatus( Request $request )
    {
        $response = null;
        $data = $request->post();

        try{
            if( $data['id'] ){
               Offer::query()->where('id', $data['id'] )->update([ 'status' => $data['status' ] ] );
            }
            $response = [ 'ok' => 'true' ];
        }catch ( \Exception $e ){
            ExceptionLog::persist( $e, ExceptionLog\LevelMapper::LEVEL_ERROR_DATABASE );
            $response = ['ok' => false, 'message' => $e->getMessage() ];
        }

        return response()->json( $response );
    }

    public function dataGrid( Request $request )
    {
        $columns = $request->post('columns' );
        $pd = $request->post('pagination' );
        $fd = $request->post( 'filter');

        $dt = new DataTable( new Offer );
        $totalRows = $dt->setColumns( $columns )->hasJoins( $columns )->setFilter( $fd )->getTotalRows();

        $dt->setPagination( $pd );

        return response()->json( [ 'total' => $totalRows, 'data' => $dt->getData() ] );
    }
}
