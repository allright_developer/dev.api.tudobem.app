<?php

namespace App\Http\Controllers\Api\Offerwall;

use App\Http\Controllers\Controller;
use App\Model\Offerwall\OfferImpression;
use App\Model\Offerwall\OfferwallImpression;
use App\Security\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OfferwallStatController extends Controller
{
    public function impression( Request $request )
    {
        $response = null;

        try{
            $data = $request->post();
            $offerwallHash = $data[ 'hash' ];
            $dataModel = [
                'id_copy' => $data['id_copy'],
                'id_user' => Auth::user()->id,
                'ad_version' => $data['ad_version'],
                "impression_{$data['event']}_at" => time(),
                'hash' => Token::generate( 8 )
            ];
            $model = OfferImpression::updateOrCreate(
                [ 'id_offer' => $data['id_offer'], 'offerwall' => $offerwallHash ],
                $dataModel
            );
            $response = response()->json( [ 'data' => $model, 'response' => 'offerwall stat' ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function click( Request $request )
    {
        $response = null;

        try{
            $data = $request->post();
            $offerwallHash = $data[ 'hash' ];
            $dataModel = [
                'id_copy' => $data['id_copy'],
                'id_user' => Auth::user()->id,
                'ad_version' => $data['ad_version'],
                'clkid' => Token::generate( 8 )
            ];
            $model = OfferImpression::updateOrCreate(
                [ 'id_offer' => $data['id_offer'], 'offerwall' => $offerwallHash ],
                $dataModel
            );
            $response = response()->json( [ 'data' => $model, 'response' => 'offerwall stat' ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function offerwallImpressionFinish( Request $request )
    {
        $response = null;

        try{
            $model = OfferwallImpression::query()
            ->where('hash', $request->post( 'hash' ) )
            ->update(
                [ 'impression_end_at' => time() ]
            );
            $response = response()->json( [ $model ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }
}
