<?php

namespace App\Http\Controllers\Api\Offerwall;

use App\Http\Controllers\Controller;
use App\Model\AdPublisher;
use Illuminate\Http\Request;

class PublisherController extends Controller
{
    public function get( Request $request, $status = 1 )
    {
        $options = AdPublisher::query()
            ->where( 'status', $status )
            ->select('id as value', 'label', 'description' )
            ->get()
            ->mapWithKeys(function( $option, $key ){
                return [ $option->value => $option ];
            });
        return response()->json( [ 'success' => true, 'options' => $options ] );
    }
}
