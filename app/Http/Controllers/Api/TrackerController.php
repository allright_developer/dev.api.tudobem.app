<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Mapper\ClickMapper;
use App\Model\Offerwall\OfferPayout;
use App\Model\User;
use App\Model\UserCredit;
use App\Model\UserLog;
use App\Repository\Tracker\Click;
use App\Repository\Tracker\Conversion;
use ExceptionLog\ExceptionLog;
use ExceptionLog\LevelMapper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TrackerController extends Controller
{
    public function click( Request $request )
    {
        $response = null;
        $fields   = $request->post();
        try{
            $data = Click::persist( $fields, Auth::user() );
            if( $data )
                $response = response()->json( [ 'hash' => $data[ 'token' ] ] );
        } catch( \Exception $e ){
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
            ExceptionLog::persist( $e, LevelMapper::LEVEL_ERROR_APPLICATION, $fields );
        }
        return $response;
    }

    public function conversion( Request $request )
    {
        $clickToken = $request->get('tdbtoken', null );
        try{
            $click = Click::getByToken( $clickToken );
            // a valid click is required
            if( $click ){
                switch ( $click->entity ){
                    case ClickMapper::ENTITY_OFFER :
                        // 1. get user
                        $user = User::query()
                            ->where([
                                [ 'id', '=', $click->id_user ],
                                [ 'status','>', 0 ]
                            ])->firstOrFail(['id', 'token','email', 'status' ] );
                        // 2. get entity bonus and payout info
                        $payoutCols = ['payout_type', 'payout_user_crypto','payout_ong_crypto'];
                        $payout = OfferPayout::query()->where( 'id_offer', $click->id_entity )->firstOrFail( $payoutCols );
                        // 3. persist conversion data (click_token, id_user, entity, id_entity, params )
                        $data = [ 'click_token' => $clickToken, 'id_user' => $user->id, 'entity' => $click->entity, 'id_entity' => $click->id_entity ];
                        $data[ 'params' ] = [
                            'ow_hash' => $click->params->ow_hash, 'type' => $payout->payout_type
                        ];
                        $res = Conversion::persist( $data );
                        // 4. attribute credits to user
                        if( $res ){
                            $userCredit = UserCredit::query()->where('id_user', $user->id );
                            $userCredit->increment('crypto', $payout->payout_user_crypto );
                            $userCredit->increment('crypto_org', $payout->payout_ong_crypto );
                            // setting click to converted/processed
                            \App\Model\Tracker\Click::changeStatus( $clickToken, 1 );
                            // logging user conversion
                            UserLog::create([
                                'id_user' => $user->id, 'action_type' => \App\Mapper\UserLog::CONVERSION_OFFER['id'],
                                'description' => \App\Mapper\UserLog::CONVERSION_OFFER['label'], 'data' => [ 'offer' => $click->id_entity ], 'notifiable' => 1
                            ]);

                            $response = response()->json( [ 'ok' => true ] );
                        }
                        else
                            throw new \Exception( 'Token not found' );

                        break;
                }
            }
            else
                throw new \Exception( 'Error on user processing' );

        } catch( \Exception $e ){
            $response = response()->json( [ 'ok' => false ], 421 );
            ExceptionLog::persist( $e, LevelMapper::LEVEL_ERROR_APPLICATION, ['click_token' => $clickToken ] );
        }

        return $response;
    }
}
