<?php
namespace App\Http\Controllers\Api\Service;

use App\Http\Controllers\Controller;
use App\Model\User\ServiceSubscription;
use App\Service\Google\Fcm\WebPush;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PushController extends Controller
{
    public function subscribe( Request $request, $token, $service = 'fcm' )
    {
        $response = null;
        try {
            $topic = $request->post('topic', 'institutional' );
            $pushService = new WebPush();

            if( $pushService->subscribe( $token, $topic ) ){
                $idUser = Auth::check() ? Auth::user()->id : null;
                $subscription = (new ServiceSubscription)->fill([ 'data' => [ 'token' => $token, 'topics' => [ $topic ] ], 'id_user' => $idUser ] )->save();
            }

            $response = response()->json([ 'ok' => true ]);
        }
        catch ( \Exception $e ){
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }
}
