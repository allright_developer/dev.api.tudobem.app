<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Ong;
use App\Model\User\Data;
use App\Service\TableGrid\DataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OngController extends Controller
{
    public function post( Request $request )
    {
        $data = $request->post();
        $response = null;
        try {
            if( $data['id'] )
                return $this->update( $data );
            // saving the new survey
            $entity = new Ong;
            $preparedProductData = $entity->prepareData( $data );
            $entity->fill( $preparedProductData )->save();

            $response = response()->json( [ 'success' => true, 'data' => [ 'id' => $entity->id ] ] );

        }catch ( \Exception $e ){
            $response = response()->json([ 'success' => false, 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 500 );
        }

        return $response;
    }

    protected function update( array $data )
    {
        $response = null;
        try{
            $entity = Ong::find( $data['id'] );
            $entity->update( $entity->prepareData( $data ) );
            $response = response()->json( [ 'success' => true, 'data' => $data, 'message' => 'Informações da ONG atualizadas' ] );
        }
        catch ( \Exception $e ){
            $response = response()->json([ 'success' => false, 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function get( Request $request, $id = null )
    {
        $response = null;
        try{
            if( is_numeric( $id ) )
                $ong = Ong::find( $id );
            else
                $ong = Ong::query()->where( 'status', 1 )
                    ->limit( 10 )
                    ->get( [ 'id', 'label', 'description', 'images' ] );

            $response = response()->json( $ong );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function favorite( Request $request, $id = null )
    {
        sleep( 2);
        $response = null;
        try{
            if( $id ){
                $userData = Data::where('id_user', Auth::user()->id )->first(['data', 'id']);
                $data = $userData->data;
                $data['ong'] = $id;
                $response = $userData->update([ 'data' => $data ]);
            }
            $response = response()->json( [ 'ok' => $response ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function dataGrid( Request $request )
    {
        $columns = $request->post('columns' );
        $pd = $request->post('pagination' );
        $fd = $request->post( 'filter');

        $dt = new DataTable( new Ong );
        $totalRows = $dt->setColumns( $columns )->hasJoins( $columns )->setFilter( $fd )->getTotalRows();

        $dt->setPagination( $pd );

        return response()->json( [ 'total' => $totalRows, 'data' => $dt->getData() ] );
    }
}
