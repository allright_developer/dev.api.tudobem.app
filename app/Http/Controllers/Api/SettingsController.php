<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingsRequest;
use App\Model\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function post( SettingsRequest $request )
    {
        $response = null;
        try {

            $response = Settings::add( $request->post() );
            $response = response()->json( $response );

        }catch ( \Exception $e ){
            $response = response()->json([ 'success' => false, 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 500 );
        }

        return $response;
    }

    public function get( Request $request )
    {
        $response = null;
        try {
            $settings = Settings::query()->where('status', 1 )->firstOrFail(['conf']);
            $response = response()->json( $settings );
        }catch ( \Exception $e ){
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function getPublic(Request $request)
    {
        $response = null;
        try {
            $settings = Settings::where('status', 1 )->firstOrFail(['conf']);
            $response = response()->json( $settings['conf']['bonus'] );
        }catch ( \Exception $e ){
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }
}
