<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Lead\Lead;
use App\Model\Offerwall as ow;
use App\Security\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OfferwallController extends Controller
{
    public function get( Request $request )
    {
        $response = null;
        try{
            $user   = Auth::user() ?? null; // User::query()->find(1) is just for tests
            $params = $request->query();
            $offers = ow\Offer::simpleGet( $user, $params );
            $validatedOffers = [ 'offers' => [] ];
            if( $offers ){
                $offersToStat = [ 'offers' => [] ];
                foreach ( $offers as $offer ){

                    if( $user && ( isset( $offer->geo ) && isset( ow\Offer::$userData->geo ) ) ){
                        $geo = json_decode( $offer->geo );
                        $coords = [
                            'origin_lat' => $geo->lat, 'origin_lon' => $geo->lon,
                            'destination_lat' => ow\Offer::$userData->geo->lat, 'destination_lon' => ow\Offer::$userData->geo->lon,
                            'radius' => $geo->radius
                        ];
                        $geoValidated = ow\Offer::validateGeo( $coords );
                        if( $geoValidated ){
                            $validatedOffers[ 'offers' ][] = $offer;
                            $offersToStat[ 'offers' ][]    = $offer->id;
                        }
                    }
                    else{
                        $validatedOffers[ 'offers' ][] = $offer;
                        $offersToStat[ 'offers' ][]    = $offer->id;
                    }
                }

                if( $user )
                    $offersToStat[ 'id_user' ] = $user->id;
                $validatedOffers[ 'ow_hash' ]  = ow\OfferwallImpression::add( $offersToStat );
            }

            $response = response()->json( [ 'data' => $validatedOffers ] );
        } catch ( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function getById( Request $request, $id )
    {
        $response = null;
        try {
            $offer = ow\Offer::simpleGet( null, [ 'id' => $id ] );
            if( count( $offer ) )
                $response = response()->json( [ 'offer' => $offer[0] ] );
        }
        catch ( \Exception $e ){
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }
    /**
     * Offer Lead data post processing
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function post( Request $request )
    {
        $response = null;
        $data = $request->post();

        try{
            $idUser = null;
            $user = Auth::user();
            if( $user )
                $idUser = $user->id;

            $lead = Lead::create([
                'token' => Token::generate( 8 ),
                'id_user' => $idUser,
                'generated_by' => 'offer',
                'id_entity' => $data[ 'id_offer' ],
                'data' => $data,
            ]);

            if( $lead )
                $response = true;

            $response = response()->json( [ 'response' => $response ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }
}
