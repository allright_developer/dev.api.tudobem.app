<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddCreditRequest;
use App\Model\UserCredit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CreditController extends Controller
{
    public function post( AddCreditRequest $req )
    {
        $units = $req->post();
        $response = null;
        try {
            $user = Auth::user();
            $userCredits = UserCredit::getUserCredits( $user->id );

            foreach ( $units as $unit => $amount ){
                $userCredits->increment( $unit, $amount );
            }
            $response = response()->json( [ 'success' => true, 'data' => [ 'crypto' => $userCredits->crypto, 'fiat' => $userCredits->fiat ], 'messages' => null ] );
        }catch (\Exception $e ){
            $response = response()->json( [ 'success' => false, 'data' => null, 'messages' => ['error'] ], 501 );
        }

        return $response;
    }
}
