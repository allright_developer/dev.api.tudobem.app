<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\User\Data;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserDataController extends Controller
{
    public function get( Request $request )
    {
        $response = null;

        try{
            $userId = Auth::user()->id;
            $response = Data::getByIdUser( $userId )->data;
        } catch ( \Exception $e ){
            return response()->json([ 'message' => $e->getMessage(), 'line' => $e->getLine(), 'file' => $e->getFile() ], 421 );
        }

        return response()->json( $response );
    }

    public function update( Request $request )
    {
        $response = null;
        $post = $request->post();
        try{
            $userId = Auth::user()->id;
            $userData = Data::query()->where( 'id_user', $userId )->firstOrFail( [ 'data', 'id' ] );
            $data = $userData->data;
            $data[ 'marital_status' ] = $post[ 'marital_status' ];
            $data[ 'economic' ]  = $post[ 'economic' ];
            $data[ 'interests' ] = $post[ 'interests' ];
            $data[ 'children' ]  = $post[ 'children' ];
            dd( $data );
        } catch ( \Exception $e ){
            return response()->json([ 'message' => $e->getMessage(), 'line' => $e->getLine(), 'file' => $e->getFile() ], 421 );
        }

        return response()->json( $response );
    }

}
