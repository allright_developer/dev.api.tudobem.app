<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    public function get( Request $request, $id = null )
    {
        $data = Bank::query()
            ->where( 'status', 1 )
            ->select('id as value', 'label' )
            ->get()
            ->mapWithKeys(function($bank, $key ){
                return [ $key => $bank ];
            });

        return response()->json( [ 'success' => true, 'options' => $data ] );
    }
}
