<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\RegisterAuthRequest;
use App\Model\User;
use App\Model\UserCredit;
use App\Model\UserLog;
use App\Security\Token;
use App\Service\TableGrid\DataTable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    public $loginAfterSignup = true;

    public function update( Request $request )
    {
        $response = null;
        try{
            $user = Auth()->user();
            $data = $request->post();
            $user->update( [ 'gender' => $data[ 'gender' ] ] );
            unset( $data['gender'] );

            $userData = User\Data::where( 'id_user', $user->id )->firstOrFail( [ 'data', 'id' ] );
            $newData = [
                'cep' => $data['cep'], 'cpf' => $data[ 'cpf' ], 'dob' => $data['dob'], 'geo' => $data['geo'],
                'address' => $data[ 'address' ], 'economic' => $data['economic'], 'interests' => $data['interests'],
                'marital_status' => $data['marital_status'], 'children' => $data['children']
            ];
            $data = array_merge( $userData->data ?? [], $newData );
//            dd( 'new Data', $data );
            $userData->update( [ 'data' => $data ] );

            $response = response()->json( [ 'message' => 'ok' ] );

        } catch (\Exception $e ){
            $response = response()->json([
                'success' => false,
                'data' => ['message' => $e->getMessage().' : '.$e->getFile().' : '.$e->getLine() ]
            ], 421);
        }

        return $response;
    }

    public function register( RegisterAuthRequest $request )
    {
        $response = null;
        try{
            $passwd = Str::random(8);
            $request->request->set( 'password', $passwd );
//            dd( $request->request );
            $user = User::firstOrCreate([
                'email' => $request->post('email' ),
            ],[
                'token'  => Token::generate(),
                'password'  => bcrypt( $passwd ),
                'name'  => $request->post('name' ),
            ]);

            if( $this->loginAfterSignup ) {
                $response = $this->login( $request );
            }
            else
                $response = response()->json([
                    'success' => true,
                    'data' => $user
                ], 200);

        } catch (\Exception $e ){
            $response = response()->json([
                'success' => false,
                'data' => ['message' => $e->getMessage().' : '.$e->getFile().' : '.$e->getLine(), 'type' => 'error_register' ]
            ], 401);
        }

        return $response;
    }

    public function login( Request $request )
    {
        $credentials = $request->request->all();
        $response = null;

        if ( $token = $this->guard()->attempt( $credentials ) ) {
            $response = response()->json( ['success' => true, 'data' => [ 'token' => $token, 'level' => 0 ] ], 200)
                            ->header('Authorization', $token );
            UserLog::create([
                'id_user' => Auth::user()->id, 'action_type' => \App\Mapper\UserLog::USER_LOGIN['id']
            ]);
        }
        else{
            $user = User::where( 'email', $request->post( 'email' ) )
                ->where('status', '>', 0 )
                ->first();
            $responseData = [ 'data' => [ 'error' => 'login_error'], 'success' => false, 'messages' => null ];
            if( $user )
                $responseData = [ 'data' => [ 'error' => 'phone_not_related'], 'success' => false, 'messages' => null ];

//            $user = User::where( 'phone', $request->post( 'email' ) )->first();
            $response = response()->json( $responseData, 401);
        }

        return $response;
    }

    public function logout( Request $request )
    {
        try {
            $this->guard()->logout();
            return response()->json([
                'status' => 'success',
                'msg' => 'Logout efetuado com sucesso.'
            ], 200);

        } catch ( JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Pedimos perdão, mas o usuário não pode ser desconectado.'
            ], 401);
        }
    }

    /**
     * Get authenticated user
     */
    public function user(Request $request)
    {
        try{
            $user = User::find( Auth::user()->id );
            $response = response()->json([
                'success' => true,
                'data' => [ 'user' => $user, 'token' => JWTAuth::fromUser( $user )]
            ]);
        }catch ( TokenExpiredException $e ){
            $response = response()->json(['success' => false ], 401 );
        }catch ( TokenInvalidException $e ){
            $response = response()->json(['success' => false ], 401 );
        }catch ( JWTException $e ){
            $response = response()->json(['success' => false ], 401 );
        }

        return $response;
    }

    /**
     * Refresh JWT token
     */
    public function refresh()
    {
        $response = null;
        $token = $this->guard()->refresh();
        if ( $token ){
            $user = Auth::user();
            $credits = UserCredit::getUserCredits( $user->id )->only( 'crypto', 'fiat' );
            $user = [ 'name' => $user->name, 'email' => $user->email ];
            $response = response()
                ->json([
                    'success' =>  true,
                    'data' => [ 'token' => $token, 'user' => $user, 'credits' => $credits ]
                ], 200);
        }
        else
            $response = response()->json( [ 'messages' => [ 'Não foi possível renovar o Login' ], 'success' => false ], 401 );

        return $response;
    }

    /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard();
    }

    /**
     * @example to replicate with other Entities
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataGrid( Request $request )
    {
        $columns = $request->post('columns' );
        $pd = $request->post('pagination' );
        $fd = $request->post( 'filter');

        $dt = new DataTable( new User );
        $totalRows = $dt->setColumns( $columns )->hasJoins( $columns )->setFilter( $fd )->getTotalRows();

        $dt->setPagination( $pd );

        return response()->json( [ 'total' => $totalRows, 'data' => $dt->getData() ] );
    }
}
