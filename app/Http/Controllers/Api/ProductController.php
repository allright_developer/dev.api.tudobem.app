<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Model\Product;
use App\Service\TableGrid\DataTable;
use ExceptionLog\ExceptionLog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function post( ProductRequest $request )
    {
        $data = $request->post();
        $response = null;
        try {
            if( $data['id'] )
                return $this->update( $data );
            // saving the new survey
            $entity = new Product;
            $preparedProductData = $entity->prepareData( $data );
            $entity->fill( $preparedProductData )->save();

            (new Product\Image )->addImages( $data['images'], $entity->id );
            $response = response()->json( [ 'success' => true, 'data' => [ 'id' => $entity->id ] ] );

        }catch ( \Exception $e ){
            $response = response()->json([ 'success' => false, 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 500 );
        }

        return $response;
    }

    protected function update( array $data )
    {
        $response = null;

        $entity = Product::find( $data['id'] );
        $entity->update( $entity->prepareData( $data ) );

        if( $data[ 'images' ] )
            foreach ( $data['images'] as $image ){

                switch ( $image['status'] ){
                    case 1:
                        Product\Image::query()->where('name', $image['name'] )->update( [ 'description' => $image['description' ] ] );
                        break;
                    case 2:
                        $productModel = new Product\Image;
                        $images = $productModel->getImagesByProductId( $entity->id );
                        foreach( $images as $img )
                            $productModel->removeImage( $img );

                        $productModel->addImage( $image, $data['id'] );
                        break;
                    case 0:
                        (new Product\Image )->removeImage( $image );
                        break;
                }
            }

        $response = response()->json( [ 'success' => true, 'data' => $data, 'message' => 'Pesquisa atualizada com sucesso' ] );
        return $response;
    }

    public function dataGrid( Request $request )
    {
        $columns = $request->post('columns' );
        $pd = $request->post('pagination' );
        $fd = $request->post( 'filter');

        $dt = new DataTable( new Product );
        $totalRows = $dt->setColumns( $columns )->hasJoins( $columns )->setFilter( $fd )->getTotalRows();

        $dt->setPagination( $pd );

        return response()->json( [ 'total' => $totalRows, 'data' => $dt->getData() ] );
    }

    public function get( Request $request, $slug = null )
    {
        $response = null;
        try{
            $filters = $request->input();
            if( Auth::check() )
                $filters[ 'id_user' ] = Auth::user()->id;

            $products = Product::get( $filters );
            $response = response()->json( $products );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function getBySlug( Request $request, $slug = null )
    {
        $response = null;
        try{
            $filters = $request->input();
            if( Auth::check() )
                $filters[ 'id_user' ] = Auth::user()->id;

            $product = Product::where('slug', $slug )->firstOrFail();
            $response = response()->json( $product );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    /**
     * @param Request $request
     * @param string $id The Product token id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getById( Request $request, string $id ): JsonResponse
    {
        $response = null;
        try{
            $entity = Product::query()->where('token', $id )->firstOrFail()->toArray();
            $entity[ 'images' ] = (new Product\Image)->getImagesByProductId( $entity['id'] );

            $response = response()->json( [ 'data' => $entity ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function favorite( Request $request )
    {
        $response = null;
        $data = $request->post();
        try{
            $user = Auth::user();

            if( $user ){
                $entity = Product\Favorited::query()->firstOrCreate([
                    'id_product' => $data['id'], 'id_user' => $user->id
                ]);
                $entity->where([ 'id_user' => $user->id ] )->where([ 'id_product' => $data['id']])
                ->update(['favorited' => $data['favorited'] ] );
            }
            $response = response()->json( [ 'ok' => true ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function exchange( Request $request )
    {
        $response = null;
        $idProduct = $request->post( 'id_product', null );
        try{
            $user = Auth::user();

            if( $user )
                $response = Product\UserExchange::persist( $idProduct, $user->id );

            $response = response()->json( [ 'ok' => true, 'crypto' => $response ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'ok' => false, 'data' => [
                'message' => 'Pedimos Perdão, houve um erro e a solicitação de troca não foi efetuada',
                'error_message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ]
            ], 421 );
        }

        return $response;
    }

    public function toggleStatus( Request $request )
    {
        $response = null;
        $data = $request->post();

        try{
            if( $data['id'] ){
                Product::query()->where('id', $data['id'] )->update([ 'status' => $data['status' ] ] );
            }
            $response = [ 'ok' => 'true' ];
        }catch ( \Exception $e ){
            ExceptionLog::persist( $e, ExceptionLog\LevelMapper::LEVEL_ERROR_DATABASE );
            $response = ['ok' => false, 'message' => $e->getMessage() ];
        }

        return response()->json( $response );
    }
}
