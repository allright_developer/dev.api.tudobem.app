<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\SurveyRequest;
use App\Model\Option;
use App\Model\Question;
use App\Model\Survey;
use App\Model\UserSurvey;
use App\Service\TableGrid\DataTable;
use ExceptionLog\ExceptionLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class SurveyController extends Controller
{
    /**
     * @todo
     * 1. Retrieve a Survey
     *   1.1 Not responded by the user OK
     *   1.2 Related with the user profile
     * @param Request $request
     * @param string $slug a survey slug name
     * @return Response|null
     */
    public function get( Request $request, $slug = null )
    {
        $response = null;
        $user = Auth::user();
        try{
            if( $user->email_verified_at !== null ){
                // it gets a survey that was not answered yet by an user
                $survey = (new Survey)->getAllowedSurveyByUserAndSlugCriteria( $user, $slug );
            }
            else{
                $response = response()->json( [ 'data' => [ 'message' => 'User not verified' ] ] );
            }

            $response = response()->json( [ 'data' => $survey !== null ? $survey->compiled : null ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 421 );
        }

        return $response;
    }

    public function getById( Request $request, $id )
    {
        $response = null;
        try{
            $survey = Survey::find( $id );
            $response = response()->json( [ 'data' => $survey->buildJson() ] );
        } catch( \Exception $e ) {
            $response = response()->json( [ 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'survey' => $survey ] ], 421 );
        }

        return $response;
    }

    public function post( SurveyRequest $request )
    {
        $data = $request->post();
        $response = null;

        try {
            if( $data['id'] )
                return $this->update( $data );
            // saving the new survey
            $survey = new Survey;

            $survey->fill( $survey->prepareData( $data ) )->save();
            // iterating over the questions
            foreach ( $data['questions'] as $order => $question ){
                $questionObj = new Question;

                $questionObj->fill( Question::prepareData( $question, $survey->id, $order) )->save();

                if( in_array( $question['type'], ['checkbox', 'radio'] ) ){
                    foreach( $question[ 'answers' ] as $optionOrder => $option ){
                        $optionData = [
                            'id_question' => $questionObj->id,
                            'order'       => $optionOrder,
                            'label'       => $option['label'],
                            'status'      => 1
                        ];
                        Option::create( $optionData );
                    }
                }
            }
            $survey->update([ 'compiled' => $survey->buildJson() ] );

            $response = response()->json( [ 'success' => true, 'data' => [ 'data' => $survey ] ] );

        }catch ( \Exception $e ){
            $response = response()->json([ 'success' => false, 'data' => [ 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ], 500 );
        }

        return $response;
    }

    protected function update( array $data )
    {
        $response = null;
//        dd( $data );
        // updating survey
        $survey = Survey::find( $data['id'] );

        $survey->update( $survey->prepareData( $data ) );
        // iterating over the questions
        foreach ( $data['questions'] as $order => $question ){
            $preparedData = Question::prepareData( $question, $survey->id, $order );
            if( $question['id'] ){
                $questionObj = Question::find( $question['id'] );
                $questionObj->update( $preparedData );
            }
            else{
                $questionObj = new Question();
                $questionObj->fill( $preparedData )->save();
            }

            if( in_array( $question['type'], ['checkbox', 'radio'] ) ){
                foreach( $question[ 'answers' ] as $optionOrder => $option ){
                    $status = $option[ 'status' ] === 2 ? 1 : $option[ 'status' ];
                    $optionData = [
                        'id_question' => $questionObj->id,
                        'order'       => $optionOrder,
                        'label'       => $option['label'],
                        'status'       => $status
                    ];
                    // saves a new option, whatever it is deleted
                    if( is_numeric( $option['val'] ) ){
                        Option::query()->find( $option['val'] )->update( $optionData );
                    }
                    else{
                        ( new Option )->fill( $optionData )->save();
                    }
                }
            }
        }

        $survey->update( ['compiled' => $survey->buildJson() ] );

        $response = response()->json( [ 'success' => true, 'data' => null, 'message' => 'Pesquisa atualizada com sucesso' ] );
        return $response;
    }

    public function toggleStatus( Request $request )
    {
        $response = null;
        $data = $request->post();

        try{
            if( $data['id'] ){
                Survey::query()->where('id', $data['id'] )->update([ 'status' => $data['status' ] ] );
            }
            $response = [ 'ok' => 'true' ];
        }catch ( \Exception $e ){
            ExceptionLog::persist( $e, ExceptionLog\LevelMapper::LEVEL_ERROR_DATABASE );
            $response = ['ok' => false, 'message' => $e->getMessage() ];
        }

        return response()->json( $response );
    }
    /**
     * @example to replicate with other Entities
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataGrid( Request $request )
    {
        $columns = $request->post('columns' );
        $pd = $request->post('pagination' );
        $fd = $request->post( 'filter');

        $dt = new DataTable( new Survey );
        $totalRows = $dt->setColumns( $columns )->hasJoins( $columns )->setFilter( $fd )->getTotalRows();

        $dt->setPagination( $pd );

        return response()->json( [ 'total' => $totalRows, 'data' => $dt->getData() ] );
    }
}
