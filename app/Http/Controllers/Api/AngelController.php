<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\User\Angel;
use App\Model\User\AngelGift;

class AngelController extends Controller
{
    public function get()
    {
        try {
            $response = response()->json( [ 'angels' =>  Angel::getList(), 'gifts' => AngelGift::getGifts() ] );
        }
        catch ( \Exception $e ){
            $response = response()->json([ 'msg' => 'Error' ], 421 );
        }

        return $response;
    }
}
