<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Answer;
use App\Model\Question;
use App\Model\Survey;
use App\Model\UserCredit;
use App\Model\UserLog;
use App\Model\UserSurvey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{
    public function post( Request $request )
    {
        $data = $request->post();
        $response = null;
        try{
            $user = Auth::user();
            $survey = Survey::find( $data['survey']['id'] );
            $alreadyAnswered = UserSurvey::where([
                ['id_user', '=', $user->id ], [ 'id_survey','=', $survey->id ]
            ])->first();

            if( $alreadyAnswered === null ){

                $userCredits = UserCredit::getUserCredits( $user->id );
                $credits = $survey->credits ?: [ 'user' => 0, 'org' => 0 ];

                if( $data['survey']['questions'] ){
                    foreach( $data['survey']['questions'] as $question ){
                        $questionModel = Question::find( $question['id'], [ 'type', 'credits', 'credits_beneficent' ] );
//                        dump( $questionModel );
                        if( !is_array( $question['val'] ) ){
                            Answer::firstOrCreate([
                                'id_question' => $question['id'], 'id_user' => $user->id, 'val' => $question['val']
                            ]);
                            $credits[ 'user' ] += $questionModel->credits;
                            $credits[ 'org' ]  += $questionModel->credits_beneficent;
                        }
                        else{
                            foreach( $question[ 'val' ] as $val ){
                                Answer::firstOrCreate([
                                    'id_question' => $question['id'], 'id_user' => $user->id, 'val' => $val
                                ]);
                                $credits[ 'user' ] += $questionModel->credits;
                                $credits[ 'org' ]  += $questionModel->credits_beneficent;
                            }
                        }
                    }
                }
                UserLog::create([
                    'id_user' => $user->id, 'action_type' => \App\Mapper\UserLog::SURVEY_COMPLETED['id'],
                    'description' => 'Ganhou '.$credits['user'].' Credibens e '.$credits['org'].' Credibens foram reservados para Doação por concluir a Pesquisa '.$survey->name,
                    'data' => [ 'id_survey' => $survey->id, 'credits' => $credits ]
                ]);

                UserSurvey::create([
                    'id_user' => $user->id, 'id_survey' => $survey->id
                ]);

                $userCredits->increment( 'crypto', $credits['user'] );
                $userCredits->increment( 'crypto_org', $credits['org'] );
                $response = response()->json( [ 'data' => ['credits' => [ 'crypto' => $userCredits->crypto,'crypto_org' => $userCredits->crypto_org,  'fiat' => null ] ] ] );
            }
            else{
                $response = response()->json([ 'answered' => $alreadyAnswered ] );
            }
        } catch ( \Exception $e ){
            $response = response()->json([ 'error' => ['message' => $e->getMessage(), 'line' => $e->getLine(), 'file' => $e->getFile() ] ], 421 );
        }
        return $response;
    }
}
