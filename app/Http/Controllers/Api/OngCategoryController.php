<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\OngCategory;
use Illuminate\Http\Request;

class OngCategoryController extends Controller
{
    public function get()
    {
        $categories = OngCategory::query()
            ->where( 'status', 1 )
            ->select('id as value', 'label', 'description' )
            ->get()
            ->mapWithKeys(function( $category, $key ){
                return [ $category->value => $category ];
            });
        return response()->json( [ 'success' => true, 'options' => $categories ] );
    }
}
