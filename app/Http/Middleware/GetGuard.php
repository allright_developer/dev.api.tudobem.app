<?php
namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class GetGuard extends Middleware
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure  $next
     * @param string $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null )
    {
        if( $guard !== null )
            auth()->shouldUse($guard);

        return $next($request);
    }

    // Override authentication method
    protected function authenticate( $request, array $guards )
    {
        $response = false;
        if (empty($guards)) {
            $guards = [null];
        }
        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                $response = $this->auth->shouldUse( $guard );
            }
        }

        return $response;
    }
}
