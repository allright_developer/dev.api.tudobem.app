<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if ( !$request->expectsJson() ) {
            return route('login');
        }
    }

    public function handle( $request, \Closure $next, ...$guards )
    {
        try {
            if( $this->authenticate($request, $guards) === false ) {
                return response()->json([ 'success' => false, 'data' => [ 'message' => 'Unauthenticated' ] ],401);
            }
        } catch (\Exception $e) {
            if( $e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException ){
                return response()->json( [ 'success' => false, 'data' => [ 'message' => 'Invalid Token' ] ] , 401);
            }else if( $e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException ){
                return response()->json( [ 'success' => false, 'data' => [ 'message' => 'Token Expired' ] ], 403 );
            }else{
                return response()->json( [ 'success' => false, 'data' => [ 'message' => 'Token not found' ] ], 403 );
            }
        }
        return $next($request);
    }

    // Override authentication method
    protected function authenticate( $request, array $guards )
    {
        $response = false;
        if (empty($guards)) {
            $guards = [null];
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                $response = $this->auth->shouldUse( $guard );
            }
        }

        return $response;
    }
}
