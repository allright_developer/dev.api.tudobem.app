<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class OfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_token' => 'required',
            'label' => 'required',
            'description' => 'required',
            'crypto' => 'required|numeric',
            'fiat'      => 'required|numeric',
            'begin_at'  => 'required|date_format:d/m/Y H:i',
            'finish_at' => 'required|date_format:d/m/Y H:i',
        ];
    }

    public function messages()
    {
        return [
            'id_product.required'  => 'Por favor informe o ID do Produto',
            'label.required'       => 'Por favor, informe o nome da Oferta',
            'description.required' => 'Por favor, informe uma Descrição',
            'crypto.required'      => 'Por favor, informe um Valor em Credibens',
            'crypto.numeric'       => 'Digite um Valor em Credibens Numérico',
            'begin_at.required'    => 'Informe a Data de Inicio',
            'begin_at.date_format'    => 'O formato da data de início está incorreto',
            'finish_at.date_format'   => 'O formato da data de finalização está incorreto',
            'finish_at.required'   => 'Informe a Data de Finalização',
            'fiat.required'        => 'Por favor, informe um valor em Reais',
            'fiat.numeric'         => 'Por favor, informe um valor Monetário'
        ];
    }
}
