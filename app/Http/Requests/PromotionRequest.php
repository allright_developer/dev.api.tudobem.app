<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_category' => 'required',
            'name' => 'required',
            'url' => 'required|url',
            'rules' => 'required|url',
            'description' => 'required',
            'finish_at' => 'required|date_format:d/m/Y H:i',
            'begin_at' => 'required|date_format:d/m/Y H:i'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Por favor, informe o nome da Promoção',
            'label.required' => 'Por favor, informe Um Breve Descritivo da Promoção',
            'description.required' => 'Por favor, informe a Descrição da Promoção',
            'url.required' => 'Por favor, informe a URL da Promoção',
            'url.url' => 'Por favor, informe uma URL válida para a Promoção',
            'id_category.required' => 'Por favor, informe a Categoria',
            'finish_at.required' => 'Por favor, Escolha uma Data de Finalização',
            'begin_at.required' => 'Por favor, Escolha uma Data de Inicio',
            'finish_at.date_format' => 'A data informada em :attribute não está no formato Correto',
            'begin_at.date_format' => 'A data informada em :attribute não está no formato Correto',
            'rules.required' => 'Por favor, Informe a URL do Regulamento',
            'rules.url' => 'Por favor, Informe uma URL válida para o Regulamento',
        ];
    }

    public function attributes()
    {
        return [
            'finish_at' => 'Data de Finalização da Pesquisa',
            'begin_at' => 'Data de Início da Pesquisa',
            'name' => 'Nome da Promoção',
            'rules' => 'URL do Regulamento',
            'category' => 'Categoria',
            'url' => 'Site da Promoção',
        ];
    }
}
