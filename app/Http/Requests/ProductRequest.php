<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'required',
            'id_category' => 'required',
            'description' => 'required|min:50',
            'crypto' => 'required|integer',
            'crypto_cashback_user' => 'required|integer',
            'crypto_cashback_org' => 'required|integer',
            'fiat' => 'required|numeric',
            'images' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'label.required' => 'Por favor digite o Nome do Produto',
            'label.unique' => 'Existe outro produto com o mesmo nome, escolha um nome Único para o Produto',
            'id_category.required' => 'Por favor informe uma Categoria',
            'description.required' => 'Por favor faça uma Breve Descrição do Produto',
            'description.min' => 'Por favor faça uma Breve Descrição do Produto com no mínimo :min caracteres',
            'images.required' => 'Por favor, adicione pelo menos 1 Imagem do Produto',
            'crypto.required' => 'Por favor, informe o valor do produto em Credibens',
            'crypto.integer' => 'O valor do Produto em Credibens deve ser Numérico',
            'crypto_cashback_user.required' => 'Por favor, informe o valor de Cashback ao Usuário em Credibens',
            'crypto_cashback_user.integer' => 'O valor em Credibens para o Usuário deve ser Numérico',
            'crypto_cashback_org.required' => 'Por favor, informe o valor de Cashback para Doação em Credibens',
            'crypto_cashback_org.integer' => 'O valor em Credibens para Doação deve ser Numérico',
            'fiat.required' => 'O valor em Moeda Corrente do Produto é Obrigatório',
            'fiat.numeric' => 'O valor em Moeda Corrente do Produto deve ser Numérico',
        ];
    }
}
