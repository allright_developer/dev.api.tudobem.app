<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_category' => 'required',
            'name' => 'required',
            'description' => 'required',
            'finish_at' => 'required|date_format:d/m/Y H:i',
            'begin_at' => 'required|date_format:d/m/Y H:i',
            'bonify' => 'required|boolean',
            'credits' => 'required_if:bonify,true',
            'questions' => 'required',
        ];
    }

    public function messages()
    {


        return [
            'name.required' => 'Por favor, informe o nome da Campanha',
            'description.required' => 'Por favor, informe a Descrição da Campanha',
            'id_category.required' => 'Por favor, informe a Categoria',
            'finish_at.required' => 'Por favor, Escolha uma Data de Finalização',
            'begin_at.required' => 'Por favor, Escolha uma Data de Inicio',
            'finish_at.date_format' => 'A data informada em :attribute não está no formato Correto',
            'begin_at.date_format' => 'A data informada em :attribute não está no formato Correto',
            'credits.required_if' => 'Por favor, Informe as Bonificações',
            'questions.not_empty' => 'Por favor, Crie Perguntas para a Pesquisa',
            'questions.required' => 'Por favor, Crie Perguntas para a Pesquisa',
        ];
    }

    public function attributes()
    {
        return [
            'finish_at' => 'Data de Finalização da Pesquisa',
            'begin_at' => 'Data de Início da Pesquisa',
            'name' => 'Nome da Campanha',
            'category' => 'Categoria',
            'credits' => 'Bonificações',
            'questions' => 'Perguntas',
        ];
    }
}
