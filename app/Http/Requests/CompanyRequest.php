<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_category' => 'required',
            'label' => 'required|unique:company,label',
            'description' => 'required|min:40'
        ];
    }

    public function messages()
    {
        return [
            'id_category.required' => 'Por favor, informe a Categoria',
            'label.required' => 'Por favor, informe o Nome da Empresa',
            'label.unique' => 'Por favor, informe outro Nome para a Empresa, este já está sendo usado',
            'description.min' => 'Por favor, informe um descritivo com pelo menos :min caracteres',
        ];
    }
}
