<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|min:5',
            'description' => 'required|min:10',
            'begin_at'    => 'required|date_format:d/m/Y H:i',
            'finish_at'   => 'required|date_format:d/m/Y H:i',
            'payout'      => 'required',
            'action'      => 'required',
            'ad_version'  => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'       => 'Por favor, escolha o nome da Oferta',
            'description.required'=> 'Detalhe a Oferta na Descrição',
            'begin_at.required'   => 'Informe a data de Início da Veiculação da Oferta',
            'finish_at.required'  => 'Informe a data de Finalização da Oferta',
            'payout.required'     => 'Por favor, informe os detalhes do payout',
            'action.required'     => 'Por favor, defina o tipo de Ação da Oferta',
            'ad_version.required' => 'Crie pelo menos um versão de anúncio',

            'name.min'              => 'Por favor, digite um nome válido',
            'description.min'       => 'Por favor, descreva a Oferta com mais detalhes',
            'begin_at.date_format'  => 'A data de Início está incorreta',
            'finish_at.date_format' => 'A data de Finalização está incorreta'
        ];
    }
}
