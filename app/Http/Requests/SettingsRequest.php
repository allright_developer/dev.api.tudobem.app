<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'bonus' => 'required|array',
            'taxes' => 'required|numeric|max:30'
        ];
    }
    /**
     * Get the validation error message.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'bonus.required' => 'Por favor, informe os créditos de bonus',
            'taxes.required' => 'Por favor, informe a carga de impostos',
            'taxes.numeric' => 'A taxa de impostos precisa ser um valor numérico'
        ];
    }
}
