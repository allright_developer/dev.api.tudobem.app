<?php

namespace App\Http\Requests;

use App\Rules\EmailAuthentic;
use App\Rules\PhoneUnique;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|string',
            'email' => /*'required|email|unique:users',*/[ 'required', 'email', 'unique:users', new EmailAuthentic ],
            'phone' => [ 'required', new PhoneUnique ]
        ];
    }

    public function messages()
    {
        return [
            'name.required'  => 'req',
            'name.string'  => 'inv',
            'email' => 'inv',
            'email.unique' => 'use',
            'phone.required' => 'req'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errorMessages = [ 'success' => false, 'messages' => $validator->getMessageBag()->getMessages() ];
        throw new HttpResponseException( response()->json( $errorMessages, 409 ) );
    }
}
