<?php
namespace App\Api\Marketing\Publisher\Afilio;


use GuzzleHttp\Client;

class Campaign
{
    public static $endpoint = 'http://v2.afilio.com.br/api/prog_api.php?token=access_token&affid=aff_id';

    public static $client = null;

    public static function get()
    {
        $endpoint = str_replace( [ 'access_token', 'aff_id' ], config('publishers.afilio' ), self::$endpoint );
        $client = new Client();
        $file = json_decode( $client->get( $endpoint )->getBody()->getContents(), true );

        dd( $file );
    }
}
