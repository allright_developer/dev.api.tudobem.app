<?php
namespace App\Api\Marketing\Publisher\Lomadee;

class Client
{
    public const ENDPOINTS = [
        'sandbox'    => 'http://sandbox-api.lomadee.com/',
        'production' => 'https://api.lomadee.com/'
    ];

    public static $client = null;


    public static function getClient()
    {
        if( !self::$client ){
            self::$client = new \GuzzleHttp\Client( [ 'base_uri' => self::ENDPOINTS[ config('publishers.lomadee.mode' ) ].'/' ] );
        }
        return self::$client;
    }

    /**
     * @param string $uri
     * @param string $mode
     * @return \stdClass|null
     * @todo implement logging
     */
    public static function makeGetRequest( $uri, $params = [] )
    {
        $response = null;
        $params[ 'sourceId' ] = config( 'publishers.lomadee.sourceID' );

        $uri = str_replace( 'app-token', config('publishers.lomadee.access_token' ), $uri );

        $response = ( self::getClient() )
            ->get($uri.'?'.\http_build_query( $params ) )
            ->getBody()
            ->getContents();

        $response = json_decode( $response );

        return $response;
    }
}
