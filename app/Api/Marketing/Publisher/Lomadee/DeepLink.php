<?php
namespace App\Api\Marketing\Publisher\Lomadee;

class DeepLink
{
    public static $apiVersion = 'v2/';

    /**
     * @param array $params Params to be tracked <br>
     * <b>url</b>: Required. The product url to be tracked, get it in the lomadee offers
     * <b>domain</b>: Required. The tracking domain, options: compre.vc (it is https), acesse.vc, oferta.vc
     * <b>mdasc</b>: Optional. Sub id to use to customize the tracking, @see https://developer.lomadee.com/afiliados/relatorios/recursos/identifique-suas-vendas/
     * @return \stdClass|null
     */
    public static function create( array $params = [] )
    {
        $response = Client::makeGetRequest( self::$apiVersion."app-token/deeplink/_create", $params );
        return $response;
    }
}
