<?php
namespace App\Api\Marketing\Publisher\Lomadee;

class Coupon
{
    public static $apiVersion = 'v2/';

    public static function all()
    {
        $response = Client::makeGetRequest( self::$apiVersion.'app-token/coupon/_all' );
        return $response;
    }

    public static function getById( $idCoupon )
    {
        $response = Client::makeGetRequest( self::$apiVersion."app-token/coupon/_id/{$idCoupon}" );
        return $response;
    }

    public static function get( array $params = [] )
    {
        $response = Client::makeGetRequest( self::$apiVersion."app-token/coupon/_all", $params );
        return $response;
    }

    public static function getCategories()
    {
        $response = Client::makeGetRequest( self::$apiVersion.'app-token/coupon/_categories' );
        return $response;
    }

    public static function getStoresWithCoupons( array $params = [] )
    {
        $response = Client::makeGetRequest( self::$apiVersion.'app-token/coupon/_stores', $params );
        return $response;
    }

}
