<?php
namespace App\Api\Marketing\Publisher\Lomadee;

class Offer
{
    public static $apiVersion = 'v3/';

    public static function getById( $id )
    {
        $response = Client::makeGetRequest( self::$apiVersion."app-token/offer/_id/{$id}" );
        return $response;
    }

    public static function getByCategory( $idCategory )
    {
        $response = Client::makeGetRequest( self::$apiVersion."app-token/offer/_category/{$idCategory}" );
        return $response;
    }

    public static function getByStore( $idStore )
    {
        $response = Client::makeGetRequest( self::$apiVersion."app-token/offer/_store/{$idStore}" );
        return $response;
    }

    public static function search( $searchStatemet )
    {
        $response = Client::makeGetRequest( self::$apiVersion."app-token/offer/_search", [ 'keyword' => $searchStatemet ] );
        return $response;
    }

}
