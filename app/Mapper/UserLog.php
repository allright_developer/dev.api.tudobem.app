<?php
namespace App\Mapper;


class UserLog
{
    const USER_SIGNUP = [ 'id' => 1, 'label' => 'Novo Usuario'];

    const USER_LOGIN  = [ 'id' => 2, 'label' => 'Usuario Entrou'];

    const SURVEY_COMPLETED = [ 'id' => 10, 'label' => 'Pesquisa Respondida'];

    const SHARE_PRODUCT = [ 'id' => 11, 'label' => 'Produto Compartilhado'];

    const SHARE_PROMOTION = [ 'id' => 12, 'label' => 'Promoção Compartilhada'];

    const SHARE_OFFER = [ 'id' => 13, 'label' => 'Oferta do Carrossel Compartilhada' ];

    const MEMBER_BY_MEMBER = [ 'id' => 14, 'label' => 'Novo Membro inscrito usando Token' ];

    const CONVERSION_OFFER = [ 'id'  => 15, 'label' => 'Conversão de Campanha' ];

    const CONVERSION_PRODUCT = [ 'id'  => 16, 'label' => 'Conversão de Produto' ];
}
