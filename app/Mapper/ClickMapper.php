<?php
namespace App\Mapper;

class ClickMapper
{
    const ENTITY_OFFER     = 1;

    const ENTITY_PRODUCT   = 2;

    const ENTITY_PROMOTION = 3;

    const ENTITIES = [
        'offer' => 1, 'product' => 2, 'promotion' => 3
    ];
}
