<?php
namespace App\Mapper;

class OfferAction
{
    public const subTypes = [
        'select' => 'CustomSelect', 'text' => 'CustomText', 'check' => 'CustomCheck', 'radio' => 'CustomRadio'
    ];

    public const predefinedTypes = [
        'name' => 'Name', 'email' => 'Email', 'cpf' => 'CPF', 'cep' => 'CEP', 'gender' => 'Gender'
    ];
    public static function formatName( string $name ): string
    {
        return trim( strtolower( $name ) );
    }
}
