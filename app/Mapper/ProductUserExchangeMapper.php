<?php
namespace App\Mapper;

class ProductUserExchangeMapper
{
    public const STATUS_CANCELED = 0;

    public const STATUS_DELIVERED = 1;

    public const STATUS_WAITING   = 2;

    public const STATUS_ONGOING   = 3;
}
