<?php
namespace App\Mapper;

class OfferAsset
{
    const FS_AWS    = [ 'id' => 1, 'label' => 's3',    'path' => 'img/offer/' ];
    const FS_LOCAL  = [ 'id' => 2, 'label' => 'local', 'path' => 'img/offer/' ];
    const FS_GCP    = [ 'id' => 3, 'label' => 'gcp',   'path' => 'img/offer/' ];

    const TYPE_IMG    = [ 'id' => 1, 'label' => 'Imagem' ];
    const TYPE_VIDEO  = [ 'id' => 2, 'label' => 'Video' ];
    const TYPE_LOTTIE = [ 'id' => 3, 'label' => 'Lottie File' ];
}
