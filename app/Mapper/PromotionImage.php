<?php
namespace App\Mapper;

class PromotionImage
{
    const FS_AWS = [ 'id' => 1, 'label' => 's3', 'path' => 'img/promotion/' ];
    const FS_LOCAL  = [ 'id' => 2, 'label' => 'local', 'path' => 'img/promotion/'];
    const FS_GCP = [ 'id' => 3, 'label' => 'gcp', 'path' => 'img/promotion/' ];
}
