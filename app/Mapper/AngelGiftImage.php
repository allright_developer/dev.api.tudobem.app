<?php
namespace App\Mapper;

class AngelGiftImage
{
    const FS_AWS = [ 'id' => 1, 'label' => 's3', 'path' => 'img/angel-gift/' ];

    const FS_LOCAL  = [ 'id' => 2, 'label' => 'local', 'path' => 'img/angel-gift/' ];

    const FS_GCP = [ 'id' => 3, 'label' => 'gcp', 'path' => 'img/angel-gift/' ];
}
