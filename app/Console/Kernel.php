<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule( Schedule $schedule )
    {
        $schedule->command('product:offer' )->everyFiveMinutes()->runInBackground();
        $schedule->command('angel:count' )->twiceDaily(0,12);
        $schedule->command('offerwall:offer' )->dailyAt('0:00:00');
        $schedule->command('promotion:status' )->dailyAt('0:00:00');
//        $schedule->command('faker:feed' )->everyMinute();
        $schedule->command('stats:period' )->everyFifteenMinutes();
        $schedule->command('stats:period PT30M' )->everyThirtyMinutes();
        $schedule->command('stats:period PT1H' )->hourly();
        $schedule->command('stats:period PT12H' )->twiceDaily(0,12);
        $schedule->command('stats:period P1D' )->daily();
        $schedule->command('stats:period P7D' )->weekly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
