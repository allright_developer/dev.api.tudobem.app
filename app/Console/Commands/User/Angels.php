<?php

namespace App\Console\Commands\User;

use App\Model\User\Angel;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Angels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'angels:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sum angels crypto earned for donation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $format = 'Y-m-d H:i';
        $mainDate = now();
        $currentMonth = \date( 'm' );
        $currentYear = \date( 'Y' );
//        while( $mainDate < now() ){

            $now = $mainDate->format( $format );
            $past = $mainDate->subMinutes(5 )->format( $format );

            $sql = "SELECT distinct( id_user ) `user`, sum(data->'$.credits.org') count_org, sum(data->'$.credits.user') count_user FROM user_log
                    WHERE created_at BETWEEN :past AND :now GROUP BY `user`";
            $result = DB::select( $sql, [ 'past' => $past, 'now' => $now ] );

            if( $result )
                foreach( $result as $user ){
                    $angel = Angel::query()->firstOrCreate(
                        [ 'id_user' => $user->user , 'month' => $currentMonth , 'year' => $currentYear ]
                    );

                    $angel->increment( 'crypto_org', $user->count_org );
                    $angel->increment( 'crypto_user', $user->count_user );
                }
//        }
    }
}
