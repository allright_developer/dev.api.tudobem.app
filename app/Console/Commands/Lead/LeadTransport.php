<?php

namespace App\Console\Commands\Lead;

use App\Model\Lead\Lead;
use App\Model\Lead\LeadDelivery;
use ExceptionLog\ExceptionLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LeadTransport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:transport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delivers lead to the destination and manage the lead queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $deliverySettings = LeadDelivery::getAll();

        try {
            if( $deliverySettings )
                foreach ( $deliverySettings as $deliverySetting ){
                    $howManyToSend = LeadDelivery::howManyToSend( $deliverySetting );
                    if( $howManyToSend > 0 ){
                        $conf = json_decode( $deliverySetting->conf );
                        // get the proper transport according to the transport type and settings
                        $transporter = \App\Repository\LeadTransport::getStrategy( $deliverySetting->transport, $conf->transport_settings );
                        // get the entity-related Leads with the defined limit
                        $leads = Lead::getToSend( [ 'id_entity' => $deliverySetting ->id_entity, 'entity' => $deliverySetting->entity, 'limit' => $howManyToSend ] );
                        // execute the main method to send the lead
                        $transporter->doTransport( $leads, $conf->data->mapping );
                    }
                }
        }
        catch ( \Exception $e ){
            ExceptionLog::persist( $e, \ExceptionLog\LevelMapper::LEVEL_ERROR_APPLICATION, [ 'command' => 'lead:transport' ] );
        }
    }
}
