<?php

namespace App\Console\Commands\OfferPublishers\Rakuten;

use GuzzleHttp\Client;
use Illuminate\Console\Command;

class Advertiser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rakuten:advertisers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get RakutenCoupon Subscribed Advertisers';

    protected $client = null;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client =  new Client([ 'base_uri' => 'https://api.rakutenmarketing.com/advertisersearch/1.0' ] );
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = $this->client->get(null, [ 'headers' => [ 'Authorization' => 'Bearer 6ebf1acea429fee9fa7aeb1761a9d1a', 'Accept' => 'application/xml' ] ] );
        dd( $response->getBody()->getContents() );
    }
}
