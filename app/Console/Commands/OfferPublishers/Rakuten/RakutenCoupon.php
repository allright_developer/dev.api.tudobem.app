<?php
namespace App\Console\Commands\OfferPublishers\Rakuten;

use GuzzleHttp\Client;
use Illuminate\Console\Command;

class RakutenCoupon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rakuten:coupon {network=8}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'RakutenCoupon brazilian coupons api';

    protected $client = null;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client =  new Client( [ 'base_uri' => 'https://api.rakutenmarketing.com/coupon/1.0' ] );
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = $this->client->get('?network='.$this->argument('network' ), [ 'headers' => [ 'Authorization' => 'Bearer 86d1f3154beb7c8b37ec4e227f17d998', 'Accept' => 'application/xml' ] ] );
    }
}
