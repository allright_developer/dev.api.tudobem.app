<?php
namespace App\Console\Commands\OfferPublishers\Rakuten;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Whoops\Util\SystemFacade;

class Token extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rakuten:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a new Rakuten Access Token';

    protected $client = null;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client =  new Client([ 'base_uri' => 'https://api.rakutenmarketing.com/' ] );
    }

    /**
     * get the access token to be used in all other requests.
     *
     * @return mixed
     */
    public function handle()
    {
        $token = null;
        try{
            $cfg = config('publishers.rakuten.VenddiRakuten' );
            $response = $this->client->post('token', [
                'form_params' => $cfg['form_params'],
                'headers' => [
                    'Authorization' => 'Basic '.$cfg[ 'request_token' ]
                ]
            ] );

            if( $response->getStatusCode() === 200 )
                $token = json_decode( $response->getBody()->getContents() )->access_token;

        }catch ( \Exception $e ){
            /** @todo implement logging */
        }

        $this->info( $token );
    }
}
