<?php
namespace App\Console\Commands\Promotion;

use App\Model\Promotion;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Status extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promotion:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manages the promotion status watching the begin_at and finish_at properties';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $table =  ( new Promotion )->getTable();
//        deactivate the expired offers
        $sql = "UPDATE $table SET status = 0 WHERE finish_at <= NOW() AND status > 0";
        DB::update( $sql );
//        activate the new offers by begin_at and waiting status
        $sql = "UPDATE $table SET status = 1 WHERE begin_at <= NOW() AND status = 2";
        return DB::update( $sql );
    }
}
