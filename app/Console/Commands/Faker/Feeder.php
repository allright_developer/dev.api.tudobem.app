<?php

namespace App\Console\Commands\Faker;

use App\Formatter\UserInput;
use App\Model\User;
use App\Model\UserCredit;
use App\Model\UserLog;
use App\Security\Token;
use Faker\Generator;
use Illuminate\Console\Command;

class Feeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faker:feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Feed tables routinely to test the system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $newUsersCount = rand( 1 ,7 );
        $faker = \Faker\Factory::create();
        $faker->addProvider( new \Faker\Provider\pt_BR\Person( $faker ) );
        $faker->addProvider( new \Faker\Provider\pt_BR\PhoneNumber( $faker ) );
        $faker->addProvider( new \Faker\Provider\pt_BR\Address( $faker ) );

        for( $i = 0; $i <= $newUsersCount; $i++ ){
            $this->getFakeUser( $faker );
        }

        for( $i = 0; $i <= 5; $i++ ){
            $this->getFakeCredits();
        }
    }

    public function getFakeUser( Generator $faker )
    {
        $phone = $faker->phoneNumber;
        $email = $faker->email;
        $name = $faker->name;

        $passwd = UserInput::phone( $phone );
        $user = User::query()->firstOrCreate([
            'email' => $email,
        ],[
            'token'   => Token::generate( 8 ),
            'name'    => $name,
            'gender'  => rand(1,2),
            'ddd'     => $faker->numberBetween( 11, 99 ),
            'phone'   => $faker->numberBetween( 920002000, 999999999 ),
            'password' => bcrypt( $passwd ),
        ]);

        return $user;
    }

    public function getFakeCredits()
    {
        $user = User::query()->inRandomOrder()->first();
        $userCredits = UserCredit::getUserCredits( $user->id );
        $org = rand( 5, 30 );
        $usr = rand( 5, 30 );

        $userCredits->increment( 'crypto', $usr );
        $userCredits->increment( 'crypto_org', $org );

        UserLog::create([
            'id_user' => $user->id, 'action_type' => \App\Mapper\UserLog::SURVEY_COMPLETED['id'],
            'description' => 'Ganhou '.$usr.' Credibens e '.$org.' Credibens foram reservados para Doação por '.\App\Mapper\UserLog::SURVEY_COMPLETED['label'],
            'data' => [ 'credits' => [ 'org' => $org, 'user' => $usr ] ]
        ]);
    }
}
