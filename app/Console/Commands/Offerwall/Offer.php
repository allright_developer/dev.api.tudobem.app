<?php

namespace App\Console\Commands\Offerwall;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Offer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offerwall:offer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Offer maintenance command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $table = (new \App\Model\Offerwall\Offer)->getTable();
        $sql = "UPDATE $table SET status = 0 where finish_at <= NOW() and status > 0";
        DB::update( $sql );
        $sql = "UPDATE $table SET status = 1 where begin_at <= NOW() and status = 2";
        DB::update( $sql );

    }
}
