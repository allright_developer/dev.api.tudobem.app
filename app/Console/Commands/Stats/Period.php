<?php

namespace App\Console\Commands\Stats;

use App\Model\Stat\Users;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Period extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:period {period=PT15M} {entity=users}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gather stats about the app activity';

    protected $dateFormat = 'Y-m-d H:i';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function handle()
    {
        $period = $this->argument( 'period' );
        $entity = $this->argument( 'entity' );

        $startAt = now()->sub( new \DateInterval( $period ) )->setSecond(0);
        $endAt   = now()->setSecond(0);

        if( $period === 'PT15M' ) {
            $sql = "SELECT COUNT(id) total FROM $entity WHERE created_at BETWEEN :start_at AND :end_at";
            $total = DB::select( $sql, [ 'start_at' => $startAt->format( $this->dateFormat ), 'end_at' => $endAt->format( $this->dateFormat ) ] )[0]->total;
        }
        else {
            $sql = "SELECT SUM(total) total FROM stat_user WHERE start_at >= :start_at AND period = 'PT15M'";
            $total = DB::select( $sql, [ 'start_at' => $startAt->format( $this->dateFormat ) ] )[0]->total;
        }

        Users::query()->create([
            'start_at' => $startAt, 'end_at' => $endAt, 'period' => $period, 'total' => $total
        ]);
        return 1;
    }
}
