<?php

namespace App\Console\Commands\Product;

use App\Model\Product;
use ExceptionLog\ExceptionLog;
use ExceptionLog\Model\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Offer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:offer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update offer begin and finish date and status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        $table =  (new Product\Offer)->getTable();
//        deactivate the expired offers
        $sql = "UPDATE $table SET status = 0 WHERE finish_at <= NOW() AND status > 0";
        DB::update( $sql );
//        activate the new offers by begin_at and waiting status
        $sql = "UPDATE $table SET status = 1 WHERE begin_at <= NOW() AND status = 2";
        DB::update( $sql );
    }
}
