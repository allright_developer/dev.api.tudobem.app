<?php
namespace App\Security;

class Token
{
    public static function generate( $length = 19 )
    {
        return \str_replace('-','', substr( \Illuminate\Support\Str::uuid(),0,$length ));
    }
}
