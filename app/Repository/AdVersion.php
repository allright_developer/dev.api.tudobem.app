<?php
namespace App\Repository;

use App\Mapper\OfferAsset;
use Illuminate\Support\Facades\DB;
use App\Model\Offerwall\{ OfferCopy };
use App\Security\Token;
use Illuminate\Support\Facades\Storage;

class AdVersion
{
    /**
     * @param array $adVersions
     * @param int $idOffer
     */
    public static function save(  array $adVersions, int $idOffer )
    {
        foreach ( $adVersions as $version => $adVersion ){
//            persisting copy
            $copy = new OfferCopy;
            $copy->fill( [ 'text' => $adVersion['copy'], 'id_offer' => $idOffer, 'ad_version' => $version ] )->save();
//            saving image assets
            if( isset( $adVersion['assets']['images'] ) ){
                foreach( $adVersion['assets']['images'] as $image ){
                    self::addImage( $image, $idOffer, $version );
                }
            }
        }
    }

    public static function update( array $adVersions, int $idOffer )
    {
        foreach ( $adVersions as $version => $adVersion ){
//            persisting copy
            $copy = OfferCopy::query()
                ->where([ [ 'id_offer','=', $idOffer ], [ 'ad_version','=', $adVersion[ 'ad_version' ] ] ] )->first();
            $copy->update( [ 'text' => $adVersion['copy'], 'id_offer' => $idOffer, 'ad_version' => $adVersion[ 'ad_version' ] ] );
//            saving image assets
            if( isset( $adVersion['assets']['images'] ) ){
                foreach( $adVersion['assets']['images'] as $image ){
                    if( $image['status'] === 0 ){
                        self::deleteImage( $image, $idOffer );
                    }
                    elseif( $image['status'] === 2 )
                        self::addImage( $image, $idOffer, $adVersion['ad_version'] );
                    else
                        self::updateImage( $image, $idOffer );
                }
            }
        }
    }

    public static function updateImage( array $image, $idOffer )
    {
        return \App\Model\Offerwall\OfferAsset::query()
            ->where([
                [ 'name', '=', $image['name'] ], [ 'id_offer', '=', $idOffer ]
            ])
            ->update( [ 'description' => $image['description'] ] );
    }

    /**
     * Soft System and hard remote image removing
     * @param array $image array of image data
     * @param int $idOffer
     * @return int returns 1 if the image was deleted from the storage and flagged with status 1
     */
    public static function deleteImage( array $image, int $idOffer ): int
    {
        $result = \App\Model\Offerwall\OfferAsset::query()
            ->where([
                [ 'name', '=', $image['name'] ],
                [ 'id_offer', '=', $idOffer ]
            ])
            ->update( [ 'status' => 0 ] );
        if( $result )
            Storage::disk( 's3' )->delete( $image[ 'name' ] );

        return $result;
    }

    public static function addImage( array $data, $idOffer, $adVersion = 0 )
    {
        $newImageEntity = null;
        $imgFullPath = public_path( OfferAsset::FS_AWS[ 'path' ] );
        $toFile = $imgFullPath;
        $cropData = $data[ 'canvas' ];

        $tmpImage = \Intervention\Image\ImageManagerStatic::make( $data['data'] );
        $name = Token::generate(9).'.'.explode('/', $tmpImage->mime() )[1];
        $fullName = OfferAsset::FS_AWS[ 'path' ].$name;
        $toFile .= $name;

        $resultImg = $tmpImage->crop( $cropData['width'], $cropData['height'], $cropData['left'] < 0 ? 0 : $cropData['left'], $cropData['top'] < 0 ? 0 : $cropData['top'])
            ->resize( 450, null, function( $constraint ){
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save( $toFile );

        $s3 = Storage::disk( 's3' );
        $fileResult = $s3->put( $fullName, $resultImg->getEncoded(), [ 'ACL' => 'public-read' ] );

        if( $resultImg && $fileResult ){
            unlink( $toFile );

            $imageData = [
                'id_offer'    => $idOffer,
                'name'        => $fullName,
                'url'         => $s3->url( $fullName ),
                'ad_version'  => $adVersion,
                'description' => $data[ 'description' ],
                'storage'     => OfferAsset::FS_AWS[ 'id' ],
                'type'        => OfferAsset::TYPE_IMG[ 'id' ],
                'status'      => 1
            ];

            $newImageEntity = \App\Model\Offerwall\OfferAsset::create( $imageData );
        }

        return $newImageEntity;
    }

    /**
     * @param int $idOffer
     * @return array
     */
    public static function getAllByIdOffer( $idOffer ): array
    {
        $sql = "SELECT
                    oa.name, oa.description, oa.url, oa.ad_version, oa.status, oa.ad_version,
                    oc.text
                FROM offer_asset oa
                INNER JOIN offer_copy oc ON oc.id_offer = oa.id_offer AND oc.ad_version = oa.ad_version AND oc.status = 1
                WHERE oa.id_offer = :id_offer AND oa.status = 1
                ORDER BY oa.ad_version";

        $adVersions = DB::select( $sql, ['id_offer' => $idOffer ] );

        if( $adVersions )
            foreach ( $adVersions as $adVersion ){
                $adVersions[ $adVersion->ad_version ] = [
                    'ad_version' => $adVersion->ad_version,
                    'copy' => json_decode( $adVersion->text, true ),
                    'assets' => [
                        'images' => [
                            [
                                'ad_version' => $adVersion->ad_version,
                                'name' => $adVersion->name,
                                'description' => $adVersion->description,
                                'data' => $adVersion->url,
                                'canvas' => [ 'top' => 0, 'left' => 0, 'width' => 100, 'height' => 50 ],
                                'status' => $adVersion->status
                            ]
                        ]
                    ]
                ];
            }

        return $adVersions;
    }
}
