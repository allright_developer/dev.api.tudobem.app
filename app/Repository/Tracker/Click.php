<?php
namespace App\Repository\Tracker;

use App\Mapper\ClickMapper;
use App\Model\User;
use App\Repository\Exceptions\InvalidClickException;
use App\Security\Token;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Click
{
    public static $tbl = 'click';
    public static $ttl = 604800; // 7 days of TTL
    /**
     * Persist a click data event to be used by other processes
     * @param array $data click data
     * @param Authenticatable $user
     * @return array|bool return the new data with click unique token
     * @throws \Exception
     */
    public static function persist( array $data, Authenticatable $user )
    {
        $tbl = self::$tbl;
        $data[ 'token' ] = Token::generate(8);
        $data[ 'entity' ] = $data['ety'] ?? 'offer';
        $data[ 'id_user' ] = $user->id;

        if( !isset( $data['id_ety'] ) )
            throw new \Exception( 'entity id not specified' );

        if( isset( $data['params'] ) ) {
            $data['params'] = json_encode( $data['params'] );
        }
        else
            $data['params'] = [ 'id_user' => $user->id ];

        $result = DB::insert(
                "INSERT INTO $tbl(token, entity, id_entity, id_user, params ) VALUES ( :token, :entity, :id_entity, :id_user, :params )",
                [
                    'token' => $data['token' ], 'entity' => ClickMapper::ENTITIES[ $data['entity'] ],
                    'id_entity' => $data['id_ety'], 'id_user' => $data['id_user'], 'params' => $data['params']
                ]
            );

        if( $result )
            $result = $data;

        return $result;
    }

    /**
     * get a valid click related with the tdbtoken ( click token ). Use it when a click already exists
     * @param string $token the unique click token or tdbtoken
     * @return \stdClass|null an native object with the click parameters
     */
    public static function getByToken( string $token )
    {
        $tbl = self::$tbl;
        $sql = "SELECT * FROM $tbl WHERE token = :token and status = 2";
        $click = DB::select( $sql, [ 'token' => $token ] );

        return self::prepareAndValidateClick( $click );
    }

    /**
     * It retrieves the last user click interaction by user identity
     * @param int $idUser user identity
     * @return mixed|null
     */
    public static function getLastUserClick( int $idUser )
    {
        $tbl = self::$tbl;
        $sql = "SELECT * FROM $tbl WHERE id_user = :id_user order by desc limit 1";
        $click = DB::select( $sql, [ 'id_user' => $idUser ] );

        return self::prepareAndValidateClick( $click );
    }

    /**
     * validate a click against some rules like time to live
     * @param \stdClass $click
     * @return bool
     */
    public static function validateClick( \stdClass $click )
    {
        $click = Carbon::parse( $click->created_at )->timestamp+self::$ttl;
        $now = now()->timestamp;

        $notExpired = $click > $now; // validation over expiration time
//        $notParsed = $click->status === 2; // validation against double clicking

        return $notExpired;
    }
    /**
     * @param mixed|null $click
     * @todo revise the responsibilities
     * @return mixed|null
     */
    public static function prepareAndValidateClick( $click )
    {
        if( $click ){
            $click = $click[0];
            $isValid = self::validateClick( $click );
            if( $isValid )
                $click->params = json_decode( $click->params );
            else
                $click = null;
        }
        else
            throw new InvalidClickException( 'invalid or not found click' );

        return $click;
    }
}
