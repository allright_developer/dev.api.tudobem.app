<?php
namespace App\Repository\Tracker;

use App\Mapper\ClickMapper;
use Illuminate\Support\Facades\DB;

class Conversion
{
    public static $tbl = 'conversion';

    /**
     * Persist a click data event to be used later
     * @param array $data click data
     * @return array|bool return the new data with click unique token
     * @throws \Exception
     */
    public static function persist( array $data )
    {
        $tbl = self::$tbl;

        $result = DB::insert(
            "INSERT INTO $tbl(click_token, id_user, entity, id_entity, params, created_at, status )
                       VALUES ( :click_token, :id_user, :entity, :id_entity, :params, :created_at, :status )",
            [
                'click_token' => $data[ 'click_token' ],
                'id_user' => $data[ 'id_user' ],
                'entity' => $data['entity'],
                'id_entity' => $data['id_entity'],
                'params' => \json_encode( $data[ 'params' ] ),
                'created_at' => \date('Y-m-d H:i:s' ),
                'status' => 1
            ]
        );

        if( $result )
            $result = $data;

        return $result;
    }
}
