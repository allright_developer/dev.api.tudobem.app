<?php
namespace App\Repository;

use App\Repository\Exceptions\LeadTransportNotFoundException;
use App\Service\Lead\Transporter\Http;
use App\Service\Lead\Transporter\Transporter;

class LeadTransport
{
    /**
     * Resolves the Lead transporter Strategy
     * @param string $transportName
     * @param \stdClass $settings
     * @throws \Exception
     * @return Transporter
     */
    public static function getStrategy( string $transportName, \stdClass $settings ): Transporter
    {
        $transportName = explode('-', $transportName );
        $strategy = null;

        switch ( $transportName[0] ){
            case 'http':
                $strategy = (new Http( $settings, $transportName[1] ))->resolveType();
            break;
        }

        if( !$strategy )
            throw new LeadTransportNotFoundException( 'Strategy not found' );

        return $strategy;
    }
}
