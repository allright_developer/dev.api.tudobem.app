<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserEmailConfirmedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public string $delayFor;
    /**
     * @var string
     */
    public string $userToken;

    /**
     * Create a new job instance.
     * @param string $userToken
     * @param string $delay
     */
    public function __construct( string $userToken, $delay = 'PT5M' )
    {
        $this->userToken = $userToken;
        $this->delayFor = $delay;
    }

    /**
     * Execute the job.
     * @return void
     */
    public function handle()
    {
        $config = config('mail.inboss' );
        $params = [ 'template' => $config['template'], 'id_origin' => $this->userToken, 'delay' => $this->delay ];
        (new Client())->post($config['endpoint'].$config['apikey'].'/message/send-email', [ 'form_params' => $params ] );
    }
}
