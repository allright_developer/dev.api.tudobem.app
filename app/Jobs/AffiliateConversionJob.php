<?php

namespace App\Jobs;

use App\Model\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AffiliateConversionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public User $user;
    public string $src;

    /**
     * AffiliateConversionJob constructor.
     * @param User $user
     * @param string $src
     */
    public function __construct( User $user, string $src )
    {
        $this->user = $user;
        $this->src = $src;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = \curl_init();
        $url = 'https://trck.venddi.digital/postback/receive/payout_type/cpl/u1tid/'.$this->src;
        \curl_setopt( $client, \CURLOPT_RETURNTRANSFER, 1 );
        \curl_setopt( $client, \CURLOPT_TIMEOUT, 5 );

        \curl_setopt( $client, \CURLOPT_URL, $url );
        $response = \curl_exec( $client );
    }
}
