<?php

namespace App\Jobs;

use App\Model\User;
use App\Model\User\ServiceSubscription;
use App\Service\Google\Fcm\WebPush;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PushSubscriptionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public User $user;

    public string $topic;

    public string $pushToken;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( User $user, string $topic, string $pushToken )
    {
        $this->user = $user;
        $this->topic = $topic;
        $this->pushToken = $pushToken;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pushService = new WebPush();

        if( $pushService->subscribe( $this->pushToken, $this->topic ) ){
            (new ServiceSubscription)
                ->fill([ 'data' => [ 'token' => $this->pushToken, 'topics' => [ $this->topic ] ], 'id_user' => $this->user->id ])->save();
        }
    }
}
