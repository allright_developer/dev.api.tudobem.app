<?php
namespace App\Jobs;

use App\Model\User;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EmailIntegrationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $retryAfter = 2;

    public User $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( User $user )
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $config = config('mail.inboss' );
        $params = [
            'name' => $this->user->name, 'email' => $this->user->email, 'ddd' => $this->user->ddd, 'phone' => $this->user->phone,
            'campaigns' => $config['campaign'], 'id_origin' => $this->user->token
        ];
        (new Client())->post($config['endpoint'].$config['apikey'].'/contact/post?from=post', [ 'form_params' => $params ] );
    }
}
