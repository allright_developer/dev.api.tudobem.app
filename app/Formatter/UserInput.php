<?php

namespace App\Formatter;


class UserInput
{
    /**
     * Returns only the phone numbers
     * @param string $v Phone to remove what it is not a number
     * @return string
     */
    public static function phone( $v )
    {
        return preg_replace('/\D/', '', $v );
    }
}
