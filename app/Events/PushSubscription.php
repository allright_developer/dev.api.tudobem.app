<?php

namespace App\Events;

use App\Model\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PushSubscription
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    public $topic;

    public $pushToken;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct( User $user, string $topic, string $pushToken )
    {
        $this->user = $user;
        $this->topic = $topic;
        $this->pushToken = $pushToken;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
