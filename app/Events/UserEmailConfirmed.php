<?php
namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserEmailConfirmed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string user unique token
     */
    public string $userToken;

    public string $delay = 'PT5M';
    /**
     * Create a new event instance.
     * @param string $userToken
     * @param string $delay
     */
    public function __construct( string $userToken, string $delay = '' )
    {
        $this->userToken = $userToken;
        $this->delay     = $delay ?: $this->delay;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
