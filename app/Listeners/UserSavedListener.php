<?php
namespace App\Listeners;

use App\Events\AffiliateConversionEvent;
use App\Events\UserCreated;
use App\Model\Settings;
use App\Model\User;
use App\Model\User\Data;
use App\Model\UserCredit;
use App\Model\UserLog;
use App\Notifications\UserNotificator;
use App\Security\Token;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserSavedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle user saved event.
     * @param  object  $event
     * @return void
     */
    public function handle( UserCreated $event )
    {
        $event->user->notify( new UserNotificator( $event->user ) );
        // dynamic bonus
        $bonus = \json_decode( Settings::query()->where('status', 1 )->first(['conf']) )->conf->bonus;
        // user credit creation, it could be improved with customized credit addition
        $credits = UserCredit::create([
            'token' => Token::generate(),
            'id_user' => $event->user->id,
            'crypto' => $bonus->signup ?? 0
        ]);
        $userData = Data::create( [ 'id_user' => $event->user->id ] );
        $queryParams = request()->post( 'query' );
        $this->verifyInvitation( $event->user, $bonus, $queryParams );
        $this->verifyAffiliation( $event->user, $queryParams );
    }

    /**
     * Verifies and attributes a new user registration to an inviter
     * @param User $user
     * @param $bonus
     * @param array $query
     */
    protected function verifyInvitation( User $user, $bonus, $query = [] )
    {
        if( isset( $query['token'] ) ){
            $inviter = User::where('token', $query['token'] )->first(['id']);
            if( $inviter ){
                // bonifying the user inviter
                $bonus = $bonus->memberGetMember > 100 ? 10 : $bonus->memberGetMember;
                $r = UserCredit::where( 'id_user', $inviter->id )->increment('crypto', $bonus );
                if( $r ){
                    User\Invitation::firstOrCreate([
                        'id_user' => $user->id, 'invited_by' => $inviter->id, 'from' => $query['from'] ?? null
                    ]);
                    UserLog::create([
                        'id_user' => $inviter->id, 'action_type' => \App\Mapper\UserLog::MEMBER_BY_MEMBER['id'],
                        'description' => \App\Mapper\UserLog::MEMBER_BY_MEMBER['label'], 'data' => [ 'member' => $user->name ], 'notifiable' => 1
                    ]);
                }
            }
        }
    }

    /**
     * affiliation conversion business logic. it fetches for src query parameter to fire a conversion
     * @param User $user
     * @param $query
     */
    protected function verifyAffiliation( User $user, $query )
    {
        try{
            if( isset( $query['src'] ) ){
                event( new AffiliateConversionEvent( $user, $query['src'] ) );
            }
        }
        catch( \Exception $exception ){

        }
    }
}
