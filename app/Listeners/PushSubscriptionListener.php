<?php

namespace App\Listeners;

use App\Events\PushSubscription;
use App\Jobs\PushSubscriptionJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PushSubscriptionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle( PushSubscription $event )
    {
        PushSubscriptionJob::dispatch( $event->user, $event->topic, $event->pushToken )
            ->onQueue('push_integration' )
            ->delay( now()->addMinutes(2) );
    }
}
