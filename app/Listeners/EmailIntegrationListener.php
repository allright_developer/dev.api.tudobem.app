<?php
namespace App\Listeners;

use App\Events\EmailIntegrationEvent;
use App\Jobs\EmailIntegrationJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class EmailIntegrationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {}

    /**
     * Handle the event.
     * @param EmailIntegrationEvent $event
     * @return void
     */
    public function handle( EmailIntegrationEvent $event )
    {
        EmailIntegrationJob::dispatch( $event->user )
            ->delay( now()->addMinutes( 1 ) )
            ->onQueue( 'email_integration' );
    }
}
