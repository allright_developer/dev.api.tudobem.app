<?php

namespace App\Listeners;

use App\Events\AffiliateConversionEvent;
use App\Jobs\AffiliateConversionJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AffiliateConversionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AffiliateConversionEvent $event
     * @return void
     */
    public function handle( AffiliateConversionEvent $event )
    {
        AffiliateConversionJob::dispatch( $event->user, $event->src )
            ->onQueue('affiliate_conversion' )->delay( now()->addMinutes( 2 ) );
    }
}
